<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LayoutsDetails extends Model
{
    protected $fillable = [
        'parent_id',
        'busy',
        'detail',
        'image'
    ];
}
