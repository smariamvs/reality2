<?php

namespace App\Http\Controllers;

use App\AboutIcons;
use App\AboutUs;
use App\Layouts;
use App\LayoutsDetails;
use App\Settings;
use Illuminate\Http\Request;
use App\Translations;
use App\Models\AdminSettings;
use App\Models\Gallery;
use App\Models\Partners;
class AdminController extends Controller
{
    public function index(){
        return view('admin.index');
    }

    public function AddFloor(){
        return view('admin.floor.add');
    }
    public function storeFloor (Request $request){
        $data = $request->all();
        if($request->hasFile('image')){
            $image = \UploadHelper::upload($request->file('image'));
            $data['image']  = $image;
        }
        Layouts::create($data);

        return redirect()->back()->with('success','Floor has been successfully added');
    }

    public function AddLayoutsDetails(){

    }

    public function Settings(){
        $setting = Settings::find(1);
        return view('admin.settings.index',compact('setting'));
    }

    public function UpdateSettings(Request $request){
//        dd($request->all());
        $setting = Settings::find(1);
        $data = $request->all();
        if($request->hasFile('logo')){
            $data['logo'] = \UploadHelper::upload($request->file('logo'));
        }
        if($request->hasFile('partners')){
            $data['partners'] = \UploadHelper::upload($request->file('partners'));
        }  if($request->hasFile('green')){
            $data['green'] = \UploadHelper::upload($request->file('green'));
        }
        $setting->update($data);
        return redirect()->back()->with('success','Settings has been successfully updated');

    }

    public function translation(){
        $translation = Translations::all();
        $return = [
            'page_title'=>'Translations',
            'translation'=>$translation,
        ];
        return view('admin.translation',$return);
    }
    public function translationAdd(Request $request){
        $this->validate($request, [
            'key' => 'required|unique:translations|max:255',
            'am' => 'required',
            'ru' => 'required',
            'en' => 'required',
        ]);
        Translations::create($request->all());
        return redirect()->back()->with('success', 'Translation has been added');
    }

    public function translationDelete($id){
        $trans = Translations::find($id);
        if(!is_null($trans)){
            $trans->delete();
            return redirect()->back()->with('success', 'Translation has been deleted');
        }else{
            return redirect()->back()->with('success', 'No such data');
        }

    }

    public function translationEdit(Request $request){
        $id = $request->editid;
        $trans = Translations::FindOrfail($id);
        $trans->en = $request->input('en');
        $trans->ru = $request->input('ru');
        $trans->am = $request->input('am');
        $trans->save();

        return redirect()->back()->with('success', 'Translation has been successfully updated');

    }


    public function Video(Request $request){
        $video = AdminSettings::where('key','video')->first();
        return view('admin.video',compact('video'));
    }
//video
    public function saveVideo(Request $request){

        $request->validate([
            'video' => 'required|mimes:mp4,x-flv,x-mpegURL,MP2T,3gpp,quicktime,x-msvideo,x-ms-wmv,webm'
        ]);
        $videoName = \UploadHelper::upload($request->file('video'));
        $video = AdminSettings::where('key','video')->first();
        if ($video) {
            $video->value = '/uploads/' . $videoName;
            $video->save();
        }
        else {
           $video = new AdminSettings();
           $video->key = "video";
           $video->value = '/uploads/' . $videoName;
           $video->save();
        }
        return redirect()->route('admin.video')->with('success', 'Successful upload');
    }

//    Gallery
    public function PhotoGalleryIndex(){
        $gallery = Gallery::orderBy('order', 'asc')->get();
        return view('admin.gallery.index',compact('gallery'));
    }
    public function PhotoGallerySave(Request $request){
        $lastOrder = Gallery::orderBy('id', 'desc')->first();
        if (!$lastOrder) $lastOrder = 1;
       else $lastOrder = $lastOrder->order + 1;
        $this->validate(
            $request,
            [
                'images' => 'required',
                'images.*' => 'mimes:jpeg,jpg,png,svg,webp|max:10000',
            ]);
       foreach ($request->images as $image){
           $imageName = \UploadHelper::upload($image);
           $galleryItem = new Gallery();
           $galleryItem->image = '/uploads/' . $imageName;
           $galleryItem->order = $lastOrder;
           $galleryItem->save();
           $lastOrder++;
       }
        return redirect()->route('admin.photo-gallery-index')->with('success', 'Successful upload');
    }

    public function PhotoGalleryDelete($id){
        $image = Gallery::findOrFail($id);
        $image->delete();
        return redirect()->route('admin.photo-gallery-index')->with('success', 'Successful Deleted');
    }
    public function PhotoGalleryChangeOrder(Request $request){
       $order = 1;
       $ids = explode(',' , $request->ids);
       foreach ($ids as $id){
           $image = Gallery::findOrFail($id);
           $image->order = $order;
           $order++;
           $image->save();
       }
       return response()->json([
           'success'=>true
       ]);
    }
//    Partners

    public function PartnersIndex(){
        $partners = Partners::orderBy('order', 'asc')->get();
        return view('admin.partners.index',compact('partners'));
    }

    public function PartnersSave(Request $request){
        $lastOrder = Partners::orderBy('id', 'desc')->first();
        if (!$lastOrder) $lastOrder = 1;
       else $lastOrder = $lastOrder->order + 1;
        $this->validate(
            $request,
            [
                'images' => 'required',
                'images.*' => 'mimes:jpeg,jpg,png,svg,webp|max:10000',
            ]);
       foreach ($request->images as $image){
           $imageName = \UploadHelper::upload($image);
           $galleryItem = new Partners();
           $galleryItem->image = '/uploads/' . $imageName;
           $galleryItem->order = $lastOrder;
           $galleryItem->save();
           $lastOrder++;
       }
        return redirect()->route('admin.partners-index')->with('success', 'Successful upload');
    }

    public function PartnersDelete($id){
        $image = Partners::findOrFail($id);
        $image->delete();
        return redirect()->route('admin.partners-index')->with('success', 'Successful Deleted');
    }
    public function PartnersChangeOrder(Request $request){
       $order = 1;
       $ids = explode(',' , $request->ids);
       foreach ($ids as $id){
           $image = Partners::findOrFail($id);
           $image->order = $order;
           $order++;
           $image->save();
       }
       return response()->json([
           'success'=>true
       ]);
    }

    public function AddLayOutDetails (){
        $floors = Layouts::get();
        return view('admin.floor.addFloorDetails',compact('floors'));
    }

    public function SaveLayoutDetails(Request $request){

        foreach ($request->details as $detail){
            $detail = trim($detail);
            $detail = str_replace('<area','<area data-id="" data-bussy="" name="" ',$detail);
            LayoutsDetails::create([
                'parent_id'=>$request->parent_id,
                'detail'=>$detail
            ]);
        }
        return redirect()->back()->with('success','Details has been successfully added');
    }

    public function LayOutsList(){
        $layouts = Layouts::orderBy('floor','ASC')->get();
        return view('admin.floor.list',compact('layouts'));
    }

    public function EditLayout($id){
        $layout = Layouts::find($id);
        return view('admin.floor.layoutEdit',compact('layout'));
    }

    public function UpdateLayoutDetail (Request $request){
        $detail = LayoutsDetails::find($request->id);
        if($request->hasFile('image')){
            $detail->image = \UploadHelper::upload($request->file('image'));
        }
        $detail->busy = $request->busy;
        $detail->save();
        return redirect()->back()->with('success','Floor has been successfully updated');

    }

    public function AboutUs (){
        $about_am = AboutUs::where('lang','am')->first();
        $about_ru = AboutUs::where('lang','ru')->first();
        $about_en = AboutUs::where('lang','en')->first();

        return view('admin.about',compact('about_am','about_ru','about_en'));
    }

    public function AboutUsUpdate(Request $request){
        $about_am = AboutUs::where('lang','am')->first();
        $about_am->text = $request->about_am;
        $about_am->save();
        $about_ru = AboutUs::where('lang','ru')->first();
        $about_ru->text = $request->about_ru;
        $about_ru->save();
        $about_en = AboutUs::where('lang','en')->first();
        $about_en->text = $request->about_en;
        $about_en->save();

        return redirect()->back()->with('success','About us edited successfully');
    }

    public function AboutUsIcons(){
        $icons = AboutIcons::where('lang',Lang())->get();
        return view('admin.aboutIcons',compact('icons'));
    }
    public function AddAboutUsIcon (){
        return view('admin.addAboutIcon');
    }

    public function SaveAboutIcon(Request $request){
       $this->validate($request,[
           'icon'=>'required',
           'text_am'=>'required',
           'text_ru'=>'required',
           'text_en'=>'required',
       ]);
        $rand = rand(0001,9999);
       AboutIcons::create([
            'icon'=>$request->icon,
            'unique_id'=>$rand,
           'text'=>$request->text_am,
           'lang'=>'am'
       ]);
       AboutIcons::create([
            'icon'=>$request->icon,
            'unique_id'=>$rand,
           'text'=>$request->text_en,
           'lang'=>'en'
       ]);
       AboutIcons::create([
            'icon'=>$request->icon,
            'unique_id'=>$rand,
           'text'=>$request->text_ru,
           'lang'=>'ru'
       ]);
       \Session::flash('success','About us icon has been successfully added');
       return redirect()->route('admin.about.us.icons');
    }

    public function EditAboutIcon ($id){
        $icon_am = AboutIcons::where('lang','am')->where('unique_id',$id)->first();
        $icon_ru = AboutIcons::where('lang','ru')->where('unique_id',$id)->first();
        $icon_en = AboutIcons::where('lang','en')->where('unique_id',$id)->first();
        return view('admin.editAboutIcon',compact('icon_am','icon_en','icon_ru'));
    }

    public function UpdateAboutIcon ($id,Request $request){
        $icon_am = AboutIcons::where('lang','am')->where('unique_id',$id)->first();
        $icon_am->text = $request->text_am;
        $icon_am->icon = $request->icon;
        $icon_am->save();
        $icon_ru = AboutIcons::where('lang','ru')->where('unique_id',$id)->first();
        $icon_ru->text = $request->text_ru;
        $icon_ru->icon = $request->icon;
        $icon_ru->save();
        $icon_en = AboutIcons::where('lang','en')->where('unique_id',$id)->first();
        $icon_en->text = $request->text_en;
        $icon_en->icon = $request->icon;
        $icon_en->save();
        \Session::flash('success','About us icons updated successfully');
        return redirect()->route('admin.about.us.icons');
    }

    public function DeleteAboutUsIcon ($id){
        AboutIcons::where('unique_id',$id)->delete();
        \Session::flash('success','About us icons deleted successfully');
        return redirect()->route('admin.about.us.icons');
    }
}
