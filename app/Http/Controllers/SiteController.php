<?php

namespace App\Http\Controllers;

use App\AboutIcons;
use App\AboutUs;
use App\Layouts;
use App\Models\AdminSettings;
use App\Models\Gallery;
use App\Models\Partners;
use App\Settings;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index(){
        $layouts = Layouts::orderBy('floor','ASC')->get();
        $settings = Settings::find(1);
        $video = AdminSettings::where('key','video')->first();
        $gallery = Gallery::orderBy('order')->get();
        $partners = Partners::orderBy('order')->get();
        $about = AboutUs::where('lang',Lang())->first();
        $aboutIcons = AboutIcons::where('lang',Lang())->get();

        return view('front.index',compact('layouts','settings','video','gallery','partners','about','aboutIcons'));
    }
}
