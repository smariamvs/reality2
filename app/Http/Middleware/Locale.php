<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = [
            'am',
            'en',
            'ru'
        ];
        $locale = $request->segment(1);
        if(!in_array($locale,$lang)){
            return redirect()->route('index',App::getLocale());
        }
        App::setLocale($locale);
        return $next($request);
    }
}
