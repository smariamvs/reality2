<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layouts extends Model
{
    protected $with = ['details'];

    protected $fillable = [
        'image',
        'name',
        'start',
        'end',
        'floor',
    ];
    public function details(){
      return $this->hasMany(LayoutsDetails::class,'parent_id','id');
    }
}
