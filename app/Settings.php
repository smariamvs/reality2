<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = [
        'logo',
        'fb',
        'insta',
        'fb_status',
        'phone',
        'email',
        'partners',
        'green',

    ];
}
