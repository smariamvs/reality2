<?php

use App\Cart;
use App\Models\Products;
use App\Translations;
use App\Models\Settings;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

function getlang($key)
{
    $local = App::getLocale();
    if (is_null($local)) {
        $local = 'am';
    }
    $translation = Translations::where('key', $key)->first();
    if ($translation) {
        return $translation->$local;
    } else {
        return $key;
    }

}

function Lang()
{
    $local = App::getLocale();
    if (is_null($local)) {
        $local = 'am';

    }
    return $local;
}

function getCart()
{
    $carts = Session('cart');
    $count = 0;
    if ($carts == null) {
        return 0;
    }

    foreach ($carts as $cart) {
        $count = $count + 1;
    }
    return $count;
}

function getCartMoney()
{
    $price = 0;
    $carts = Session('cart');
    if ($carts != null) {
        foreach ($carts as $cart) {
            $price += $cart['price'] * $cart['qty'];
        }
    }
    return $price;
}

function cart()
{
    $cart = [];
    if (Session::has('user_token')) {

        $user_token = Session::get('user_token');
        $cart = Cart::where('user_token', $user_token)->get();
    }

    return $cart;
}

function setting()
{
    $setting = Settings::where('lang', Lang())->first();
    return $setting;
}

function categories(){
    return \App\Models\Categories::where('lang',Lang())->with('subs')->get();
}

function subcategories(){
    return \App\SubCategories::where('lang',Lang())->get();
}




