<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutIcons extends Model
{
    protected $fillable = [
        'unique_id',
        'text',
        'icon',
        'lang'
    ];
}
