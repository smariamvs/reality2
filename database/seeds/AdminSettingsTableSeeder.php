<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminSettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_settings')->delete();
        
        \DB::table('admin_settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'video',
                'value' => '/uploads/627a7a8ab32c3.mp4',
                'created_at' => '2022-05-10 14:45:30',
                'updated_at' => '2022-05-10 14:45:30',
            ),
        ));
        
        
    }
}