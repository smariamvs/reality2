<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TranslationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('translations')->delete();
        
        \DB::table('translations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'Home',
                'en' => 'Home',
                'ru' => 'Главная',
                'am' => 'Գլխավոր',
                'created_at' => '2022-05-08 18:34:29',
                'updated_at' => '2022-05-11 17:03:28',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'Salles Hall',
                'en' => 'Salles Hall',
                'ru' => 'Торговый зал',
                'am' => 'Վաճառքի սրահ',
                'created_at' => '2022-05-08 18:35:27',
                'updated_at' => '2022-05-08 18:35:27',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'About Us',
                'en' => 'About Us',
                'ru' => 'О нас',
                'am' => 'Մեր մասին',
                'created_at' => '2022-05-08 18:36:11',
                'updated_at' => '2022-05-08 18:36:11',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'Gallery',
                'en' => 'Gallery',
                'ru' => 'Галерея',
                'am' => 'Ցուցասրահ',
                'created_at' => '2022-05-08 18:37:03',
                'updated_at' => '2022-05-08 18:37:03',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'Contacts',
                'en' => 'Contacts',
                'ru' => 'Контакты',
                'am' => 'Կապ',
                'created_at' => '2022-05-08 18:37:34',
                'updated_at' => '2022-05-08 18:37:34',
            ),
        ));
        
        
    }
}