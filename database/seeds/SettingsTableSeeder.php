<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'logo' => '62780b3a8f9fc.png',
                'fb' => 'https://fb.com',
                'insta' => 'https://instagram.com',
                'fb_status' => '1',
                'insta_status' => '1',
                'phone' => '+374454596',
                'email' => 'admin@mail.com',
                'created_at' => NULL,
                'updated_at' => '2022-05-08 18:26:02',
            ),
        ));
        
        
    }
}