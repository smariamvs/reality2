<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AboutUsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('about_us')->delete();
        
        \DB::table('about_us')->insert(array (
            0 => 
            array (
                'id' => 1,
                'slug' => 'aboutus',
                'text' => 'Edited text',
                'lang' => 'am',
                'created_at' => NULL,
                'updated_at' => '2022-05-11 18:08:36',
            ),
            1 => 
            array (
                'id' => 2,
                'slug' => 'aboutus',
                'text' => 'aboutus',
                'lang' => 'ru',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'slug' => 'aboutus',
                'text' => 'aboutus',
                'lang' => 'en',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}