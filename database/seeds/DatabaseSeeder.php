<?php

use Database\Seeders\AboutUsTableSeeder;
use Database\Seeders\LayoutsDetailsTableSeeder;
use Database\Seeders\LayoutsTableSeeder;
use Database\Seeders\SettingsTableSeeder;
use Database\Seeders\TranslationsTableSeeder;
use Database\Seeders\AdminSettingsTableSeeder;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(LayoutsTableSeeder::class);
        $this->call(LayoutsDetailsTableSeeder::class);
        $this->call(TranslationsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AboutUsTableSeeder::class);
        $this->call(AdminSettingsTableSeeder::class);
    }
}
