<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LayoutsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('layouts')->delete();
        
        \DB::table('layouts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'image' => '63692a6782f7f.png',
                'name' => 'parking2',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => -2,
                'created_at' => '2022-11-07 15:55:19',
                'updated_at' => '2022-11-07 15:55:19',
            ),
            1 => 
            array (
                'id' => 2,
                'image' => '63692a9803ea9.png',
                'name' => 'parking1',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => -1,
                'created_at' => '2022-11-07 15:56:08',
                'updated_at' => '2022-11-07 15:56:08',
            ),
            2 => 
            array (
                'id' => 3,
                'image' => '63692ab2dd7de.png',
                'name' => 'floor1',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 1,
                'created_at' => '2022-11-07 15:56:34',
                'updated_at' => '2022-11-07 15:56:34',
            ),
            3 => 
            array (
                'id' => 4,
                'image' => '63692ac7e321f.png',
                'name' => 'floor2',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 2,
                'created_at' => '2022-11-07 15:56:55',
                'updated_at' => '2022-11-07 15:56:55',
            ),
            4 => 
            array (
                'id' => 5,
                'image' => '63692ad8d655f.png',
                'name' => 'floor3',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 3,
                'created_at' => '2022-11-07 15:57:12',
                'updated_at' => '2022-11-07 15:57:12',
            ),
            5 => 
            array (
                'id' => 6,
                'image' => '63692ae759d0a.png',
                'name' => 'floor4',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 4,
                'created_at' => '2022-11-07 15:57:27',
                'updated_at' => '2022-11-07 15:57:27',
            ),
            6 => 
            array (
                'id' => 7,
                'image' => '63692af45d544.png',
                'name' => 'floor5',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 5,
                'created_at' => '2022-11-07 15:57:40',
                'updated_at' => '2022-11-07 15:57:40',
            ),
            7 => 
            array (
                'id' => 8,
                'image' => '63692b0299533.png',
                'name' => 'floor6',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 6,
                'created_at' => '2022-11-07 15:57:54',
                'updated_at' => '2022-11-07 15:57:54',
            ),
            8 => 
            array (
                'id' => 9,
                'image' => '636ab263b9839.jpg',
                'name' => 'floor7',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 7,
                'created_at' => '2022-11-08 19:47:47',
                'updated_at' => '2022-11-08 19:47:47',
            ),
            9 => 
            array (
                'id' => 10,
                'image' => '636ab27387936.png',
                'name' => 'floor8',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 8,
                'created_at' => '2022-11-08 19:48:03',
                'updated_at' => '2022-11-08 19:48:03',
            ),
            10 => 
            array (
                'id' => 11,
                'image' => '636ab288b56ac.png',
                'name' => 'floor9',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 9,
                'created_at' => '2022-11-08 19:48:24',
                'updated_at' => '2022-11-08 19:48:24',
            ),
            11 => 
            array (
                'id' => 12,
                'image' => '636ab29645a4e.png',
                'name' => 'floor10',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 10,
                'created_at' => '2022-11-08 19:48:38',
                'updated_at' => '2022-11-08 19:48:38',
            ),
            12 => 
            array (
                'id' => 13,
                'image' => '636ab2a358d37.png',
                'name' => 'floor11',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 11,
                'created_at' => '2022-11-08 19:48:51',
                'updated_at' => '2022-11-08 19:48:51',
            ),
            13 => 
            array (
                'id' => 14,
                'image' => '636ab2be95e89.png',
                'name' => 'floor12',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 12,
                'created_at' => '2022-11-08 19:49:18',
                'updated_at' => '2022-11-08 19:49:18',
            ),
            14 => 
            array (
                'id' => 15,
                'image' => '636ab2d055ea8.png',
                'name' => 'floor13',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 13,
                'created_at' => '2022-11-08 19:49:36',
                'updated_at' => '2022-11-08 19:49:36',
            ),
            15 => 
            array (
                'id' => 16,
                'image' => '636ab2df3945d.png',
                'name' => 'floor14',
                'start' => '<map name="">',
                'end' => '</map>',
                'floor' => 14,
                'created_at' => '2022-11-08 19:49:51',
                'updated_at' => '2022-11-08 19:49:51',
            ),
        ));
        
        
    }
}