<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Admin',
                    'email' => 'admin@admin.com',
                    'password' => '$2a$12$rr0uEQwkrtyKoKWxr6.BsOFlderJ1W0VQcjPhyaRi/9AnY9M6gBF6',/*admin*/
                ),
        ));
    }
}
