<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LayoutsDetailsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('layouts_details')->delete();
        
        \DB::table('layouts_details')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => 4,
                'busy' => 'yes',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,226,1115,230,1106,1089,1429,1085,1409,1476,272,1484" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 18:53:38',
                'updated_at' => '2022-11-08 18:53:38',
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => 4,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,1512,1433,1508,1437,1911,1115,1911,1102,2746,276,2754" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 18:53:41',
                'updated_at' => '2022-11-08 18:53:41',
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => 4,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1127,1916,1921,1924,1921,2734,1921,3565,1062,3565,1054,2758,1119,2754" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 18:53:41',
                'updated_at' => '2022-11-08 18:53:41',
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => 4,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1941,3569,3529,3565,3525,2992,3223,2972,3231,2706,2699,2722,2687,2137,2449,2125,2449,1875,1945,1887,1925,2718" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 18:53:42',
                'updated_at' => '2022-11-08 18:53:42',
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => 4,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3493,2734,4295,2730,4303,1484,3493,1484" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 18:53:42',
                'updated_at' => '2022-11-08 18:53:42',
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => 4,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="4307,1476,4315,44,3485,52,3493,1476" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 18:53:42',
                'updated_at' => '2022-11-08 18:53:42',
            ),
            6 => 
            array (
                'id' => 7,
                'parent_id' => 4,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="2658,48,3477,56,3461,1057,3203,1053,3190,1641,2683,1645" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 18:53:42',
                'updated_at' => '2022-11-08 18:53:42',
            ),
            7 => 
            array (
                'id' => 8,
                'parent_id' => 4,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1893,246,2654,234,2654,1645,1880,1661" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 18:53:42',
                'updated_at' => '2022-11-08 18:53:42',
            ),
            8 => 
            array (
                'id' => 9,
                'parent_id' => 5,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,222,1086,238,1086,1057,1421,1069,1413,1468,264,1476" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:17:05',
                'updated_at' => '2022-11-08 19:17:05',
            ),
            9 => 
            array (
                'id' => 10,
                'parent_id' => 5,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="276,1484,272,2916,1038,2920,1050,2730,1094,2702,1102,1916,1413,1899,1417,1492" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:17:08',
                'updated_at' => '2022-11-08 19:17:08',
            ),
            10 => 
            array (
                'id' => 11,
                'parent_id' => 5,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1111,1924,1913,1907,1913,3553,1050,3549,1062,2734,1098,2730" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:17:08',
                'updated_at' => '2022-11-08 19:17:08',
            ),
            11 => 
            array (
                'id' => 12,
                'parent_id' => 5,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1925,1875,1933,3545,3509,3545,3489,2976,3211,2960,3207,2702,2683,2702,2679,2129,2453,2117,2441,1871" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:17:08',
                'updated_at' => '2022-11-08 19:17:08',
            ),
            12 => 
            array (
                'id' => 13,
                'parent_id' => 5,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1872,226,2650,234,2634,1633,1868,1629" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:17:08',
                'updated_at' => '2022-11-08 19:17:08',
            ),
            13 => 
            array (
                'id' => 14,
                'parent_id' => 5,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="2654,52,3461,52,3452,1048,3186,1040,3174,1593,2666,1593" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:17:08',
                'updated_at' => '2022-11-08 19:17:08',
            ),
            14 => 
            array (
                'id' => 15,
                'parent_id' => 5,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3469,56,4291,48,4287,1476,3481,1472" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:17:08',
                'updated_at' => '2022-11-08 19:17:08',
            ),
            15 => 
            array (
                'id' => 16,
                'parent_id' => 5,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3465,1496,4283,1500,4287,2710,3465,2722" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:17:08',
                'updated_at' => '2022-11-08 19:17:08',
            ),
            16 => 
            array (
                'id' => 17,
                'parent_id' => 6,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="284,226,1094,226,1090,1069,1421,1081,1413,1488,272,1476" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:25:17',
                'updated_at' => '2022-11-08 19:25:17',
            ),
            17 => 
            array (
                'id' => 18,
                'parent_id' => 6,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="268,1496,1417,1504,1425,1891,1115,1911,1094,2714,1058,2738,1050,2908,284,2920" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:25:20',
                'updated_at' => '2022-11-08 19:25:20',
            ),
            18 => 
            array (
                'id' => 19,
                'parent_id' => 6,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1913,1911,1127,1924,1111,2734,1058,2742,1042,3557,1913,3545" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:25:21',
                'updated_at' => '2022-11-08 19:25:21',
            ),
            19 => 
            array (
                'id' => 20,
                'parent_id' => 6,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1921,1867,2453,1871,2457,2117,2675,2117,2687,2702,3211,2702,3219,2984,3517,2984,3509,3553,1929,3553" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:25:21',
                'updated_at' => '2022-11-08 19:25:21',
            ),
            20 => 
            array (
                'id' => 21,
                'parent_id' => 6,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3477,2726,4295,2730,4295,1484,3473,1492" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:25:21',
                'updated_at' => '2022-11-08 19:25:21',
            ),
            21 => 
            array (
                'id' => 22,
                'parent_id' => 6,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="4307,1472,4295,44,3473,52,3489,1472" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:25:21',
                'updated_at' => '2022-11-08 19:25:21',
            ),
            22 => 
            array (
                'id' => 23,
                'parent_id' => 6,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3465,40,2658,44,2662,1637,3186,1637,3190,1040,3465,1044" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:25:21',
                'updated_at' => '2022-11-08 19:25:21',
            ),
            23 => 
            array (
                'id' => 24,
                'parent_id' => 6,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1880,234,2634,234,2638,1637,1868,1629" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:25:21',
                'updated_at' => '2022-11-08 19:25:21',
            ),
            24 => 
            array (
                'id' => 25,
                'parent_id' => 7,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="264,218,1082,218,1082,1113,1421,1109,1401,1468,248,1464" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:37:34',
                'updated_at' => '2022-11-08 19:37:34',
            ),
            25 => 
            array (
                'id' => 26,
                'parent_id' => 7,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="268,1488,1417,1496,1421,1903,1098,1920,1086,2734,1046,2746,1038,2924,252,2940" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:37:37',
                'updated_at' => '2022-11-08 19:37:37',
            ),
            26 => 
            array (
                'id' => 27,
                'parent_id' => 7,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1119,1936,1913,1924,1909,2746,1913,3557,1042,3569,1050,2746,1102,2746" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:37:37',
                'updated_at' => '2022-11-08 19:37:37',
            ),
            27 => 
            array (
                'id' => 28,
                'parent_id' => 7,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1921,3561,3521,3557,3513,3000,3211,3000,3203,2702,2662,2702,2634,2149,2445,2145,2437,1883,1933,1879" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:37:37',
                'updated_at' => '2022-11-08 19:37:37',
            ),
            28 => 
            array (
                'id' => 29,
                'parent_id' => 7,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3485,2746,4311,2722,4303,1480,3489,1480" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:37:37',
                'updated_at' => '2022-11-08 19:37:37',
            ),
            29 => 
            array (
                'id' => 30,
                'parent_id' => 7,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3477,44,4307,44,4311,1472,3489,1460" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:37:37',
                'updated_at' => '2022-11-08 19:37:37',
            ),
            30 => 
            array (
                'id' => 31,
                'parent_id' => 7,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="2654,28,3473,36,3461,1040,3190,1053,3190,1637,2670,1641" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:37:38',
                'updated_at' => '2022-11-08 19:37:38',
            ),
            31 => 
            array (
                'id' => 32,
                'parent_id' => 7,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1860,214,2642,222,2638,1633,1888,1629" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:37:38',
                'updated_at' => '2022-11-08 19:37:38',
            ),
            32 => 
            array (
                'id' => 33,
                'parent_id' => 8,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="276,222,1102,234,1106,1125,1425,1125,1425,1274,1421,1476,276,1480" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:45:22',
                'updated_at' => '2022-11-08 19:45:22',
            ),
            33 => 
            array (
                'id' => 34,
                'parent_id' => 8,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,1504,268,2928,1046,2916,1058,2742,1111,2730,1111,1916,1429,1903,1429,1496" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:45:23',
                'updated_at' => '2022-11-08 19:45:23',
            ),
            34 => 
            array (
                'id' => 35,
                'parent_id' => 8,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1123,1928,1921,1920,1917,3557,1058,3565,1070,2750,1115,2742" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:45:23',
                'updated_at' => '2022-11-08 19:45:23',
            ),
            35 => 
            array (
                'id' => 36,
                'parent_id' => 8,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1925,1871,2457,1871,2457,2133,2691,2133,2699,2714,3207,2710,3207,2972,3513,2992,3517,3553,1933,3561" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:45:23',
                'updated_at' => '2022-11-08 19:45:23',
            ),
            36 => 
            array (
                'id' => 37,
                'parent_id' => 8,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3497,2730,4295,2726,4295,1488,3481,1484" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:45:23',
                'updated_at' => '2022-11-08 19:45:23',
            ),
            37 => 
            array (
                'id' => 38,
                'parent_id' => 8,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3485,1472,4299,1476,4295,44,3473,60" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:45:23',
                'updated_at' => '2022-11-08 19:45:23',
            ),
            38 => 
            array (
                'id' => 39,
                'parent_id' => 8,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="2650,44,3469,48,3465,1057,3199,1061,3178,1625,2670,1633" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:45:23',
                'updated_at' => '2022-11-08 19:45:23',
            ),
            39 => 
            array (
                'id' => 40,
                'parent_id' => 8,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1880,230,2638,234,2650,1637,1884,1645" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:45:23',
                'updated_at' => '2022-11-08 19:45:23',
            ),
            40 => 
            array (
                'id' => 41,
                'parent_id' => 9,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="77,66,307,70,301,306,399,311,397,410,77,414" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:54:43',
                'updated_at' => '2022-11-08 19:54:43',
            ),
            41 => 
            array (
                'id' => 42,
                'parent_id' => 9,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="75,422,73,813,295,820,298,765,310,764,309,537,397,534,396,421" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:54:43',
                'updated_at' => '2022-11-08 19:54:43',
            ),
            42 => 
            array (
                'id' => 43,
                'parent_id' => 9,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="314,542,535,538,538,994,295,994,300,767,310,767" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:54:43',
                'updated_at' => '2022-11-08 19:54:43',
            ),
            43 => 
            array (
                'id' => 44,
                'parent_id' => 9,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="524,63,741,64,745,460,526,459" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:54:43',
                'updated_at' => '2022-11-08 19:54:43',
            ),
            44 => 
            array (
                'id' => 45,
                'parent_id' => 9,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="742,11,972,11,971,296,892,294,890,459,748,460" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:54:43',
                'updated_at' => '2022-11-08 19:54:43',
            ),
            45 => 
            array (
                'id' => 46,
                'parent_id' => 9,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="976,14,1203,12,1204,413,975,413" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:54:43',
                'updated_at' => '2022-11-08 19:54:43',
            ),
            46 => 
            array (
                'id' => 47,
                'parent_id' => 9,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="975,419,1204,421,1207,767,976,764" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:54:43',
                'updated_at' => '2022-11-08 19:54:43',
            ),
            47 => 
            array (
                'id' => 48,
                'parent_id' => 9,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="541,523,543,997,988,998,983,837,903,836,904,756,746,753,742,598,689,597,687,525" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 19:54:43',
                'updated_at' => '2022-11-08 19:54:43',
            ),
            48 => 
            array (
                'id' => 57,
                'parent_id' => 10,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,222,1078,238,1082,1101,1425,1109,1413,1472,272,1464" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:38',
                'updated_at' => '2022-11-08 20:02:38',
            ),
            49 => 
            array (
                'id' => 58,
                'parent_id' => 10,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,1500,1413,1500,1421,1899,1090,1916,1086,2666,1042,2674,1030,2912,268,2916" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:41',
                'updated_at' => '2022-11-08 20:02:41',
            ),
            50 => 
            array (
                'id' => 59,
                'parent_id' => 10,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1115,1924,1913,1924,1921,3533,1058,3541,1058,2742,1111,2738" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            51 => 
            array (
                'id' => 60,
                'parent_id' => 10,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1860,226,2638,214,2646,1625,1876,1641" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            52 => 
            array (
                'id' => 61,
                'parent_id' => 10,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="2642,24,3452,44,3444,1044,3182,1036,3186,1637,2658,1637" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            53 => 
            array (
                'id' => 62,
                'parent_id' => 10,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3477,48,4291,36,4295,1464,3469,1480" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            54 => 
            array (
                'id' => 63,
                'parent_id' => 10,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3469,1492,4287,1492,4299,2722,3481,2718" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            55 => 
            array (
                'id' => 64,
                'parent_id' => 10,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1913,1867,1929,3541,3505,3533,3505,2992,3215,2980,3199,2698,2654,2702,2634,2141,2453,2121,2445,1879" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            56 => 
            array (
                'id' => 65,
                'parent_id' => 11,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,222,1078,238,1082,1101,1425,1109,1413,1472,272,1464" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:38',
                'updated_at' => '2022-11-08 20:02:38',
            ),
            57 => 
            array (
                'id' => 66,
                'parent_id' => 11,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,1500,1413,1500,1421,1899,1090,1916,1086,2666,1042,2674,1030,2912,268,2916" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:41',
                'updated_at' => '2022-11-08 20:02:41',
            ),
            58 => 
            array (
                'id' => 67,
                'parent_id' => 11,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1115,1924,1913,1924,1921,3533,1058,3541,1058,2742,1111,2738" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            59 => 
            array (
                'id' => 68,
                'parent_id' => 11,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1860,226,2638,214,2646,1625,1876,1641" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            60 => 
            array (
                'id' => 69,
                'parent_id' => 11,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="2642,24,3452,44,3444,1044,3182,1036,3186,1637,2658,1637" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            61 => 
            array (
                'id' => 70,
                'parent_id' => 11,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3477,48,4291,36,4295,1464,3469,1480" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            62 => 
            array (
                'id' => 71,
                'parent_id' => 11,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3469,1492,4287,1492,4299,2722,3481,2718" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            63 => 
            array (
                'id' => 72,
                'parent_id' => 11,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1913,1867,1929,3541,3505,3533,3505,2992,3215,2980,3199,2698,2654,2702,2634,2141,2453,2121,2445,1879" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            64 => 
            array (
                'id' => 73,
                'parent_id' => 12,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,222,1078,238,1082,1101,1425,1109,1413,1472,272,1464" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:38',
                'updated_at' => '2022-11-08 20:02:38',
            ),
            65 => 
            array (
                'id' => 74,
                'parent_id' => 12,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,1500,1413,1500,1421,1899,1090,1916,1086,2666,1042,2674,1030,2912,268,2916" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:41',
                'updated_at' => '2022-11-08 20:02:41',
            ),
            66 => 
            array (
                'id' => 75,
                'parent_id' => 12,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1115,1924,1913,1924,1921,3533,1058,3541,1058,2742,1111,2738" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            67 => 
            array (
                'id' => 76,
                'parent_id' => 12,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1860,226,2638,214,2646,1625,1876,1641" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            68 => 
            array (
                'id' => 77,
                'parent_id' => 12,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="2642,24,3452,44,3444,1044,3182,1036,3186,1637,2658,1637" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            69 => 
            array (
                'id' => 78,
                'parent_id' => 12,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3477,48,4291,36,4295,1464,3469,1480" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            70 => 
            array (
                'id' => 79,
                'parent_id' => 12,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3469,1492,4287,1492,4299,2722,3481,2718" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            71 => 
            array (
                'id' => 80,
                'parent_id' => 12,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1913,1867,1929,3541,3505,3533,3505,2992,3215,2980,3199,2698,2654,2702,2634,2141,2453,2121,2445,1879" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            72 => 
            array (
                'id' => 81,
                'parent_id' => 13,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,222,1078,238,1082,1101,1425,1109,1413,1472,272,1464" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:38',
                'updated_at' => '2022-11-08 20:02:38',
            ),
            73 => 
            array (
                'id' => 82,
                'parent_id' => 13,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,1500,1413,1500,1421,1899,1090,1916,1086,2666,1042,2674,1030,2912,268,2916" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:41',
                'updated_at' => '2022-11-08 20:02:41',
            ),
            74 => 
            array (
                'id' => 83,
                'parent_id' => 13,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1115,1924,1913,1924,1921,3533,1058,3541,1058,2742,1111,2738" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            75 => 
            array (
                'id' => 84,
                'parent_id' => 13,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1860,226,2638,214,2646,1625,1876,1641" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            76 => 
            array (
                'id' => 85,
                'parent_id' => 13,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="2642,24,3452,44,3444,1044,3182,1036,3186,1637,2658,1637" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            77 => 
            array (
                'id' => 86,
                'parent_id' => 13,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3477,48,4291,36,4295,1464,3469,1480" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            78 => 
            array (
                'id' => 87,
                'parent_id' => 13,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3469,1492,4287,1492,4299,2722,3481,2718" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            79 => 
            array (
                'id' => 88,
                'parent_id' => 13,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1913,1867,1929,3541,3505,3533,3505,2992,3215,2980,3199,2698,2654,2702,2634,2141,2453,2121,2445,1879" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            80 => 
            array (
                'id' => 89,
                'parent_id' => 14,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,222,1078,238,1082,1101,1425,1109,1413,1472,272,1464" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:38',
                'updated_at' => '2022-11-08 20:02:38',
            ),
            81 => 
            array (
                'id' => 90,
                'parent_id' => 14,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,1500,1413,1500,1421,1899,1090,1916,1086,2666,1042,2674,1030,2912,268,2916" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:41',
                'updated_at' => '2022-11-08 20:02:41',
            ),
            82 => 
            array (
                'id' => 91,
                'parent_id' => 14,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1115,1924,1913,1924,1921,3533,1058,3541,1058,2742,1111,2738" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            83 => 
            array (
                'id' => 92,
                'parent_id' => 14,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1860,226,2638,214,2646,1625,1876,1641" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            84 => 
            array (
                'id' => 93,
                'parent_id' => 14,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="2642,24,3452,44,3444,1044,3182,1036,3186,1637,2658,1637" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            85 => 
            array (
                'id' => 94,
                'parent_id' => 14,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3477,48,4291,36,4295,1464,3469,1480" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            86 => 
            array (
                'id' => 95,
                'parent_id' => 14,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3469,1492,4287,1492,4299,2722,3481,2718" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            87 => 
            array (
                'id' => 96,
                'parent_id' => 14,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1913,1867,1929,3541,3505,3533,3505,2992,3215,2980,3199,2698,2654,2702,2634,2141,2453,2121,2445,1879" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            88 => 
            array (
                'id' => 97,
                'parent_id' => 15,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,222,1078,238,1082,1101,1425,1109,1413,1472,272,1464" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:38',
                'updated_at' => '2022-11-08 20:02:38',
            ),
            89 => 
            array (
                'id' => 98,
                'parent_id' => 15,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,1500,1413,1500,1421,1899,1090,1916,1086,2666,1042,2674,1030,2912,268,2916" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:41',
                'updated_at' => '2022-11-08 20:02:41',
            ),
            90 => 
            array (
                'id' => 99,
                'parent_id' => 15,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1115,1924,1913,1924,1921,3533,1058,3541,1058,2742,1111,2738" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            91 => 
            array (
                'id' => 100,
                'parent_id' => 15,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1860,226,2638,214,2646,1625,1876,1641" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            92 => 
            array (
                'id' => 101,
                'parent_id' => 15,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="2642,24,3452,44,3444,1044,3182,1036,3186,1637,2658,1637" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            93 => 
            array (
                'id' => 102,
                'parent_id' => 15,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3477,48,4291,36,4295,1464,3469,1480" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            94 => 
            array (
                'id' => 103,
                'parent_id' => 15,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3469,1492,4287,1492,4299,2722,3481,2718" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            95 => 
            array (
                'id' => 104,
                'parent_id' => 15,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1913,1867,1929,3541,3505,3533,3505,2992,3215,2980,3199,2698,2654,2702,2634,2141,2453,2121,2445,1879" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            96 => 
            array (
                'id' => 105,
                'parent_id' => 16,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,222,1078,238,1082,1101,1425,1109,1413,1472,272,1464" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:38',
                'updated_at' => '2022-11-08 20:02:38',
            ),
            97 => 
            array (
                'id' => 106,
                'parent_id' => 16,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="272,1500,1413,1500,1421,1899,1090,1916,1086,2666,1042,2674,1030,2912,268,2916" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:41',
                'updated_at' => '2022-11-08 20:02:41',
            ),
            98 => 
            array (
                'id' => 107,
                'parent_id' => 16,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1115,1924,1913,1924,1921,3533,1058,3541,1058,2742,1111,2738" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            99 => 
            array (
                'id' => 108,
                'parent_id' => 16,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1860,226,2638,214,2646,1625,1876,1641" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            100 => 
            array (
                'id' => 109,
                'parent_id' => 16,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="2642,24,3452,44,3444,1044,3182,1036,3186,1637,2658,1637" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            101 => 
            array (
                'id' => 110,
                'parent_id' => 16,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3477,48,4291,36,4295,1464,3469,1480" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            102 => 
            array (
                'id' => 111,
                'parent_id' => 16,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="3469,1492,4287,1492,4299,2722,3481,2718" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
            103 => 
            array (
                'id' => 112,
                'parent_id' => 16,
                'busy' => 'no',
                'detail' => '<area data-id="" data-bussy="" name=""  target="" alt="" title="" href="" coords="1913,1867,1929,3541,3505,3533,3505,2992,3215,2980,3199,2698,2654,2702,2634,2141,2453,2121,2445,1879" shape="poly">',
                'image' => NULL,
                'created_at' => '2022-11-08 20:02:42',
                'updated_at' => '2022-11-08 20:02:42',
            ),
        ));
        
        
    }
}