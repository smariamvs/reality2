<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::group(['prefix'=>'admin','as'=>'admin.','middleware'=>'auth'],function(){
    Route::get('/','AdminController@index')->name('index');
    Route::get('/add-floor','AdminController@AddFloor')->name('add.floor');
    Route::post('/save-floor','AdminController@storeFloor')->name('store.floor');
    Route::get('/settings','AdminController@Settings')->name('settings');
    Route::post('/settings/save','AdminController@UpdateSettings')->name('settings.save');

    Route::get('/add-layouts-details','AdminController@AddLayOutDetails')->name('add.layout.details');
    Route::post('/save-layouts-detail','AdminController@SaveLayoutDetails')->name('save.layout.details');

    Route::get('/layouts-list','AdminController@LayOutsList')->name('layouts.list');
    Route::get('edit-layout/{id}','AdminController@EditLayout')->name('layouts.edit');
    Route::post('/update-layout','AdminController@UpdateLayoutDetail')->name('update.layout.details');
    // Admin Translations
    Route::get('/translation','AdminController@translation')->name('translation');
    Route::post('/translation/add','AdminController@translationAdd')->name('translation.add');
    Route::get('/translation/delete/{id}','AdminController@translationDelete')->name('translation.delete');
    Route::post('/translation/edit','AdminController@translationEdit')->name('translation.edit');
    Route::get('/video','AdminController@Video')->name('video');

//    Admin Settings
    Route::post('/save-video','AdminController@saveVideo')->name('save-video');

//    Gallery
    Route::get('/photo-gallery','AdminController@PhotoGalleryIndex')->name('photo-gallery-index');
    Route::post('/photo-gallery/save','AdminController@PhotoGallerySave')->name('photo-gallery-save');
    Route::get('/photo-gallery/delete/{id}','AdminController@PhotoGalleryDelete')->name('gallery-delete');
    Route::post('/photo-gallery/changeOrder','AdminController@PhotoGalleryChangeOrder')->name('gallery-change-order');


    //    Gallery
    Route::get('/partners-gallery','AdminController@PartnersIndex')->name('partners-index');
    Route::post('/partners-gallery/save','AdminController@PartnersSave')->name('partners-save');
    Route::get('/partners-gallery/delete/{id}','AdminController@PartnersDelete')->name('partners-delete');
    Route::post('/partners-gallery/changeOrder','AdminController@PartnersChangeOrder')->name('partners-change-order');

    // aboutus
    Route::get('/about-us','AdminController@AboutUs')->name('about.us');
    Route::post('/about-us/store','AdminController@AboutUsUpdate')->name('about.store');

    Route::get('about-us-icons','AdminController@AboutUsIcons')->name('about.us.icons');
    Route::get('add-about-icon','AdminController@AddAboutUsIcon')->name('about.us.icons.add');
    Route::post('save-about-icon','AdminController@SaveAboutIcon')->name('save.about.icons');
    Route::get('/edit-about-icon/{id}','AdminController@EditAboutIcon')->name('edit.about.icon');
    Route::post('/update-about-icon/{id}','AdminController@UpdateAboutIcon')->name('update.about.icon');
    Route::get('/delete-about-us-icon/{id}','AdminController@DeleteAboutUsIcon')->name('delete.about.us.icon');
});

Route::group(['prefix' => '{locale?}','middleware' => 'locale'], function () {
    Route::get('/', 'SiteController@index')->name('index');
    Route::get('/home', 'HomeController@index')->name('home');
});


