<!DOCTYPE html>
<html>
<head>
    <title>Page Title</title>




    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/revolution-slider.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="js/respond.js"></script><![endif]-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="module" crossorigin="" src='{{asset("/js/index-ea3b161f.js")}}'></script>
    <link rel="stylesheet" href="{{'/css/index-36cb45f6.css'}}">
    {{--    <link rel="preconnect" href="https://assets.zyrosite.com" data-el-id="https://assets.zyrosite.com">--}}
    <title data-el-id="title">Home</title>
    <meta content="Home" property="og:title" data-el-id="og:title">
    <meta content="website" property="og:type" data-el-id="og:type">
    <meta content="https://anirazzui.zyrosite.com/" proeprty="og:url" data-el-id="og:url">
    <meta name="twitter:card" content="summary_large_image" data-el-id="twitter:card">
    <meta name="twitter:title" content="Home" data-el-id="twitter:title">
    <link rel="icon" href="data:;base64,iVBORw0KGgo=" data-el-id="favicon">
    <link rel="apple-touch-icon" href="data:;base64,iVBORw0KGgo=" data-el-id="apple-touch-icon">
    <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin="true" data-el-id="gstatic-preconnect">
    <link href="css2?family=Unna:wght@400&amp;family=Open+Sans:wght@300;400&amp;display=swap" rel="preload" as="style" data-el-id="google-fonts-preload">
    <link href="css2?family=Unna:wght@400&amp;family=Open+Sans:wght@300;400&amp;display=swap" rel="stylesheet" referrerpolicy="no-referrer" media="print" onload="if(!window._isAppPrerendering)this.removeAttribute('media');" data-el-id="google-fonts-stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script>window._isAppHydrating=true</script>
    <style>
        .floor-list img{
            max-width: 100%;
        }
        .magnific-img img {
            width: 100%;
            height: 240px;
            object-fit: cover;
            margin-bottom: 21px;
        }
        @media (min-width: 768px){
            .floor-list img{
                max-width: 800px;
            }
        }
        @media (max-width: 768px){
            .magnific-img{
                width: 100%;
            }
        }
    </style>
</head>

<body>
<div class="page-wrapper">
    <div id="app" data-server-rendered="true">
        <div></div>
        <main class="page" style="--h1-color:var(--colors-dark); --h1-font-size:50px; --h1-font-style:normal; --h1-font-family:var(--font-primary); --h1-font-weight:400; --h1-line-height:1.2; --h1-m-font-size:48px; --h1-letter-spacing:normal; --h1-text-transform:none; --h1-text-decoration:none; --h2-color:var(--colors-dark); --h2-font-size:64px; --h2-font-style:normal; --h2-font-family:var(--font-primary); --h2-font-weight:400; --h2-line-height:1.2; --h2-m-font-size:40px; --h2-letter-spacing:normal; --h2-text-transform:none; --h2-text-decoration:none; --h3-color:var(--colors-dark); --h3-font-size:56px; --h3-font-style:normal; --h3-font-family:var(--font-primary); --h3-font-weight:400; --h3-line-height:1.2; --h3-m-font-size:32px; --h3-letter-spacing:normal; --h3-text-transform:none; --h3-text-decoration:none; --h4-color:var(--colors-dark); --h4-font-size:40px; --h4-font-style:normal; --h4-font-family:var(--font-primary); --h4-font-weight:400; --h4-line-height:1.2; --h4-m-font-size:30px; --h4-letter-spacing:normal; --h4-text-transform:none; --h4-text-decoration:none; --h5-color:var(--colors-dark); --h5-font-size:24px; --h5-font-style:normal; --h5-font-family:var(--font-primary); --h5-font-weight:400; --h5-line-height:1.2; --h5-m-font-size:24px; --h5-letter-spacing:normal; --h5-text-transform:none; --h5-text-decoration:none; --h6-color:var(--colors-dark); --h6-font-size:16px; --h6-font-style:normal; --h6-font-family:var(--font-primary); --h6-font-weight:400; --h6-line-height:1.2; --h6-m-font-size:16px; --h6-letter-spacing:normal; --h6-text-transform:none; --h6-text-decoration:none; --body-color:var(--colors-dark); --body-font-size:12px; --body-font-style:normal; --body-font-family:var(--font-secondary); --body-font-weight:400; --body-line-height:1.5; --body-m-font-size:16px; --body-letter-spacing:normal; --body-text-transform:normal; --body-text-decoration:none; --font-primary:&quot;Unna&quot;, serif; --font-secondary:&quot;Open Sans&quot;, sans-serif; --colors-dark:#CB9C57; --colors-light:#FFFFFF; --colors-danger:#f86c6b; --colors-primary:#CB9C57; --colors-success:#4dbd74; --colors-warning:#ffc107; --colors-accent-1:#117df9; --colors-accent-2:#b2f7ef; --colors-grey-100:#e3e3e3; --colors-grey-200:#e5e5e5; --colors-grey-300:#4d4d4d; --colors-grey-400:#8f9ca3; --colors-grey-500:#73848c; --colors-grey-600:#5c6970; --colors-grey-700:#454f54; --colors-grey-800:#2e3538; --colors-grey-900:#2e3538; --colors-secondary:#826C49; --colors-primary-dark:#b2833e; --colors-accent-1-dark:#034c9f; --colors-accent-2-dark:#80cbc3; --colors-primary-light:#dfb06b; --colors-accent-1-light:#8cbaee; --colors-accent-2-light:#e4faf8; --colors-primary-accent:#d5a661; --colors-secondary-dark:#695330; --colors-accent-1-accent:#1c80d0; --colors-accent-2-accent:#61ebdb; --colors-secondary-light:#96805d; --colors-secondary-accent:#8c7653; --nav-link-color:var(--colors-dark); --nav-link-font-size:16px; --nav-link-font-style:normal; --nav-link-color-hover:var(--colors-secondary-dark); --nav-link-font-family:var(--font-primary); --nav-link-font-weight:400; --nav-link-line-height:1.35; --nav-link-m-font-size:16px; --nav-link-letter-spacing:normal; --nav-link-text-transform:none; --nav-link-text-decoration:none; --body-large-color:var(--colors-dark); --body-large-font-size:24px; --body-large-font-style:normal; --body-large-font-family:var(--font-secondary); --body-large-font-weight:300; --body-large-line-height:1.5; --body-large-m-font-size:24px; --body-large-letter-spacing:normal; --body-large-text-transform:none; --body-large-text-decoration:none; --body-small-color:var(--colors-dark); --body-small-font-size:14px; --body-small-font-style:normal; --body-small-font-family:var(--font-secondary); --body-small-font-weight:300; --body-small-line-height:1.5; --body-small-m-font-size:14px; --body-small-letter-spacing:normal; --body-small-text-transform:none; --body-small-text-decoration:none; --grid-button-primary-color:var(--colors-light); --grid-button-primary-font-size:16px; --grid-button-primary-padding-x:50px; --grid-button-primary-padding-y:15px; --grid-button-primary-font-style:normal; --grid-button-primary-color-hover:var(--colors-dark); --grid-button-primary-font-family:var(--font-secondary); --grid-button-primary-font-weight:300; --grid-button-primary-line-height:1.35; --grid-button-primary-m-font-size:16px; --grid-button-primary-m-padding-x:50px; --grid-button-primary-m-padding-y:15px; --grid-button-primary-border-color:transparent; --grid-button-primary-border-width:0; --grid-button-primary-box-shadow-x:0px; --grid-button-primary-box-shadow-y:0px; --grid-button-primary-border-radius:0; --grid-button-primary-letter-spacing:normal; --grid-button-primary-text-transform:none; --grid-button-primary-box-shadow-blur:0px; --grid-button-primary-m-border-radius:0; --grid-button-primary-text-decoration:none; --grid-button-primary-background-color:var(--colors-dark); --grid-button-primary-box-shadow-color:transparent; --grid-button-primary-box-shadow-spread:0px; --grid-button-primary-border-color-hover:transparent; --grid-button-primary-border-width-hover:0px; --grid-button-primary-box-shadow-x-hover:0px; --grid-button-primary-box-shadow-y-hover:0px; --grid-button-primary-transition-duration:0.2s; --grid-button-primary-box-shadow-blur-hover:0px; --grid-button-primary-background-color-hover:var(--colors-grey-100); --grid-button-primary-box-shadow-color-hover:0px; --grid-button-primary-box-shadow-spread-hover:0px; --grid-button-primary-transition-timing-function:ease; --grid-button-secondary-color:var(--colors-light); --grid-button-secondary-font-size:16px; --grid-button-secondary-padding-x:50px; --grid-button-secondary-padding-y:15px; --grid-button-secondary-font-style:normal; --grid-button-secondary-color-hover:var(--colors-light); --grid-button-secondary-font-family:var(--font-secondary); --grid-button-secondary-font-weight:400; --grid-button-secondary-line-height:1.35; --grid-button-secondary-m-font-size:13px; --grid-button-secondary-m-padding-x:40px; --grid-button-secondary-m-padding-y:10px; --grid-button-secondary-border-color:transparent; --grid-button-secondary-border-width:0; --grid-button-secondary-box-shadow-x:0px; --grid-button-secondary-box-shadow-y:0px; --grid-button-secondary-border-radius:0; --grid-button-secondary-letter-spacing:normal; --grid-button-secondary-text-transform:none; --grid-button-secondary-box-shadow-blur:0px; --grid-button-secondary-m-border-radius:0; --grid-button-secondary-text-decoration:none; --grid-button-secondary-background-color:var(--colors-dark); --grid-button-secondary-box-shadow-color:transparent; --grid-button-secondary-box-shadow-spread:0px; --grid-button-secondary-border-color-hover:transparent; --grid-button-secondary-border-width-hover:0px; --grid-button-secondary-box-shadow-x-hover:0px; --grid-button-secondary-box-shadow-y-hover:0px; --grid-button-secondary-transition-duration:0.2s; --grid-button-secondary-box-shadow-blur-hover:0px; --grid-button-secondary-background-color-hover:var(--colors-grey-300); --grid-button-secondary-box-shadow-color-hover:0px; --grid-button-secondary-box-shadow-spread-hover:0px; --grid-button-secondary-transition-timing-function:ease;">
            <header data-v-627f5f8b="" class="block-header" height="165.734375" style="--width:1240px; --padding-top:25px; --padding:25px 16px 25px 16px; --padding-right:16px; --padding-bottom:25px; --padding-left:16px; --logo-width:64px; --cartIconSize:24px; --m-logo-width:64px; --oldContentWidth:1600px; --menu-item-spacing:55px; --space-between-menu:24px; --contrastBackgroundColor:#dcdcdc; --background-color:var(--colors-light);">
                <div data-v-627f5f8b="" class="background" style="--background-color:var(--colors-light);"></div>
                <div data-v-627f5f8b="" class="block-header__content" style="--navigation-grid-template-columns:minmax(calc(var(--logo-width) + var(--space-between-menu)), auto) 1fr; --m-navigation-grid-template-columns:minmax(calc(var(--m-logo-width, var(--logo-width)) + var(--space-between-menu)), auto) 1fr; --logo-grid-row:1/2; --m-logo-grid-row:1/2; --logo-grid-column:1/2; --m-logo-grid-column:1/2; --logo-justify-self:flex-start; --m-logo-justify-self:flex-start; --logo-image-object-position:left center; --m-logo-image-object-position:left center; --links-grid-row:1/2; --m-links-grid-row:1/2; --links-grid-column:2/3; --m-links-grid-column:2/3; --cart-grid-row:1/2; --cart-grid-column:3/3; --logo-spacing:0 var(--space-between-menu) 0 0; --m-logo-spacing:0 var(--space-between-menu) 0 0; --navigation-text-align:right; --m-navigation-text-align:right; --navigation-justify-self:flex-end; --m-navigation-justify-self:flex-end; --mobileBackground:var(--background-color, var(--background-image)); --dropdown-background-color:var(--colors-light); --m-dropdown-background-color:var(--contrastBackgroundColor);">
                    <a data-v-1be3674e="" data-v-627f5f8b="" href="index.htm" class="block-header-logo block-header__logo" style="--height:nullpx; --m-height:nullpx; --width:64px; --m-width:64px;">

                        <img data-v-1be3674e="" src="{{asset('uploads/'.$settings->logo)}}" class="block-header-logo__image" data-qa="builder-siteheader-img-logo">
                    </a>
                    <button data-v-9858e16e="" data-v-627f5f8b="" type="button" title="Menu" class="burger block-header__burger" data-qa="builder-siteheader-btn-hamburger" style="--burger-color:var(--nav-link-color);">
                        <span data-v-9858e16e="" class="burger__bun"></span>
                        <span data-v-9858e16e="" class="burger__meat"></span>
                        <span data-v-9858e16e="" class="burger__bun"></span>
                    </button>
                    <nav data-v-627f5f8b="" class="block-header__nav">
                        <ul data-v-627f5f8b="" class="block-header__links block-header__links--align-right" data-qa="builder-siteheader-emptyspace">
                            <li data-v-ffd27af7="" data-v-627f5f8b="" class="block-header-item">
                                <label data-v-ffd27af7="" class="block-header-item__label">
                                    <div data-v-9428c2e2="" data-v-ffd27af7="" class="block-header-item__item item-content-wrapper item-content-wrapper--active" data-qa="navigation-item-home">
                                        <a data-id="sales-hall-section" data-v-9428c2e2="" href="#" class="item-content" data-qa="navigationblock-page-active-home">{{getlang('Salles Hall')}}</a>
                                    </div>
                                </label>
                            </li>
                            <li data-v-ffd27af7="" data-v-627f5f8b="" class="block-header-item">
                                <label data-v-ffd27af7="" class="block-header-item__label">
                                    <div data-v-9428c2e2="" data-v-ffd27af7="" class="block-header-item__item item-content-wrapper" data-qa="navigation-item-aboutus">
                                        <a data-v-9428c2e2="" href="#" data-id="aboutus" class="item-content" data-qa="navigationblock-page-aboutus">{{getlang('About US')}}</a>
                                    </div>
                                </label>
                            </li>
                            <li data-v-ffd27af7="" data-v-627f5f8b="" class="block-header-item">
                                <label data-v-ffd27af7="" class="block-header-item__label">
                                    <div data-v-9428c2e2="" data-v-ffd27af7="" class="block-header-item__item item-content-wrapper" data-qa="navigation-item-ourportfolio">
                                        <a data-v-9428c2e2="" href="#" data-id="gallery" class="item-content" data-qa="navigationblock-page-ourportfolio">{{getlang('Gallery')}}</a>
                                    </div>
                                </label>
                            </li>
                            <li data-v-ffd27af7="" data-v-627f5f8b="" class="block-header-item">
                                <label data-v-ffd27af7="" class="block-header-item__label">
                                    <div data-v-9428c2e2="" data-v-ffd27af7="" class="block-header-item__item item-content-wrapper" data-qa="navigation-item-contactus">
                                        <a data-v-9428c2e2="" data-id="contactus" class="item-content" data-qa="navigationblock-page-contactus">{{getlang('Contacts')}}</a>
                                    </div>
                                </label>
                            </li>
                            <li>
                                <div class="nav-wrapper">
                                    <div class="sl-nav">
                                        <ul>
                                            <li><b>{{Lang()=='am' ? "AM" : (Lang()=='ru' ? "RU" : "EN") }}</b> <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                <div class="triangle"></div>
                                                <ul>
                                                    <li style="{{Lang()=='am' ? 'display:none':""}} "><i class="sl-flag flag-am"><div id="armenia"></div></i> <a href="{{route('index','am')}}" class="active">Armenian</a></li>
                                                    <li style="{{Lang()=='ru' ? 'display:none':""}} "><i class="sl-flag flag-ru"><div id="russion"></div></i> <a href="{{route('index','ru')}}" class="active">Russian</a></li>
                                                    <li style="{{Lang()=='en' ? 'display:none':""}} "><i class="sl-flag flag-en"><div id="english"></div></i> <a href="{{route('index','en')}}" class="active">English</a></li>

                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </nav>
                </div>
            </header>

            <section data-v-ac250832="" class="block block--grid" style="--rows:10; --width:1224px; --row-gap:16px; --row-size:48px; --column-gap:24px; --block-padding-top:15px; --block-padding:15px 0px 15px 0px; --block-padding-right:0px; --block-padding-bottom:15px; --block-padding-left:0px; --m-block-padding:104px 16px 134px 16px; --oldContentWidth:1600px; --grid-gap-history:16px 24px;">
                <div data-v-ac250832="" class="block-background block-background--fixed" style="--background-color:transparent; --background-overlay-opacity:0;">
                    <video autoplay="" class="block-background__image block-background__image--fixed" muted="" style="width:100%;object-fit: cover" preload="none">
                        <source src="{{asset($video->value)}}">
                    </video>
                </div>
                <div data-v-ac250832="" class="block-layout block-layout--layout" blocks="[object Object]" ecommerce-translations="[object Object]" id="Y5roTxZ8Mt" currentlocale="system" style="--m-max-width:414px; --max-width:1224px; --m-padding:0 16px; --grid-template-rows:minmax(313px, auto) minmax(134px, auto) minmax(16px, auto) minmax(29px, auto) minmax(35px, auto) minmax(52px, auto) 1fr; --grid-template-columns:19.1176% 12.9085% 16.9935% 50.9804%; --block-min-height:658px; --m-grid-template-rows:minmax(104px, auto) minmax(77px, auto) minmax(13px, auto) minmax(29px, auto) minmax(16px, auto) minmax(38px, auto) 1fr; --m-grid-template-columns:48.4293% 51.5707%; --m-block-min-height:411px;">
                    <div class="layout-element layout-element layout-element--layout" style="--text:left; --align:flex-start; --justify:flex-end; --m-element-margin:0 0 13px 0; --z-index:1; --grid-row:2/3; --grid-column:1/4; --m-grid-row:2/3; --m-grid-column:1/3;">
                        <div class="text-box layout-element__component layout-element__component--GridTextBox" id="eno7Xp9Jl" lcp="[object Object]" element-width="600" element-height="134" mobile-element-width="382" mobile-element-height="77" data-qa="gridtextbox:eno7xp9jl">
                            <h3>
                            </h3>
                        </div>
                    </div>
                    <div class="layout-element layout-element layout-element--layout" style="--text:left; --align:flex-start; --justify:flex-start; --m-element-margin:0 0 16px 0; --z-index:2; --grid-row:4/5; --grid-column:1/3; --m-grid-row:4/5; --m-grid-column:1/3;">
                    </div>
                    <div class="layout-element layout-element layout-element--layout" style="--align:flex-start; --justify:center; --m-element-margin:0 0 16px 0; --grid-button-secondary-background-color:#CB9C57; --grid-button-secondary-background-color-hover:#826C49; --grid-button-secondary-background-color-active:#CB9C57; --z-index:3; --grid-row:6/7; --grid-column:1/2; --m-grid-row:6/7; --m-grid-column:1/2;">
                    </div>
                </div>
            </section>
            <section id="sales-hall-section" class="gallery-section full-width">
                <div class="auto-container">

                    <!--Section Title-->


                    <div class="sec-title" style="margin-top: 81px;margin-bottom: 0;">
                        <h2>{{getlang('Salles Hall')}}  </h2>
                    </div>
                    <!--Filter-->

                </div>

                <div class="container" id="layouts">
                    <div class="row">

                        <div class="col-md-12">
                            @foreach($layouts as $key => $layout)
                                <div class="floor-list" id="floor_{{$layout->id}}" @if($key>0) style="display: none" @endif>
                                    <img data-id="{{$layout->id}}" class="mapImages" id="floor{{$layout->id}}"
                                         src="{{asset('uploads/'.$layout->image)}}"
                                         alt=""
                                         usemap="#floor{{$layout->id}}">
                                    {!! str_replace('name=""',"name='floor$layout->id'",$layout->start) !!}
                                    @if($layout->details->count()>0)
                                        @foreach($layout->details as $detail)
                                            @php
                                                $d = $detail->detail;

                                                $rand = str_random(9);
                                                $d = str_replace('name=""',"name='$rand'",$d);
                                               $d = str_replace('data-bussy=""',"data-bussy='$detail->busy' data-ids='$detail->id' data-image='".asset('uploads/'.$detail->image)."'",$d);
                                            @endphp
                                            {!! $d !!}
                                        @endforeach
                                    @endif
                                    {!! $layout->end !!}
                                </div>
                            @endforeach
                            <nav aria-label="Page navigation example" style="display: flex; padding: 20px">
                                <ul class="pagination layoutsPagination" style="margin: 0 auto">
                                    @foreach($layouts as $key => $item)
                                        <li data-id="{{$item->id}}" class="page-item {{$key==0?'active':""}}"><a
                                                class="page-link" href="#">{{$item->floor}}</a></li>
                                    @endforeach
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

            </section>


            <section data-v-ac250832=""  class="block block--grid" id="aboutus" style="--rows:6; --width:1224px; --row-gap:16px; --row-size:48px; --column-gap:24px; --block-padding-top:80px; --block-padding:80px 0px 80px 0px; --block-padding-right:0px; --block-padding-bottom:80px; --block-padding-left:0px; --m-block-padding:40px 16px 40px 16px; --oldContentWidth:1600px;">
                <div data-v-ac250832="" class="block-background block-background--fixed" style="--background-color:transparent; --background-overlay-opacity:0;">
                    <img alt="" src="https://assets.zyrosite.com/cdn-cgi/image/format=auto,w=1920,fit=crop/anirazzui/creative-architect-projecting-big-drawings-dark-loft-office-cafe-with-dark-retro-style-YleXl8q8k6F13oro.jpg" srcset="https://assets.zyrosite.com/cdn-cgi/image/format=auto,w=360,fit=crop/anirazzui/creative-architect-projecting-big-drawings-dark-loft-office-cafe-with-dark-retro-style-YleXl8q8k6F13oro.jpg 360w,https://assets.zyrosite.com/cdn-cgi/image/format=auto,w=720,fit=crop/anirazzui/creative-architect-projecting-big-drawings-dark-loft-office-cafe-with-dark-retro-style-YleXl8q8k6F13oro.jpg 720w,https://assets.zyrosite.com/cdn-cgi/image/format=auto,w=945,fit=crop/anirazzui/creative-architect-projecting-big-drawings-dark-loft-office-cafe-with-dark-retro-style-YleXl8q8k6F13oro.jpg 945w,https://assets.zyrosite.com/cdn-cgi/image/format=auto,w=1080,fit=crop/anirazzui/creative-architect-projecting-big-drawings-dark-loft-office-cafe-with-dark-retro-style-YleXl8q8k6F13oro.jpg 1080w,https://assets.zyrosite.com/cdn-cgi/image/format=auto,w=1440,fit=crop/anirazzui/creative-architect-projecting-big-drawings-dark-loft-office-cafe-with-dark-retro-style-YleXl8q8k6F13oro.jpg 1440w,https://assets.zyrosite.com/cdn-cgi/image/format=auto,w=2880,fit=crop/anirazzui/creative-architect-projecting-big-drawings-dark-loft-office-cafe-with-dark-retro-style-YleXl8q8k6F13oro.jpg 2880w,https://assets.zyrosite.com/cdn-cgi/image/format=auto,w=1920,fit=crop/anirazzui/creative-architect-projecting-big-drawings-dark-loft-office-cafe-with-dark-retro-style-YleXl8q8k6F13oro.jpg 1920w,https://assets.zyrosite.com/cdn-cgi/image/format=auto,w=3840,fit=crop/anirazzui/creative-architect-projecting-big-drawings-dark-loft-office-cafe-with-dark-retro-style-YleXl8q8k6F13oro.jpg 3840w" width="100vw" sizes="100vw" loading="lazy" class="block-background__image block-background__image--fixed">
                </div>
                <div data-v-ac250832="" class="block-layout block-layout--layout" blocks="[object Object]" ecommerce-translations="[object Object]" id="Sk_WZEq1Nz" currentlocale="system" style="--m-max-width:414px; --max-width:1224px; --m-padding:0 16px; --grid-template-rows:minmax(159px, auto) minmax(18px, auto) minmax(31px, auto) minmax(96px, auto) minmax(62px, auto) 1fr; --grid-template-columns:8.49673% 32.0261% 8.49673% 10.4575% 32.0261% 8.49673%; --block-min-height:528px; --m-grid-template-rows:minmax(40px, auto) minmax(24px, auto) minmax(16px, auto) minmax(36px, auto) minmax(16px, auto) minmax(248px, auto) 1fr; --m-grid-template-columns:100%; --m-block-min-height:420px;">
                    <div class="layout-element layout-element layout-element--layout" style="--text:left; --align:flex-start; --justify:center; --m-element-margin:0 0 16px 0; --z-index:1; --grid-row:2/3; --grid-column:2/4; --m-grid-row:2/3; --m-grid-column:1/2;">
                        <div class="text-box layout-element__component layout-element__component--GridTextBox" id="1PStElJal" lcp="[object Object]" element-width="496" element-height="18" mobile-element-width="382" mobile-element-height="24" data-qa="gridtextbox:1psteljal">
                            <div class="body">
                                <h1 style="color: var(--colors-light);">{{getlang('About us')}}<br style="color: var(--colors-light);"></h1>
                            </div>
                        </div>
                    </div>

                    <div class="layout-element layout-element layout-element--layout" style="--text:left; --align:flex-start; --justify:flex-start; --m-element-margin:0 0 16px 0; --z-index:3; --grid-row:4/6; --grid-column:5/6; --m-grid-row:6/7; --m-grid-column:1/2;">
                        <div class=" text-box layout-element__component layout-element__component--GridTextBox" id="VUTdqdJr7z" lcp="[object Object]" element-width="392" element-height="158" mobile-element-width="382" mobile-element-height="248" data-qa="gridtextbox:vutdqdjr7z">
                            <div style="color:white;font-size: 16px">
                                {{$about->text}}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="properties-section" >
                <div class="auto-container">


                    <div class="five-col-theme">
                        <div class="row clearfix">
                            @foreach($aboutIcons as $icon)
                            <!--Column-->
                            <article class="column">
                                <div class="inner-box">
                                    <div class="icon"><span class="{{$icon->icon}}"></span></div>
                                    <h4 class="title">{{$icon->text}}</h4>
                                </div>
                            </article>
                                @endforeach



                        </div>
                    </div>

                </div>
            </section>

            <div class="container  ">

                <div class="sec-title" id="gallery" style="margin-top: 81px;margin-bottom: 0;">
                    <h2>{{getlang('Gallery')}}  </h2>
                </div>

                <div>
                    <section class="img-gallery-magnific">

                        @foreach($gallery as $key => $image)

                            <div class="magnific-img">
                                <a class="image-popup-vertical-fit" href="{{asset($image->image)}}" title="{{'Gallery Image' . $key}}">
                                    <img src="{{asset($image->image)}}" alt="{{'Gallery Image ' . $key}}" />
                                </a>
                            </div>
                        @endforeach

                    </section>
                    <div class="clear"></div>
                </div>

            </div>




{{--            <section data-v-ac250832="" class="block block--grid" style="--cols:12; --rows:10; --width:1224px; --m-rows:1; --col-gap:24px; --row-gap:16px; --row-size:48px; --column-gap:24px; --block-padding-top:0; --block-padding:0; --block-padding-right:0; --block-padding-bottom:0; --block-padding-left:0; --m-block-padding:40px 16px 40px 16px; --oldContentWidth:1600px;">--}}
{{--                <div data-v-ac250832="" class="block-background" style="--background-color:transparent; --background-overlay-opacity:0;"></div>--}}
{{--                <div data-v-ac250832="" class="block-layout block-layout--layout" blocks="[object Object]" ecommerce-translations="[object Object]" id="1aZjkF2R4U" currentlocale="system" style="--m-max-width:414px; --max-width:1224px; --m-padding:0 16px; --grid-template-rows:minmax(64px, auto) minmax(48px, auto) minmax(16px, auto) minmax(21px, auto) minmax(43px, auto) minmax(368px, auto) 1fr; --grid-template-columns:32.0261% 8.49673% 1.96078% 6.53595% 50.9804%; --block-min-height:624px; --m-grid-template-rows:minmax(40px, auto) minmax(36px, auto) minmax(24px, auto) minmax(21px, auto) minmax(24px, auto) minmax(154px, auto) minmax(16px, auto) minmax(283px, auto) 1fr; --m-grid-template-columns:100%; --m-block-min-height:639px;">--}}
{{--                    <div class="layout-element layout-element layout-element--layout" style="--text:left; --align:flex-start; --justify:center; --m-element-margin:0 0 24px 0; --z-index:1; --grid-row:2/3; --grid-column:1/5; --m-grid-row:2/3; --m-grid-column:1/2;">--}}
{{--                        <div class="text-box layout-element__component layout-element__component--GridTextBox" id="OkWOQOL7e" lcp="[object Object]" element-width="600" element-height="48" mobile-element-width="382" mobile-element-height="36" data-qa="gridtextbox:okwoqol7e">--}}
{{--                            <h4>--}}
{{--                                <span style="color: var(--colors-grey-800)">Our office</span>--}}
{{--                            </h4>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="layout-element layout-element layout-element--layout" style="--text:left; --align:flex-start; --justify:flex-start; --m-element-margin:0 0 24px 0; --z-index:2; --grid-row:4/5; --grid-column:1/2; --m-grid-row:4/5; --m-grid-column:1/2;">--}}
{{--                        <div class="text-box layout-element__component layout-element__component--GridTextBox" id="pmrdfVUexI" lcp="[object Object]" element-width="392" element-height="21" mobile-element-width="382" mobile-element-height="21" data-qa="gridtextbox:pmrdfvuexi">--}}
{{--                            <p class="body-small">--}}
{{--                                <span style="color: rgb(0, 0, 0)">19 Cecil Street, #04-00, Singapore 049704</span>--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="layout-element layout-element layout-element--layout" style="--align:center; --justify:center; --m-element-margin:0 0 16px 0; --z-index:3; --grid-row:6/7; --grid-column:4/6; --m-grid-row:6/7; --m-grid-column:1/2;">--}}
{{--                        <div data-v-b5563d9d="" class="grid-map grid-map--loading layout-element__component layout-element__component--GridMap" id="2OGEDJP3Bw" lcp="[object Object]" element-width="704" element-height="368" mobile-element-width="382" mobile-element-height="154" data-qa="gridmap:2ogedjp3bw">--}}
{{--                            <iframe data-v-b5563d9d="" width="100%" height="100%" class="grid-map__frame"></iframe>--}}
{{--                            <div data-v-b5563d9d="" class="grid-map__pin"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="layout-element layout-element layout-element--layout" style="--align:center; --justify:center; --z-index:4; --grid-row:6/7; --grid-column:1/3; --m-grid-row:8/9; --m-grid-column:1/2;">--}}
{{--                        <div data-v-fe884efd="" class="image-wrapper layout-element__component layout-element__component--GridImage" data-qa="gridimage:r7_p3dcttt">--}}
{{--                            <div data-v-9b0f33c3="" data-v-fe884efd="" target="_self" title="" class="image image-wrapper--desktop image--grid" id="R7_p3dCTTt">--}}
{{--                                <img data-v-9b0f33c3="" alt="" src="/images/photo-1568992687947-868a62a9f521?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjcyNTgzfQ&amp;w=496&amp;q=70&amp;auto=format&amp;fit=crop&amp;h=368" srcset="photo-1568992687947-868a62a9f521-1 328w?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjcyNTgzfQ&amp;w=328&amp;q=70&amp;auto=format&amp;fit=crop&amp;h=243 328w,photo-1568992687947-868a62a9f521-2 656w?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjcyNTgzfQ&amp;w=656&amp;q=70&amp;auto=format&amp;fit=crop&amp;h=487 656w,photo-1568992687947-868a62a9f521-3 861w?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjcyNTgzfQ&amp;w=861&amp;q=70&amp;auto=format&amp;fit=crop&amp;h=639 861w,photo-1568992687947-868a62a9f521-4 984w?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjcyNTgzfQ&amp;w=984&amp;q=70&amp;auto=format&amp;fit=crop&amp;h=730 984w,photo-1568992687947-868a62a9f521 496w?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjcyNTgzfQ&amp;w=496&amp;q=70&amp;auto=format&amp;fit=crop&amp;h=368 496w,photo-1568992687947-868a62a9f521-5 992w?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjcyNTgzfQ&amp;w=992&amp;q=70&amp;auto=format&amp;fit=crop&amp;h=736 992w" sizes="(min-width: 920px) 496px, calc(100vw - 0px)" height="368" width="496" loading="lazy" class="image__image" data-qa="builder-gridelement-gridimage">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </section>--}}


            <section data-v-ac250832="" class="block block--grid block--footer" id="contactus" style="--cols:12; --rows:10; --width:1224px; --m-rows:1; --col-gap:24px; --row-gap:16px; --row-size:48px; --column-gap:24px; --block-padding-top:0; --block-padding:0; --block-padding-right:0; --block-padding-bottom:0; --block-padding-left:0; --m-block-padding:40px 16px 35px 16px; --oldContentWidth:1600px;">
                <div data-v-ac250832="" class="block-background" style="--background-color:var(--colors-light); --background-overlay-opacity:0;"></div>
                <div data-v-ac250832="" class="block-layout block-layout--layout" blocks="[object Object]" ecommerce-translations="[object Object]" id="Cx6ok1mdtw" currentlocale="system" style="--m-max-width:414px; --max-width:1224px; --m-padding:0 16px; --grid-template-rows:minmax(98px, auto) minmax(30px, auto) minmax(18px, auto) minmax(74px, auto) minmax(69px, auto) minmax(66px, auto) minmax(69px, auto) minmax(91px, auto) minmax(20px, auto) minmax(50px, auto) 1fr; --grid-template-columns:10.1307% 21.8954% 8.49673% 10.4575% 49.0196%; --block-min-height:649px; --m-grid-template-rows:minmax(40px, auto) minmax(36px, auto) minmax(24px, auto) minmax(69px, auto) minmax(24px, auto) minmax(69px, auto) minmax(32px, auto) minmax(20px, auto) minmax(26px, auto) minmax(482px, auto) 1fr; --m-grid-template-columns:32.4607% 67.5393%; --m-block-min-height:857px;">
                    <div class="layout-element layout-element layout-element--layout" style="--text:left; --align:flex-start; --justify:center; --m-element-margin:0 0 24px 0; --z-index:1; --grid-row:2/4; --grid-column:1/4; --m-grid-row:2/3; --m-grid-column:1/3;">
                        <div class="text-box layout-element__component layout-element__component--GridTextBox" id="Sac2wndxK" lcp="[object Object]" element-width="496" element-height="48" mobile-element-width="382" mobile-element-height="36" data-qa="gridtextbox:sac2wndxk">
                            <h4>
                                <span style="color: rgb(0, 0, 0)">{{getlang('Get in touch')}}</span>
                            </h4>
                        </div>
                    </div>
                    <div class="layout-element layout-element layout-element--layout" style="--text:left; --align:flex-start; --justify:center; --m-element-margin:0 0 24px 0; --z-index:2; --grid-row:5/6; --grid-column:1/3; --m-grid-row:4/5; --m-grid-column:1/3;">
                        <div class="text-box layout-element__component layout-element__component--GridTextBox" id="kYdg2rlwWB" lcp="[object Object]" element-width="392" element-height="69" mobile-element-width="382" mobile-element-height="69" data-qa="gridtextbox:kydg2rlwwb">
                            <h6>
                                <span style="color: rgb(0, 0, 0)">{{getlang('Address')}}</span>
                            </h6>
                            <p class="body-small">
                                <span style="color: rgb(0, 0, 0)">{{getlang('AddressText')}}<br></span>
                            </p>
                        </div>
                    </div>
                    <div class="layout-element layout-element layout-element--layout" style="--text:left; --align:flex-start; --justify:center; --m-element-margin:0 0 32px 0; --z-index:3; --grid-row:7/8; --grid-column:1/3; --m-grid-row:6/7; --m-grid-column:1/3;">
                        <div class="text-box layout-element__component layout-element__component--GridTextBox" id="Xs52XLJAnj" lcp="[object Object]" element-width="392" element-height="69" mobile-element-width="382" mobile-element-height="69" data-qa="gridtextbox:xs52xljanj">
                            <h6>
                                <span style="color: rgb(0, 0, 0)">{{getlang('Contact')}}</span>
                            </h6>
                            <p class="body-small">
                                <span style="color: rgb(0, 0, 0)">{{$settings->phone}}<br>{{$settings->email}}<br></span>
                            </p>
                        </div>
                    </div>
                    <div class="layout-element layout-element layout-element--layout" style="--icon-size:20px; --icon-color:#000000; --icon-spacing:space-around; --icon-direction:row; --icon-color-hover:#3a3a3a; --m-element-margin:0 0 26px 0; --space-between-icons:32px; --z-index:4; --grid-row:9/10; --grid-column:1/2; --m-grid-row:8/9; --m-grid-column:1/2;">
                        <div data-v-c7cf8da7="" class="social-icons social-icons--row layout-element__component layout-element__component--GridSocialIcons" id="1_v5rdrHuE" lcp="[object Object]" element-width="124" element-height="20" mobile-element-width="124" mobile-element-height="20" data-qa="gridsocialicons:1_v5rdrhue" style="height: 100%; --space-between-icons:0px; --m-icon-direction:row; --icon-padding-vertical:0; --icon-padding-horizontal:var(--space-between-icons);">
                            <a data-v-c7cf8da7="" href="{{$settings->fb}}" target="_blank" rel="noopener" title="Go to Facebook page" class="social-icons__link">
                                <svg width="24" height="24" viewbox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M24 12.0726C24 5.44354 18.629 0.0725708 12 0.0725708C5.37097 0.0725708 0 5.44354 0 12.0726C0 18.0619 4.38823 23.0264 10.125 23.9274V15.5414H7.07661V12.0726H10.125V9.4287C10.125 6.42144 11.9153 4.76031 14.6574 4.76031C15.9706 4.76031 17.3439 4.99451 17.3439 4.99451V7.94612H15.8303C14.34 7.94612 13.875 8.87128 13.875 9.82015V12.0726H17.2031L16.6708 15.5414H13.875V23.9274C19.6118 23.0264 24 18.0619 24 12.0726Z" fill="currentColor"></path>
                                </svg>
                            </a>
                            <a data-v-c7cf8da7="" href="{{$settings->insta}}" target="_blank" rel="noopener" title="Go to Instagram page" class="social-icons__link">
                                <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 24 24" id="instagram-brands">
                                    <path d="M12.003 5.848A6.142 6.142 0 0 0 5.85 12a6.142 6.142 0 0 0 6.152 6.152A6.142 6.142 0 0 0 18.155 12a6.142 6.142 0 0 0-6.152-6.152Zm0 10.152c-2.2 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.8 4-4 4ZM19.84 5.596c0 .798-.642 1.435-1.435 1.435a1.435 1.435 0 1 1 1.435-1.435Zm4.075 1.457c-.091-1.922-.53-3.625-1.939-5.028C20.575.622 18.872.183 16.95.087c-1.981-.112-7.919-.112-9.9 0-1.917.091-3.62.53-5.027 1.933C.614 3.423.18 5.125.084 7.047c-.112 1.981-.112 7.92 0 9.9.091 1.922.53 3.625 1.939 5.028 1.408 1.403 3.105 1.842 5.027 1.938 1.981.112 7.919.112 9.9 0 1.922-.091 3.625-.53 5.027-1.938 1.403-1.403 1.842-3.106 1.939-5.028.112-1.98.112-7.913 0-9.894Zm-2.56 12.02a4.049 4.049 0 0 1-2.28 2.28c-1.58.627-5.328.483-7.073.483-1.746 0-5.499.139-7.073-.482a4.05 4.05 0 0 1-2.281-2.281c-.626-1.58-.482-5.328-.482-7.073s-.14-5.499.482-7.073a4.05 4.05 0 0 1 2.28-2.28c1.58-.627 5.328-.483 7.074-.483 1.745 0 5.498-.139 7.073.482a4.05 4.05 0 0 1 2.28 2.281c.627 1.58.482 5.328.482 7.073s.145 5.499-.482 7.073Z" fill="currentColor"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="layout-element layout-element layout-element--layout" style="--justify:center; --formSpacing:22px 10px; --m-element-margin:0 0 13px 0; --z-index:5; --grid-row:3/11; --grid-column:5/6; --m-grid-row:10/11; --m-grid-column:1/3;">
                        <div data-v-4a941d19="" id="ADRyjTcSgh" class="form layout-element__component layout-element__component--GridForm" lcp="[object Object]" element-width="600" element-height="457" mobile-element-width="382" mobile-element-height="482" data-qa="gridform:adryjtcsgh" style="--formButtonJustifySelf:start;">
                            <form data-v-4a941d19="" name="contactForm" class="form__control" id="contact-form" action="sendemail.php">
                                <div data-v-872cb3a9="" data-v-4a941d19="" class="input input--light">
                                    <label data-v-872cb3a9="" class="input__label input__label--light"> {{getlang('Name')}}* </label>
                                    <input data-v-872cb3a9="" placeholder="Your name" type="text" value="" tabindex="0" name="username" class="input__component input__component--light">
                                </div>
                                <div data-v-872cb3a9="" data-v-4a941d19="" class="input input--light">
                                    <label data-v-872cb3a9="" class="input__label input__label--light"> {{getlang('Last name')}} </label>
                                    <input data-v-872cb3a9="" placeholder="Subject" type="text" value="" tabindex="0"name="subject"  class="input__component input__component--light">
                                </div>
                                <div data-v-872cb3a9="" data-v-4a941d19="" class="input input--light">
                                    <label data-v-872cb3a9="" class="input__label input__label--light"> {{getlang('Your email')}}* </label>
                                    <input data-v-872cb3a9="" placeholder="Your email address" type="text" value="" tabindex="0" name="email"  class="input__component input__component--light">
                                </div>
                                <div data-v-872cb3a9="" data-v-4a941d19="" class="input input--light">
                                    <label data-v-872cb3a9="" class="input__label input__label--light"> {{getlang('Message')}}* </label>
                                    <textarea data-v-872cb3a9="" placeholder="Enter your message" type="text" value="" tabindex="0" name="message" class="input__component input__component--textarea input__component--light"></textarea>
                                </div>
                                <button data-v-36ac2cf0="" data-v-4a941d19="" type="submit"  name="submit" class="grid-button form__button grid-button--primary">{{getlang('Submit')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>





        {{--    <!--Main Slider-->--}}
        {{--    <section class="main-slider" id="slider-section">--}}
        {{--        <div class="video" id="home">--}}
        {{--            <video autoplay="" class="home__video" muted="" style="width:100%;object-fit: cover" playsinline>--}}
        {{--                <source src="{{asset($video->value)}}">--}}
        {{--            </video>--}}
        {{--            <div class="video__icons" style="display: none;">--}}
        {{--                <div class="video__pause"></div>--}}
        {{--                <div class="video__volume"></div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--    </section>--}}



        <!--Properties Section-->
            {{--    <section class="properties-section" id="aboutus">--}}
            {{--        <div class="auto-container">--}}

            {{--            <!--Section Title-->--}}
            {{--            <div class="sec-title">--}}
            {{--                @php($abouttext = explode(' ',getlang('About Us')))--}}

            {{--                <h2>{{$abouttext[0]}} <span class="theme_color">{{$abouttext[1]}}</span></h2>--}}

            {{--                <div class="text">--}}
            {{--                    {!! $about->text !!}--}}
            {{--                </div>--}}
            {{--            </div>--}}

            {{--            <div class="five-col-theme">--}}
            {{--                <div class="row clearfix">--}}

            {{--                    <!--Column-->--}}

            {{--                    <!--Column-->--}}
            {{--                    @foreach($aboutIcons  as $icon)--}}
            {{--                        <article class="col-md-4 col-sm-6 col-xs-12 column">--}}
            {{--                            <div class="inner-box">--}}
            {{--                                <div class="icon"><span class="{{$icon->icon}}"></span></div>--}}
            {{--                                <h4 class="title">{{$icon->text}}</h4>--}}
            {{--                            </div>--}}
            {{--                        </article>--}}
            {{--                    @endforeach--}}

            {{--                </div>--}}
            {{--            </div>--}}

            {{--        </div>--}}
            {{--    </section>--}}


            <section class="properties-section" id="aboutus">--}}
        {{--        <div class="auto-container">--}}

        {{--            <!--Section Title-->--}}
        {{--            <div class="sec-title">--}}
        {{--                @php($abouttext = explode(' ',getlang('About Us')))--}}

        {{--                <h2>{{$abouttext[0]}} <span class="theme_color">{{$abouttext[1]}}</span></h2>--}}

        {{--                <div class="text">--}}
        {{--                    {!! $about->text !!}--}}
        {{--                </div>--}}
        {{--            </div>--}}

        {{--            <div class="five-col-theme">--}}
        {{--                <div class="row clearfix">--}}

        {{--                    <!--Column-->--}}

        {{--                    <!--Column-->--}}
        {{--                    @foreach($aboutIcons  as $icon)--}}
        {{--                        <article class="col-md-4 col-sm-6 col-xs-12 column">--}}
        {{--                            <div class="inner-box">--}}
        {{--                                <div class="icon"><span class="{{$icon->icon}}"></span></div>--}}
        {{--                                <h4 class="title">{{$icon->text}}</h4>--}}
        {{--                            </div>--}}
        {{--                        </article>--}}
        {{--                    @endforeach--}}

        {{--                </div>--}}
        {{--            </div>--}}

        {{--        </div>--}}
        {{--    </section>--}}






        {{--    <!--Property Details Section-->--}}

        {{--    <!--Gallery Section-->--}}
        {{--    <section id="gallery" class="gallery-section full-width">--}}
        {{--        <div class="auto-container">--}}

        {{--            <!--Section Title-->--}}
        {{--            <div class="sec-title">--}}

        {{--                <h2>{{getlang('Gallery')}}</h2>--}}


        {{--            </div>--}}

        {{--            <!--Filter-->--}}


        {{--        </div>--}}

        {{--        <div class="images-container">--}}
        {{--            <div class="filter-list clearfix">--}}

        {{--                <!--Image Box-->--}}
        {{--                @foreach($gallery as $image)--}}
        {{--                    <div class="image-box mix mix_all living-room kitchen garage">--}}
        {{--                        <div class="inner-box">--}}
        {{--                            <figure class="image"><a href="{{asset($image->image)}}" class="lightbox-image">--}}
        {{--                                    <img style="width: 100%; height: 350px; object-fit: cover"--}}
        {{--                                         src="{{asset($image->image)}}" alt=""></a></figure>--}}
        {{--                            <a href="{{asset($image->image)}}" class="zoom-btn lightbox-image"><span--}}
        {{--                                        class="icon flaticon-add30"></span></a>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}

        {{--                @endforeach--}}
        {{--            </div>--}}
        {{--        </div>--}}


        {{--    </section>--}}

        {{--    <!--Intro Section-->--}}
        {{--    <section class="intro-section">--}}
        {{--        <div class="auto-container">--}}
        {{--            <div class="outer-box clearfix">--}}
        {{--                <span class="anim-image wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms"><img--}}
        {{--                            src="{{asset('uploads/'.$settings->green)}}" style="max-width: 250px;" alt=""></span>--}}
        {{--                <div class="col-md-9 col-sm-7 col-xs-12">--}}
        {{--                    <p>{{getlang('greentext')}}</p>--}}
        {{--                </div>--}}
        {{--                <div class="col-md-3 col-sm-5 col-xs-12 text-right">--}}
        {{--                    <a href="#" class="theme-btn btn-style-two">CONTACT NOW</a>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--    </section>--}}


        {{--    <!--Testimonials-->--}}
        {{--    <section class="testimonials-section" style="background-image:url('{{asset('uploads/'.$settings->partners)}}');">--}}
        {{--        <div class="auto-container">--}}

        {{--            <div class="sec-title">--}}
        {{--                <h2> {{getlang('Partners')}} </h2>--}}


        {{--            </div>--}}

        {{--            <!--Slider-->--}}
        {{--            <div class="testimonials-slider column-carousel three-column">--}}

        {{--                <!--Slide-->--}}
        {{--                @foreach($partners as $partner)--}}
        {{--                    <article class="slide-item">--}}
        {{--                        <img src="{{asset($partner->image)}}" alt="">--}}
        {{--                    </article>--}}

        {{--                @endforeach--}}


        {{--            </div>--}}

        {{--        </div>--}}
        {{--    </section>--}}
        {{--    <!--Contact Section-->--}}
        {{--    <section id="contactus" class="default-section faded-section contact-section"--}}
        {{--             style="background-image:url(images/background/contact-bg.jpg);">--}}
        {{--        <div class="auto-container">--}}
        {{--            <div class="row clearfix">--}}
        {{--                <!--Column-->--}}
        {{--                <div class="col-md-5 col-sm-12 col-xs-12 column">--}}
        {{--                    <h2>{{getlang('It’s Easy to Find Us')}}</h2>--}}

        {{--                    <div class="desc-text">--}}
        {{--                        <p>{{getlang('contactFooterText')}}</p>--}}
        {{--                    </div>--}}
        {{--                    <!--Contact Info-->--}}
        {{--                    <ul class="contact-info">--}}
        {{--                        <li>--}}
        {{--                            <span class="icon flaticon-location74"></span>--}}
        {{--                            <h3>{{getlang('ADDRESS')}}</h3>--}}
        {{--                            <p>{{getlang('AddressText')}}</p>--}}
        {{--                        </li>--}}

        {{--                        <li>--}}
        {{--                            <span class="icon flaticon-telephone51"></span>--}}
        {{--                            <h3>{{getlang('PHONE')}}</h3>--}}
        {{--                            <p>{{getlang('phoneText')}}</p>--}}
        {{--                        </li>--}}

        {{--                        <li>--}}
        {{--                            <span class="icon flaticon-envelope126"></span>--}}
        {{--                            <h3>{{getlang('EMAIL')}}</h3>--}}
        {{--                            <p>{{getlang('emailText')}}</p>--}}
        {{--                        </li>--}}
        {{--                    </ul>--}}
        {{--                </div>--}}

        {{--                <!--Column-->--}}
        {{--                <div class="col-md-7 col-sm-12 col-xs-12 column">--}}
        {{--                    <h2>{{getlang('CONTACT FORM')}}</h2>--}}
        {{--                    <!--Contact Form-->--}}
        {{--                    <div class="contact-form">--}}
        {{--                        <form method="post" id="contact-form" action="sendemail.php">--}}

        {{--                            <div class="row clearfix">--}}
        {{--                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">--}}
        {{--                                    <input type="text" name="username" value="" placeholder="{{getlang('name')}}">--}}
        {{--                                </div>--}}
        {{--                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">--}}
        {{--                                    <input type="email" name="email" value="" placeholder="{{getlang('email')}}">--}}
        {{--                                </div>--}}
        {{--                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">--}}
        {{--                                    <input type="text" name="subject" value="" placeholder="{{getlang('subject')}}">--}}
        {{--                                </div>--}}
        {{--                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">--}}
        {{--                                    <textarea name="message" placeholder="{{getlang('message')}}"></textarea>--}}
        {{--                                </div>--}}
        {{--                                <div class="col-md-12 col-sm-12 col-xs-12 form-group text-right">--}}
        {{--                                    <button type="submit" name="submit"--}}
        {{--                                            class="theme-btn btn-style-one">{{getlang('Send')}}</button>--}}
        {{--                                </div>--}}
        {{--                            </div>--}}
        {{--                        </form>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}

        {{--            <!--Separator-->--}}
        {{--            <div class="separator big-separator"></div>--}}

        {{--            <!--Footer Content-->--}}
        {{--            <div class="footer-content">--}}
        {{--                <div class="social-links">--}}
        {{--                    <a href="{{$settings->fb}}" target="_blank"><span class="fa-brands fa-facebook"></span></a>--}}
        {{--                    <a href="{{$settings->insta}}" target="_blank"> <span class="fa-brands fa-instagram"></span></a>--}}

        {{--                </div>--}}

        {{--                <div class="copyright">&copy; 2022 {{getlang('All Right Reserved')}} by <a href="https://am.linkedin.com/in/zhora-khzmalyan-4642591a0" target="_blank">Zhora Khzmalyan</a></div>--}}
        {{--            </div>--}}

        {{--        </div>--}}
        {{--    </section>--}}

    </div>
    <!--End pagewrapper-->

    <!--Scroll to top-->
    <div class="scroll-to-top"><span class="fa fa-arrow-up"></span></div>
    <div class="modal fade" id="roomModal" tabindex="-1" role="dialog" aria-labelledby="roomModalLabel"
         style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document" style="width: 70%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="roomModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <img class="img-fluid" style="width: 100%" src="" id="floor-image" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </main>
</div>

<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>

<script src="js/bootstrap.min.js"></script>
<script src="js/revolution.min.js"></script>
<script src="js/jquery.mixitup.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/owl.js"></script>
<script src="js/validate.js"></script>
{{--<script src="http://maps.google.com/maps/api/js"></script>--}}
{{--<script src="js/googlemaps.js"></script>--}}
<script src="js/wow.js"></script>
<script src="js/script.js"></script>

<script src="https://cdn.jsdelivr.net/npm/imagemapster@1.5.4/dist/jquery.imagemapster.js"></script>
<script>

    $(document).ready(function(){
        $('.image-popup-vertical-fit').magnificPopup({
            type: 'image',
            mainClass: 'mfp-with-zoom',
            gallery:{
                enabled:true
            },

            zoom: {
                enabled: true,

                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out', // CSS transition easing function

                opener: function(openerElement) {

                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            }

        });

    });



    $(() => {
        // $.noConflict();
        function layoutSet(id) {
            // console.log(id)
            // for (var i = 0; i < $('#layouts img').length; i++) {
            var st = $('#floor'+id);
            var image = st;
            var str = $(image)
            var areas = [];
            // console.log(str,str.attr('id'))
            $('map[name=' + str.attr('id') + ']').find('area').each(function () {
                var color = "33ff00b8";
                if ($(this).data('bussy') == 'yes') {
                    color = 'ff0000c7'
                }
                areas.push({
                    key: $(this).attr('name'),
                    fillColor: color
                })
            })
            // console.log(areas)
            str.mapster(
                {
                    fillOpacity: 0.8,
                    fillColor: "ef280b",
                    stroke: true,
                    strokeColor: "3320FF",
                    strokeOpacity: 0.8,
                    strokeWidth: 0.2,
                    singleSelect: true,
                    mapKey: 'name',
                    listKey: 'name',
                    toolTipClose: ["tooltip-click", "area-click"],
                    areas: areas
                });
        }



        $('.layoutsPagination li').click(function (e) {
            e.preventDefault();
            $('.layoutsPagination li').removeClass('active');
            $(this).addClass('active');
            $('.floor-list').css('display','none');
            $('#floor_' + $(this).data('id')).css('display','block');
            var id = $(this).data('id');
            console.log(id)
            setTimeout((()=>{
                layoutSet(id)
            }),150)
            ;

        })
        $('.block-header-item a').click(function (e) {
            $('.block-header-item a').parent().removeClass('item-content-wrapper--active')
            $(this).parent().addClass('item-content-wrapper--active')
            // if (!$(this).parent().hasClass('contact-menu')) {
                e.preventDefault();
                // $('.navigation li').removeClass('current');
                // $(this).parent().addClass('current');

                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#" + $(this).data('id')).offset().top
                }, 1000);
            // }
        });
        $('map').find('area').click(function (e) {
            e.preventDefault();
            $('#floor-image').attr('src', $(this).data('image'))
            $('#roomModal').modal('show');
        })
    })


</script>

</body>
</html>
