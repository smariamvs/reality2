<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Parkavenue.am</title>
    <!-- Stylesheets -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/revolution-slider.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/respond.js"></script><![endif]-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
          integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <style>
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            z-index: 2;
            color: #fff;
            cursor: default;
            background-color: var(--nav-link-color)!important;
            border-color: var(--nav-link-color)!important;
        }
        .pagination>li>a, .pagination>li>span{
            color: black!important;
        }
    </style>
</head>

<body>
<div class="page-wrapper">

    <!-- Preloader -->
{{--    <div class="preloader"></div>--}}

<!-- Main Header / Header Style Two-->
    <header class="main-header header-style-two">
        <!-- Header Top -->
        <div class="header-top">
            <div class="auto-container clearfix">

                <!-- Top Right -->
                <div class="top-right">

                    <!--Info-->
                    <ul class="info pull-left clearfix">
                        <li class="email"><a href="mailto:{{$settings->email}}"><span
                                        class="icon flaticon-email2"></span>{{$settings->email}}</a></li>
                        <li class="phone"><a href="tel:{{$settings->phone}}"><span
                                        class="icon flaticon-phone325"></span> {{$settings->phone}}
                            </a></li>
                    </ul>

                    <!--Social Links-->
                    <div class="social-links pull-left clearfix">
                        <a href="{{$settings->fb}}" target="_blank"><span class="fa-brands fa-facebook"></span></a>
                        <a href="{{$settings->insta}}" target="_blank"><span class="fa-brands fa-instagram"></span></a>

                    </div>

                </div>

            </div>
        </div>

        <!-- Header Lower -->
        <div class="header-lower">
            <div class="auto-container">
                <div class="lower-outer clearfix">
                    <!--Logo-->
                    <div class="logo"><a href="#"><img src="{{asset('uploads/'.$settings->logo)}}" alt="Bulldozer"
                                                       title="Dream Land"></a>
                    </div>

                    <!--Right Container-->

                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation">
                                <li class="current"><a data-id="slider-section" href="#">{{getlang('Home')}}</a></li>
                                <li><a href="#" data-id="sales-hall-section">{{getlang('Salles Hall')}}</a></li>
                                <li><a href="#" data-id="aboutus">{{getlang('About US')}}</a></li>
                                <li><a href="#" data-id="gallery">{{getlang('Gallery')}}</a></li>
                                <li><a href="#" data-id="contactus">{{getlang('Contacts')}}</a></li>
                                <li class="contact-menu" style="{{Lang()=='am'?'display:none':""}}">
                                    <a href="{{route('index','am')}}"><img style="max-width: 30px"
                                                                           src="{{asset('flags/am.png')}}" alt=""></a>
                                    <figure id="contact-1" class=" hidden-sm hidden-md hidden-xs"></figure>
                                </li>
                                <li class="contact-menu" style="{{Lang()=='ru'?'display:none':""}}">
                                    <a href="{{route('index','ru')}}"><img style="max-width: 30px"
                                                                           src="{{asset('flags/ru.png')}}" alt=""></a>
                                    <figure id="contact-1" class=" hidden-sm hidden-md hidden-xs"></figure>
                                </li>
                                <li class="contact-menu" style="{{Lang()=='en'?'display:none':""}}">
                                    <a href="{{route('index','en')}}"><img style="max-width: 30px"
                                                                           src="{{asset('flags/en.png')}}" alt=""></a>
                                    <figure id="contact-1" class=" hidden-sm hidden-md hidden-xs"></figure>
                                </li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>

        </div>


    </header>
    <!--End Main Header -->


    <!--Main Slider-->
    <section class="main-slider" id="slider-section">
        <div class="video" id="home">
            <video autoplay="" class="home__video" muted="" style="width:100%;object-fit: cover" playsinline>
                <source src="{{asset($video->value)}}">
            </video>
            <div class="video__icons" style="display: none;">
                <div class="video__pause"></div>
                <div class="video__volume"></div>
            </div>
        </div>
    </section>

    <section id="sales-hall-section" class="gallery-section full-width">
        <div class="auto-container">

            <!--Section Title-->
            @php $salesText = explode(' ',getlang('Salles Hall'));
            @endphp

            <div class="sec-title">
                <h2>{{$salesText[0]}} <span class="theme_color">{{$salesText[1]}}</span></h2>
                <div class="separator small-separator"></div>

            </div>

            <!--Filter-->

        </div>

        <div class="container" id="layouts">
            <div class="row">

                <div class="col-md-12">
                    @foreach($layouts as $key => $layout)
                        <div class="floor-list" id="floor_{{$layout->id}}" @if($key>0) style="display: none" @endif>
                            <img style="max-width: 100%" data-id="{{$layout->id}}" id="floor{{$layout->id}}"
                                 src="{{asset('uploads/'.$layout->image)}}"
                                 alt=""
                                 usemap="#floor{{$layout->id}}">
                            {!! str_replace('name=""',"name='floor$layout->id'",$layout->start) !!}
                            @if($layout->details->count()>0)
                                @foreach($layout->details as $detail)
                                    @php
                                        $d = $detail->detail;

                                        $rand = str_random(9);
                                        $d = str_replace('name=""',"name='$rand'",$d);
                                       $d = str_replace('data-bussy=""',"data-bussy='$detail->busy' data-ids='$detail->id' data-image='".asset('uploads/'.$detail->image)."'",$d);
                                    @endphp
                                    {!! $d !!}
                                @endforeach
                            @endif
                            {!! $layout->end !!}
                        </div>
                    @endforeach
                    <nav aria-label="Page navigation example" style="display: flex; padding: 20px">
                        <ul class="pagination layoutsPagination" style="margin: 0 auto">
                            @foreach($layouts as $key => $item)
                                <li data-id="{{$item->id}}" class="page-item {{$key==0?'active':""}}"><a
                                            class="page-link" href="#">{{$item->floor}}</a></li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

    </section>

    <!--Properties Section-->
    <section class="properties-section" id="aboutus">
        <div class="auto-container">

            <!--Section Title-->
            <div class="sec-title">
                @php($abouttext = explode(' ',getlang('About Us')))

                <h2>{{$abouttext[0]}} <span class="theme_color">{{$abouttext[1]}}</span></h2>
                <div class="separator small-separator"></div>
                <div class="text">
                    {!! $about->text !!}
                </div>
            </div>

            <div class="five-col-theme">
                <div class="row clearfix">

                    <!--Column-->

                    <!--Column-->
                    @foreach($aboutIcons  as $icon)
                        <article class="col-md-4 col-sm-6 col-xs-12 column">
                            <div class="inner-box">
                                <div class="icon"><span class="{{$icon->icon}}"></span></div>
                                <h4 class="title">{{$icon->text}}</h4>
                            </div>
                        </article>
                    @endforeach

                </div>
            </div>

        </div>
    </section>


    <!--Property Details Section-->

    <!--Gallery Section-->
    <section id="gallery" class="gallery-section full-width">
        <div class="auto-container">

            <!--Section Title-->
            <div class="sec-title">

                <h2>{{getlang('Gallery')}}</h2>
                <div class="separator small-separator"></div>

            </div>

            <!--Filter-->


        </div>

        <div class="images-container">
            <div class="filter-list clearfix">

                <!--Image Box-->
                @foreach($gallery as $image)
                    <div class="image-box mix mix_all living-room kitchen garage">
                        <div class="inner-box">
                            <figure class="image"><a href="{{asset($image->image)}}" class="lightbox-image">
                                    <img style="width: 100%; height: 350px; object-fit: cover"
                                         src="{{asset($image->image)}}" alt=""></a></figure>
                            <a href="{{asset($image->image)}}" class="zoom-btn lightbox-image"><span
                                        class="icon flaticon-add30"></span></a>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>


    </section>

    <!--Intro Section-->
    <section class="intro-section">
        <div class="auto-container">
            <div class="outer-box clearfix">
                <span class="anim-image wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1000ms"><img
                            src="{{asset('uploads/'.$settings->green)}}" style="max-width: 250px;" alt=""></span>
                <div class="col-md-9 col-sm-7 col-xs-12">
                    <p>{{getlang('greentext')}}</p>
                </div>
                <div class="col-md-3 col-sm-5 col-xs-12 text-right">
{{--                    <a href="#" class="theme-btn btn-style-two">CONTACT NOW</a>--}}
                </div>
            </div>
        </div>
    </section>


    <!--Testimonials-->
    <section class="testimonials-section" style="background-image:url('{{asset('uploads/'.$settings->partners)}}');">
        <div class="auto-container">

            <div class="sec-title">
                <h2> {{getlang('Partners')}} </h2>
                <div class="separator small-separator"></div>

            </div>

            <!--Slider-->
            <div class="testimonials-slider column-carousel three-column">

                <!--Slide-->
                @foreach($partners as $partner)
                    <article class="slide-item">
                        <img src="{{asset($partner->image)}}" alt="">
                    </article>

                @endforeach


            </div>

        </div>
    </section>
    <!--Contact Section-->
    <section id="contactus" class="default-section faded-section contact-section"
             style="background-image:url(images/background/contact-bg.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Column-->
                <div class="col-md-5 col-sm-12 col-xs-12 column">
                    <h2>{{getlang('It’s Easy to Find Us')}}</h2>

                    <div class="desc-text">
                        <p>{{getlang('contactFooterText')}}</p>
                    </div>
                    <!--Contact Info-->
                    <ul class="contact-info">
                        <li>
                            <span class="icon flaticon-location74"></span>
                            <h3>{{getlang('ADDRESS')}}</h3>
                            <p>{{getlang('AddressText')}}</p>
                        </li>

                        <li>
                            <span class="icon flaticon-telephone51"></span>
                            <h3>{{getlang('PHONE')}}</h3>
                            <p>{{getlang('phoneText')}}</p>
                        </li>

                        <li>
                            <span class="icon flaticon-envelope126"></span>
                            <h3>{{getlang('EMAIL')}}</h3>
                            <p>{{getlang('emailText')}}</p>
                        </li>
                    </ul>
                </div>

                <!--Column-->
                <div class="col-md-7 col-sm-12 col-xs-12 column">
                    <h2>{{getlang('CONTACT FORM')}}</h2>
                    <!--Contact Form-->
                    <div class="contact-form">
                        <form method="post" id="contact-form" action="sendemail.php">

                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="text" name="username" value="" placeholder="{{getlang('name')}}">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="email" name="email" value="" placeholder="{{getlang('email')}}">
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <input type="text" name="subject" value="" placeholder="{{getlang('subject')}}">
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <textarea name="message" placeholder="{{getlang('message')}}"></textarea>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group text-right">
                                    <button type="submit" name="submit"
                                            class="theme-btn btn-style-one">{{getlang('Send')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--Separator-->
            <div class="separator big-separator"></div>

            <!--Footer Content-->
            <div class="footer-content">
                <div class="social-links">
                    <a href="{{$settings->fb}}" target="_blank"><span class="fa-brands fa-facebook"></span></a>
                    <a href="{{$settings->insta}}" target="_blank"> <span class="fa-brands fa-instagram"></span></a>

                </div>

                <div class="copyright">&copy; 2022 {{getlang('All Right Reserved')}} by <a href="https://am.linkedin.com/in/zhora-khzmalyan-4642591a0" target="_blank">Zhora Khzmalyan</a></div>
            </div>

        </div>
    </section>

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top"><span class="fa fa-arrow-up"></span></div>
<div class="modal fade" id="roomModal" tabindex="-1" role="dialog" aria-labelledby="roomModalLabel"
     style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document" style="width: 70%">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="roomModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-fluid" style="width: 100%" src="" id="floor-image" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--<script src="js/jquery.js"></script>--}}
<script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/revolution.min.js"></script>
<script src="js/jquery.mixitup.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/owl.js"></script>
<script src="js/validate.js"></script>
{{--<script src="http://maps.google.com/maps/api/js"></script>--}}
{{--<script src="js/googlemaps.js"></script>--}}
<script src="js/wow.js"></script>
<script src="js/script.js"></script>

<script src="https://cdn.jsdelivr.net/npm/imagemapster@1.5.4/dist/jquery.imagemapster.js"></script>
<script>
    $(() => {
        // $.noConflict();
        function layoutSet(id) {
            // console.log(id)
            // for (var i = 0; i < $('#layouts img').length; i++) {
                var st = $('#floor'+id);
                    var image = st;
                    var str = $(image)
                    var areas = [];
                   // console.log(str,str.attr('id'))
                    $('map[name=' + str.attr('id') + ']').find('area').each(function () {
                        var color = "33ff00b8";
                        if ($(this).data('bussy') == 'yes') {
                            color = 'ff0000c7'
                        }
                        areas.push({
                            key: $(this).attr('name'),
                            fillColor: color
                        })
                    })
                 // console.log(areas)
                    str.mapster(
                        {
                            fillOpacity: 0.8,
                            fillColor: "ef280b",
                            stroke: true,
                            strokeColor: "3320FF",
                            strokeOpacity: 0.8,
                            strokeWidth: 0.2,
                            singleSelect: true,
                            mapKey: 'name',
                            listKey: 'name',
                            toolTipClose: ["tooltip-click", "area-click"],
                            areas: areas
                        });
                }



        $('.layoutsPagination li').click(function (e) {
            e.preventDefault();
            $('.layoutsPagination li').removeClass('active');
            $(this).addClass('active');
            $('.floor-list').css('display','none');
            $('#floor_' + $(this).data('id')).css('display','block');
            var id = $(this).data('id');
            console.log(id)
            setTimeout((()=>{
                layoutSet(id)
            }),150)
            ;

        })
        $('.navigation li a').click(function (e) {
            if (!$(this).parent().hasClass('contact-menu')) {
                e.preventDefault();
                $('.navigation li').removeClass('current');
                $(this).parent().addClass('current');

                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#" + $(this).data('id')).offset().top
                }, 1000);
            }
        });
        $('map').find('area').click(function (e) {
            e.preventDefault();
            $('#floor-image').attr('src', $(this).data('image'))
            $('#roomModal').modal('show');
        })
    })


</script>

</body>
</html>
