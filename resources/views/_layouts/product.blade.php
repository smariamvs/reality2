<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <title>{{setting()->title}} -  {{$product->title}}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="{{setting()->description}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
</style>
<link rel='stylesheet' id='prettyphoto-style-css'  href='{{asset('assets/content/themes/jardiwinery/fw/js/prettyphoto/css/prettyPhoto.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='{{asset('css/dist/block-library/style.min.css?ver=5.5.1')}}' type='text/css' media='all' />
<link rel='stylesheet' id='wc-block-vendors-style-css'  href='{{asset('assets/content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-style.css?ver=3.1.0')}}' type='text/css' media='all' />
<link rel='stylesheet' id='wc-block-style-css'  href='{{asset('assets/content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=3.1.0')}}' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='{{asset('assets/content/plugins/contact-form-7/includes/css/styles.css?ver=5.2.2')}}' type='text/css' media='all' />
<link rel='stylesheet' id='essential-grid-plugin-settings-css'  href='{{asset('assets/content/plugins/essential-grid/public/assets/css/settings.css?ver=3.0.6')}}' type='text/css' media='all' />
<link rel='stylesheet' id='tp-fontello-css'  href='{{asset('assets/content/plugins/essential-grid/public/assets/font/fontello/css/fontello.css?ver=3.0.6')}}' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='{{asset('assets/content/plugins/revslider/public/assets/css/rs6.css?ver=6.2.22')}}' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
    #rs-demo-id {}
</style>
<link rel='stylesheet' id='photoswipe-css'  href='{{asset('assets/content/plugins/woocommerce/assets/css/photoswipe/photoswipe.min.css?ver=4.4.1')}}' type='text/css' media='all' />
<link rel='stylesheet' id='photoswipe-default-skin-css'  href='{{asset('assets/content/plugins/woocommerce/assets/css/photoswipe/default-skin/default-skin.min.css?ver=4.4.1')}}' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-layout-css'  href='{{asset('assets/content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=4.4.1')}}' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='{{asset('assets/content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=4.4.1')}}' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='{{asset('assets/content/plugins/woocommerce/assets/css/woocommerce.css?ver=4.4.1')}}' type='text/css' media='all' />
<style id='woocommerce-inline-inline-css' type='text/css'>
    .woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel='stylesheet' id='jardiwinery-font-google-fonts-style-css'  href='//fonts.googleapis.com/css?family=Vidaloka|Lato:300,300italic,400,400italic,700,700italic|Montserrat:300,300italic,400,400italic,700,700italic&#038;subset=latin,latin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='fontello-style-css'  href='{{asset('assets/content/themes/jardiwinery/css/fontello/css/fontello.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='jardiwinery-main-style-css'  href='{{asset('assets/content/themes/jardiwinery/style.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='jardiwinery-animation-style-css'  href='{{asset('assets/content/themes/jardiwinery/fw/css/core.animation.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='jardiwinery-shortcodes-style-css'  href='{{asset('assets/content/plugins/trx_utils/shortcodes/theme.shortcodes.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='jardiwinery-theme-style-css'  href='{{asset('assets/content/themes/jardiwinery/css/theme.css')}}' type='text/css' media='all' />
<style id='jardiwinery-theme-style-inline-css' type='text/css'>
    .contacts_wrap .logo img{height:60px}
</style>
<link rel='stylesheet' id='jardiwinery-plugin-woocommerce-style-css'  href='{{asset('assets/content/themes/jardiwinery/css/plugin.woocommerce.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='jardiwinery-plugin-elegro-payment-style-css'  href='{{asset('assets/content/themes/jardiwinery/css/plugin.elegro-payment.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='jardiwinery-plugin-product-delivery-date-for-woocommerce-lite-style-css'  href='{{asset('assets/content/themes/jardiwinery/css/plugin.product-delivery-date-for-woocommerce-lite.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='jardiwinery-responsive-style-css'  href='{{asset('assets/content/themes/jardiwinery/css/responsive.css')}}' type='text/css' media='all' />
<link rel='stylesheet' id='mediaelement-css'  href='{{asset('js/mediaelement/mediaelementplayer-legacy.min.css?ver=4.2.13-9993131')}}' type='text/css' media='all' />
<link rel='stylesheet' id='wp-mediaelement-css'  href='{{asset('js/mediaelement/wp-mediaelement.min.css?ver=5.5.1')}}' type='text/css' media='all' />
<script type='text/javascript' src='{{asset('js/jquery/jquery.js?ver=1.12.4-wp')}}' id='jquery-core-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/revslider/public/assets/js/rbtools.min.js?ver=6.2.22')}}' id='tp-tools-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.2.22')}}' id='revmin-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70')}}' id='jquery-blockui-js'></script>
<script type='text/javascript' id='wc-add-to-cart-js-extra'>
    /* <![CDATA[ */
    var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/jardiwinery.ancorathemes.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('assets/content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=4.4.1')}}' id='wc-add-to-cart-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js?ver=6.3.0')}}' id='vc_woocommerce-add-to-cart-js-js'></script>
<script type='text/javascript' src='{{asset('assets/content/themes/jardiwinery/fw/js/photostack/modernizr.min.js')}}' id='modernizr-js'></script>

<meta name="generator" content="WordPress 5.5.1" />
<meta name="generator" content="WooCommerce 4.4.1" />
<link rel='shortlink' href='http://jardiwinery.ancorathemes.com/?p=124' />
<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
<link rel="icon" href="{{asset('assets/content/uploads/2020/04/favicon-100x100.jpg')}}" sizes="32x32" />
<link rel="icon" href="{{asset('assets/content/uploads/2020/04/favicon.jpg')}}" sizes="192x192" />
<link rel="apple-touch-icon" href="{{asset('assets/content/uploads/2020/04/favicon.jpg')}}" />
<meta name="msapplication-TileImage" content="{{asset('assets/content/uploads/2020/04/favicon.jpg')}}" />
<script type="text/javascript">function setREVStartSize(e){
        //window.requestAnimationFrame(function() {
        window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;
        window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;
        try {
            var pw = document.getElementById(e.c).parentNode.offsetWidth,
                newh;
            pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
            e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
            e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
            e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
            e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
            e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
            e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
            e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);
            if(e.layout==="fullscreen" || e.l==="fullscreen")
                newh = Math.max(e.mh,window.RSIH);
            else{
                e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
                for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];
                e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
                e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
                for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];

                var nl = new Array(e.rl.length),
                    ix = 0,
                    sl;
                e.tabw = e.tabhide>=pw ? 0 : e.tabw;
                e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
                e.tabh = e.tabhide>=pw ? 0 : e.tabh;
                e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;
                for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
                sl = nl[0];
                for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}
                var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);
                newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
            }
            if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));
            document.getElementById(e.c).height = newh+"px";
            window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";
        } catch(e){
            console.log("Failure at Presize of Slider:" + e)
        }
        //});
    };</script>
<noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
    <link rel="stylesheet" href="{{asset("assets\media.css")}}">
    <style>
        .header_mobile .menu_main_cart {
             position: unset!important;
             right: unset!important;
             margin: unset!important;
             top: unset!important;
             margin-top:unset!important;
        }
    </style>
</head>
@include('includes.header_menu')
@yield('content')
<script type="text/javascript">
    var c = document.body.className;
    c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
    document.body.className = c;
</script>

<link rel='stylesheet' id='prdd-jquery-ui-css'  href='{{asset('assets/content/plugins/product-delivery-date-for-woocommerce-lite/css/themes/smoothness/jquery-ui.css?ver=2.3.0')}}' type='text/css' media='' />
<link rel='stylesheet' id='datepicker-css'  href='{{asset('assets/content/plugins/product-delivery-date-for-woocommerce-lite/css/datepicker.css?ver=2.3.0')}}' type='text/css' media='' />
<link rel='stylesheet' id='gdpr-consent-until-css'  href='{{asset('assets/content/plugins/gdpr-framework/assets/css/consentuntil.min.css?ver=1')}}' type='text/css' media='all' />
<link rel='stylesheet' id='gdpr-consent-until-dashicons-css'  href='http://jardiwinery.ancorathemes.com/wp-includes//css/dashicons.min.css?ver=1' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='{{asset('assets/content/plugins/js_composer/assets/css/js_composer.min.css?ver=6.3.0')}}' type='text/css' media='all' />
<script type='text/javascript' src='{{asset('assets/content/themes/jardiwinery/fw/js/prettyphoto/jquery.prettyPhoto.min.js?ver=no-compose')}}' id='prettyphoto-script-js'></script>
<script type='text/javascript' id='contact-form-7-js-extra'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"http:\/\/jardiwinery.ancorathemes.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('assets/content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2.2')}}' id='contact-form-7-js'></script>


<script type='text/javascript' src='{{asset('assets/content/plugins/trx_utils/js/trx_utils.js')}}' id='trx_utils-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/woocommerce/assets/js/zoom/jquery.zoom.min.js?ver=1.7.21')}}' id='zoom-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/js_composer/assets/lib/bower/flexslider/jquery.flexslider-min.js?ver=6.3.0')}}' id='flexslider-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/woocommerce/assets/js/photoswipe/photoswipe.min.js?ver=4.1.1')}}' id='photoswipe-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/woocommerce/assets/js/photoswipe/photoswipe-ui-default.min.js?ver=4.1.1')}}' id='photoswipe-ui-default-js'></script>
<script type='text/javascript' id='wc-single-product-js-extra'>
    /* <![CDATA[ */
    var wc_single_product_params = {"i18n_required_rating_text":"Please select a rating","review_rating_required":"yes","flexslider":{"rtl":false,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"1","zoom_options":[],"photoswipe_enabled":"1","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":"1"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('assets/content/plugins/woocommerce/assets/js/frontend/single-product.min.js?ver=4.4.1')}}' id='wc-single-product-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4')}}' id='js-cookie-js'></script>
<script type='text/javascript' id='woocommerce-js-extra'>
    /* <![CDATA[ */
    var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('assets/content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=4.4.1')}}' id='woocommerce-js'></script>
<script type='text/javascript' id='wc-cart-fragments-js-extra'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_05a09a0657e41c0280ac4a7ee5c3ed7f","fragment_name":"wc_fragments_05a09a0657e41c0280ac4a7ee5c3ed7f","request_timeout":"5000"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('assets/content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=4.4.1')}}' id='wc-cart-fragments-js'></script>
<script type='text/javascript' id='donot-sell-form-js-extra'>
    /* <![CDATA[ */
    var localized_donot_sell_form = {"admin_donot_sell_ajax_url":"http:\/\/jardiwinery.ancorathemes.com\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('assets/content/plugins/gdpr-framework/assets/js/gdpr-donotsell.js?ver=1.0.0')}}' id='donot-sell-form-js'></script>
<script type='text/javascript' src='{{asset('assets/content/themes/jardiwinery/fw/js/superfish.js')}}' id='superfish-js'></script>
<script type='text/javascript' src='{{asset('assets/content/themes/jardiwinery/fw/js/jquery.slidemenu.js')}}' id='slidemenu-script-js'></script>
<script type='text/javascript' src='{{asset('assets/content/themes/jardiwinery/fw/js/core.reviews.js')}}' id='jardiwinery-core-reviews-script-js'></script>
<script type='text/javascript' src='{{asset('assets/content/themes/jardiwinery/fw/js/core.utils.js')}}' id='jardiwinery-core-utils-script-js'></script>
<script type='text/javascript' src='{{asset('assets/content/themes/jardiwinery/fw/js/core.init.js')}}' id='jardiwinery-core-init-script-js'></script>
<script type='text/javascript' src='{{asset('assets/content/themes/jardiwinery/js/theme.init.js')}}' id='jardiwinery-theme-init-script-js'></script>
<script type='text/javascript' id='mediaelement-core-js-before'>
    var mejsL10n = {"language":"en","strings":{"mejs.download-file":"Download File","mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen":"Fullscreen","mejs.play":"Play","mejs.pause":"Pause","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.live-broadcast":"Live Broadcast","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}};
</script>
<script type='text/javascript' src='{{asset('js/mediaelement/mediaelement-and-player.min.js?ver=4.2.13-9993131')}}' id='mediaelement-core-js'></script>
<script type='text/javascript' src='{{asset('js/mediaelement/mediaelement-migrate.min.js?ver=5.5.1')}}' id='mediaelement-migrate-js'></script>
<script type='text/javascript' id='mediaelement-js-extra'>
    /* <![CDATA[ */
    var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset('js/mediaelement/wp-mediaelement.min.js?ver=5.5.1')}}' id='wp-mediaelement-js'></script>
<script type='text/javascript' src='{{asset('js/comment-reply.min.js?ver=5.5.1')}}' id='comment-reply-js'></script>
<script type='text/javascript' src='{{asset('js/wp-embed.min.js?ver=5.5.1')}}' id='wp-embed-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/trx_utils/shortcodes/theme.shortcodes.js')}}' id='jardiwinery-shortcodes-script-js'></script>
<script type='text/javascript' src='{{asset('js/jquery/ui/core.min.js?ver=1.11.4')}}' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='{{asset('js/jquery/ui/datepicker.min.js?ver=1.11.4')}}' id='jquery-ui-datepicker-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/woocommerce/assets/js/select2/select2.full.min.js?ver=4.0.3')}}' id='select2-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/product-delivery-date-for-woocommerce-lite/js/i18n/jquery.ui.datepicker-en-GB.js?ver=2.3.0')}}' id='en-GB-js'></script>
<script type='text/javascript' id='prdd-lite-process-functions-js-extra'>
    /* <![CDATA[ */
    var prdd_lite_params = {"prdd_maximum_number_days":"30","prdd_lite_delivery_days":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"date_format":"mm\/dd\/y","prdd_months":"1","first_day":"1","language_selected":"en-GB","additional_data":"{\"holidays\":\"\"}"};
    /* ]]> */
    TRX_DEMO_STORAGE = null;
</script>
<script type='text/javascript' src='{{asset('assets/content/plugins/product-delivery-date-for-woocommerce-lite/js/initialize-datepicker.js?ver=2.3.0')}}' id='prdd-lite-process-functions-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/gdpr-framework/assets/js/consentuntil.min.js?ver=1')}}' id='gdpr-consent-until-js-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.3.0')}}' id='wpb_composer_front_js-js'></script>
<script type='text/javascript' src='{{asset('assets/content/plugins/trx_utils/js/core.googlemap.js')}}' id='jardiwinery-googlemap-script-js'></script>
@yield('script')
<style type="text/css">.trx_demo_inline_1351525680{color:#ffffff !important;border-color:#ffaa5f !important;background-color:#ffaa5f !important;}.trx_demo_inline_2002315140:hover{color:#ffffff !important;border-color:#cc884c !important;background-color:#cc884c !important;}</style>
