<!DOCTYPE html>
<html>
<head>
    <title>{{setting()->title}} @if(isset($product)) | {{$product->title}} @endif @if(isset($category)) {{$category->name}} @endif</title>
    <meta charset="utf-8">
    <!-- bootstrap vs fontawesome-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{asset('assets/front/img/favicon.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('assets/front/bootstrap/3.3.7/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/style-homev3.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/style-res-v3.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/style-fix-nav.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/style-form-search-mobile.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css"
          integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <style>

        .supportcss {
            width: 180px;
            top: calc(50% + 20px);
            padding: 0 10px;
            left: -73px;
        }
        .supportcss, .chatcss {
            background: #0087A7;
            background: -o-linear-gradient(top, #0087A7 0px, #0087A7 100%);
            background: -moz-linear-gradient(top, #0087A7 0px, #0087A7 100%);
            background: -webkit-linear-gradient(top, #0087A7 0px, #0087A7 100%);
            background: -ms-linear-gradient(top, #0087A7 0px, #0087A7 100%);
            -webkit-box-shadow: 0 2px 10px rgb(0 0 0 / 50%), 0 2px 3px rgb(0 0 0 / 50%);
            -moz-box-shadow: 0 2px 10px rgba(0, 0, 0, .5), 0 2px 3px rgba(0, 0, 0, .5);
            -o-box-shadow: 0 2px 10px rgba(0, 0, 0, .5), 0 2px 3px rgba(0, 0, 0, .5);
            box-shadow: 0 2px 10px rgb(0 0 0 / 50%), 0 2px 3px rgb(0 0 0 / 50%);
            border-bottom: none;
            position: fixed;
            z-index: 1000;
            border-radius: 6px 6px 0 0;
            border: 2px solid #F0FFFF;
            cursor: pointer;
            letter-spacing: 2px;
            color: #F0FFFF;
            text-align: center;
            text-shadow: 1px 1px 5px #848482;
            -moz-text-shadow: 1px 1px 5px #848482;
            -webkit-text-shadow: 1px 1px 5px #848482;
            font: 14px/34px 'Open Sans', Arial, Helvetica, sans-serif;
            font-weight: bold;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);
            -ms-transform: rotate(90deg);
        }

        .modal.modal-static .modal-dialog {
            transform: scale(1.02);
        }

        .modal-dialog-scrollable {
            height: calc(100% - 1rem);
        }
        .modal-dialog-scrollable .modal-content {
            max-height: 100%;
            overflow: hidden;
        }
        .modal-dialog-scrollable .modal-body {
            overflow-y: auto;
        }

        .modal-dialog-centered {
            display: flex;
            align-items: center;
            min-height: calc(100% - 1rem);
        }

        .modal-content {
            position: relative;
            display: flex;
            flex-direction: column;
            width: 100%;
            pointer-events: auto;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, 0.2);
            border-radius: 0.3rem;
            outline: 0;
        }

        .modal-backdrop {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1050;
            width: 100vw;
            height: 100vh;
            background-color: #000;
        }
        .modal-backdrop.fade {
            opacity: 0;
        }
        .modal-backdrop.show {
            opacity: 0.5;
        }

        .modal-header {
            display: flex;
            flex-shrink: 0;
            align-items: center;
            justify-content: space-between;
            padding: 1rem 1rem;
            border-bottom: 1px solid #dee2e6;
            border-top-left-radius: calc(0.3rem - 1px);
            border-top-right-radius: calc(0.3rem - 1px);
        }
        .modal-header .btn-close {
            padding: 0.5rem 0.5rem;
            margin: -0.5rem -0.5rem -0.5rem auto;
        }

        .modal-title {
            margin-bottom: 0;
            line-height: 1.5;
        }

        .modal-body {
            position: relative;
            flex: 1 1 auto;
            padding: 1rem;
        }

        .modal-footer {
            display: flex;
            flex-wrap: wrap;
            flex-shrink: 0;
            align-items: center;
            justify-content: flex-end;
            padding: 0.75rem;
            border-top: 1px solid #dee2e6;
            border-bottom-right-radius: calc(0.3rem - 1px);
            border-bottom-left-radius: calc(0.3rem - 1px);
        }
        .modal-footer > * {
            margin: 0.25rem;
        }

        @media (min-width: 576px) {
            .modal-dialog {
                max-width: 500px;
                margin: 1.75rem auto;
            }

            .modal-dialog-scrollable {
                height: calc(100% - 3.5rem);
            }

            .modal-dialog-centered {
                min-height: calc(100% - 3.5rem);
            }

            .modal-sm {
                max-width: 300px;
            }
        }
        @media (min-width: 992px) {
            .modal-lg,
            .modal-xl {
                max-width: 800px;
            }
        }
        @media (min-width: 1200px) {
            .modal-xl {
                max-width: 1140px;
            }
        }
        .modal-fullscreen {
            width: 100vw;
            max-width: none;
            height: 100%;
            margin: 0;
        }
        .modal-fullscreen .modal-content {
            height: 100%;
            border: 0;
            border-radius: 0;
        }
        .modal-fullscreen .modal-header {
            border-radius: 0;
        }
        .modal-fullscreen .modal-body {
            overflow-y: auto;
        }
        .modal-fullscreen .modal-footer {
            border-radius: 0;
        }

        @media (max-width: 575.98px) {
            .modal-fullscreen-sm-down {
                width: 100vw;
                max-width: none;
                height: 100%;
                margin: 0;
            }
            .modal-fullscreen-sm-down .modal-content {
                height: 100%;
                border: 0;
                border-radius: 0;
            }
            .modal-fullscreen-sm-down .modal-header {
                border-radius: 0;
            }
            .modal-fullscreen-sm-down .modal-body {
                overflow-y: auto;
            }
            .modal-fullscreen-sm-down .modal-footer {
                border-radius: 0;
            }
        }
        @media (max-width: 767.98px) {
            .modal-fullscreen-md-down {
                width: 100vw;
                max-width: none;
                height: 100%;
                margin: 0;
            }
            .modal-fullscreen-md-down .modal-content {
                height: 100%;
                border: 0;
                border-radius: 0;
            }
            .modal-fullscreen-md-down .modal-header {
                border-radius: 0;
            }
            .modal-fullscreen-md-down .modal-body {
                overflow-y: auto;
            }
            .modal-fullscreen-md-down .modal-footer {
                border-radius: 0;
            }
        }
        @media (max-width: 991.98px) {
            .modal-fullscreen-lg-down {
                width: 100vw;
                max-width: none;
                height: 100%;
                margin: 0;
            }
            .modal-fullscreen-lg-down .modal-content {
                height: 100%;
                border: 0;
                border-radius: 0;
            }
            .modal-fullscreen-lg-down .modal-header {
                border-radius: 0;
            }
            .modal-fullscreen-lg-down .modal-body {
                overflow-y: auto;
            }
            .modal-fullscreen-lg-down .modal-footer {
                border-radius: 0;
            }
        }
        @media (max-width: 1199.98px) {
            .modal-fullscreen-xl-down {
                width: 100vw;
                max-width: none;
                height: 100%;
                margin: 0;
            }
            .modal-fullscreen-xl-down .modal-content {
                height: 100%;
                border: 0;
                border-radius: 0;
            }
            .modal-fullscreen-xl-down .modal-header {
                border-radius: 0;
            }
            .modal-fullscreen-xl-down .modal-body {
                overflow-y: auto;
            }
            .modal-fullscreen-xl-down .modal-footer {
                border-radius: 0;
            }
        }
        @media (max-width: 1399.98px) {
            .modal-fullscreen-xxl-down {
                width: 100vw;
                max-width: none;
                height: 100%;
                margin: 0;
            }
            .modal-fullscreen-xxl-down .modal-content {
                height: 100%;
                border: 0;
                border-radius: 0;
            }
            .modal-fullscreen-xxl-down .modal-header {
                border-radius: 0;
            }
            .modal-fullscreen-xxl-down .modal-body {
                overflow-y: auto;
            }
            .modal-fullscreen-xxl-down .modal-footer {
                border-radius: 0;
            }
        }
        .form-control {
            display: block;
            width: 100%;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: 0.25rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        @media (prefers-reduced-motion: reduce) {
            .form-control {
                transition: none;
            }
        }
        .form-control[type=file] {
            overflow: hidden;
        }
        .form-control[type=file]:not(:disabled):not([readonly]) {
            cursor: pointer;
        }
        .form-control:focus {
            color: #212529;
            background-color: #fff;
            border-color: #86b7fe;
            outline: 0;
            box-shadow: 0 0 0 0.25rem rgba(13, 110, 253, 0.25);
        }
        .form-control::-webkit-date-and-time-value {
            height: 1.5em;
        }
        .form-control::-moz-placeholder {
            color: #6c757d;
            opacity: 1;
        }
        .form-control::placeholder {
            color: #6c757d;
            opacity: 1;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #e9ecef;
            opacity: 1;
        }
        .form-control::-webkit-file-upload-button {
            padding: 0.375rem 0.75rem;
            margin: -0.375rem -0.75rem;
            -webkit-margin-end: 0.75rem;
            margin-inline-end: 0.75rem;
            color: #212529;
            background-color: #e9ecef;
            pointer-events: none;
            border-color: inherit;
            border-style: solid;
            border-width: 0;
            border-inline-end-width: 1px;
            border-radius: 0;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        .form-control::file-selector-button {
            padding: 0.375rem 0.75rem;
            margin: -0.375rem -0.75rem;
            -webkit-margin-end: 0.75rem;
            margin-inline-end: 0.75rem;
            color: #212529;
            background-color: #e9ecef;
            pointer-events: none;
            border-color: inherit;
            border-style: solid;
            border-width: 0;
            border-inline-end-width: 1px;
            border-radius: 0;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        @media (prefers-reduced-motion: reduce) {
            .form-control::-webkit-file-upload-button {
                -webkit-transition: none;
                transition: none;
            }
            .form-control::file-selector-button {
                transition: none;
            }
        }
        .form-control:hover:not(:disabled):not([readonly])::-webkit-file-upload-button {
            background-color: #dde0e3;
        }
        .form-control:hover:not(:disabled):not([readonly])::file-selector-button {
            background-color: #dde0e3;
        }
        .form-control::-webkit-file-upload-button {
            padding: 0.375rem 0.75rem;
            margin: -0.375rem -0.75rem;
            -webkit-margin-end: 0.75rem;
            margin-inline-end: 0.75rem;
            color: #212529;
            background-color: #e9ecef;
            pointer-events: none;
            border-color: inherit;
            border-style: solid;
            border-width: 0;
            border-inline-end-width: 1px;
            border-radius: 0;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        @media (prefers-reduced-motion: reduce) {
            .form-control::-webkit-file-upload-button {
                -webkit-transition: none;
                transition: none;
            }
        }
        .form-control:hover:not(:disabled):not([readonly])::-webkit-file-upload-button {
            background-color: #dde0e3;
        }

        .form-control-plaintext {
            display: block;
            width: 100%;
            padding: 0.375rem 0;
            margin-bottom: 0;
            line-height: 1.5;
            color: #212529;
            background-color: transparent;
            border: solid transparent;
            border-width: 1px 0;
        }
        .form-control-plaintext.form-control-sm, .form-control-plaintext.form-control-lg {
            padding-right: 0;
            padding-left: 0;
        }

        .form-control-sm {
            min-height: calc(1.5em + 0.5rem + 2px);
            padding: 0.25rem 0.5rem;
            font-size: 0.875rem;
            border-radius: 0.2rem;
        }
        .form-control-sm::-webkit-file-upload-button {
            padding: 0.25rem 0.5rem;
            margin: -0.25rem -0.5rem;
            -webkit-margin-end: 0.5rem;
            margin-inline-end: 0.5rem;
        }
        .form-control-sm::file-selector-button {
            padding: 0.25rem 0.5rem;
            margin: -0.25rem -0.5rem;
            -webkit-margin-end: 0.5rem;
            margin-inline-end: 0.5rem;
        }
        .form-control-sm::-webkit-file-upload-button {
            padding: 0.25rem 0.5rem;
            margin: -0.25rem -0.5rem;
            -webkit-margin-end: 0.5rem;
            margin-inline-end: 0.5rem;
        }

        .form-control-lg {
            min-height: calc(1.5em + 1rem + 2px);
            padding: 0.5rem 1rem;
            font-size: 1.25rem;
            border-radius: 0.3rem;
        }
        .form-control-lg::-webkit-file-upload-button {
            padding: 0.5rem 1rem;
            margin: -0.5rem -1rem;
            -webkit-margin-end: 1rem;
            margin-inline-end: 1rem;
        }
        .form-control-lg::file-selector-button {
            padding: 0.5rem 1rem;
            margin: -0.5rem -1rem;
            -webkit-margin-end: 1rem;
            margin-inline-end: 1rem;
        }
        .form-control-lg::-webkit-file-upload-button {
            padding: 0.5rem 1rem;
            margin: -0.5rem -1rem;
            -webkit-margin-end: 1rem;
            margin-inline-end: 1rem;
        }

        textarea.form-control {
            min-height: calc(1.5em + 0.75rem + 2px);
        }
        textarea.form-control-sm {
            min-height: calc(1.5em + 0.5rem + 2px);
        }
        textarea.form-control-lg {
            min-height: calc(1.5em + 1rem + 2px);
        }

        .form-control-color {
            width: 3rem;
            height: auto;
            padding: 0.375rem;
        }
        .form-control-color:not(:disabled):not([readonly]) {
            cursor: pointer;
        }
        .form-control-color::-moz-color-swatch {
            height: 1.5em;
            border-radius: 0.25rem;
        }
        .form-control-color::-webkit-color-swatch {
            height: 1.5em;
            border-radius: 0.25rem;
        }
        .form-select {
            display: block;
            width: 100%;
            padding: 0.375rem 2.25rem 0.375rem 0.75rem;
            -moz-padding-start: calc(0.75rem - 3px);
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3e%3cpath fill='none' stroke='%23343a40' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M2 5l6 6 6-6'/%3e%3c/svg%3e");
            background-repeat: no-repeat;
            background-position: right 0.75rem center;
            background-size: 16px 12px;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        @media (prefers-reduced-motion: reduce) {
            .form-select {
                transition: none;
            }
        }
        .form-select:focus {
            border-color: #86b7fe;
            outline: 0;
            box-shadow: 0 0 0 0.25rem rgba(13, 110, 253, 0.25);
        }
        .form-select[multiple], .form-select[size]:not([size="1"]) {
            padding-right: 0.75rem;
            background-image: none;
        }
        .form-select:disabled {
            background-color: #e9ecef;
        }
        .form-select:-moz-focusring {
            color: transparent;
            text-shadow: 0 0 0 #212529;
        }

        .form-select-sm {
            padding-top: 0.25rem;
            padding-bottom: 0.25rem;
            padding-left: 0.5rem;
            font-size: 0.875rem;
            border-radius: 0.2rem;
        }

        .form-select-lg {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            padding-left: 1rem;
            font-size: 1.25rem;
            border-radius: 0.3rem;
        }
        /*кнопка звонка*/

        .callback-bt {
            background:#38a3fd;
            border:2px solid #38a3fd;
            border-radius:50%;
            box-shadow:0 8px 10px rgba(56,163,253,0.3);
            cursor:pointer;
            height:68px;
            text-align:center;
            width:68px;
            position: fixed;
            right: 8%;
            bottom: 18%;
            z-index:999;
            transition:.3s;
            -webkit-animation:hoverWave linear 1s infinite;
            animation:hoverWave linear 1s infinite;
        }

        .callback-bt .text-call{
            height:68px;
            width:68px;
            border-radius:50%;
            position:relative;
            overflow:hidden;
        }

        .callback-bt .text-call span {
            text-align: center;
            color:#38a3fd;
            opacity: 0;
            font-size: 0;
            position:absolute;
            right: 4px;
            top: 22px;
            line-height: 14px;
            font-weight: 600;
            text-transform: uppercase;
            transition: opacity .3s linear;
            font-family: 'montserrat', Arial, Helvetica, sans-serif;
        }

        .callback-bt .text-call:hover span {
            opacity: 1;
            font-size: 11px;
        }
        .callback-bt:hover i {
            display:none;
        }

        .callback-bt:hover {
            z-index:1;
            background:#fff;
            color:transparent;
            transition:.3s;
        }
        .callback-bt:hover i {
            color:#38a3fd;
            font-size:40px;
            transition:.3s;
        }
        .callback-bt i {
            color:#fff;
            font-size:34px;
            transition:.3s;
            line-height: 66px;transition: .5s ease-in-out;
        }

        .callback-bt i  {
            animation: 1200ms ease 0s normal none 1 running shake;
            animation-iteration-count: infinite;
            -webkit-animation: 1200ms ease 0s normal none 1 running shake;
            -webkit-animation-iteration-count: infinite;
        }

        @-webkit-keyframes hoverWave {
            0% {
                box-shadow:0 8px 10px rgba(56,163,253,0.3),0 0 0 0 rgba(56,163,253,0.2),0 0 0 0 rgba(56,163,253,0.2)
            }
            40% {
                box-shadow:0 8px 10px rgba(56,163,253,0.3),0 0 0 15px rgba(56,163,253,0.2),0 0 0 0 rgba(56,163,253,0.2)
            }
            80% {
                box-shadow:0 8px 10px rgba(56,163,253,0.3),0 0 0 30px rgba(56,163,253,0),0 0 0 26.7px rgba(56,163,253,0.067)
            }
            100% {
                box-shadow:0 8px 10px rgba(56,163,253,0.3),0 0 0 30px rgba(56,163,253,0),0 0 0 40px rgba(56,163,253,0.0)
            }
        }@keyframes hoverWave {
             0% {
                 box-shadow:0 8px 10px rgba(56,163,253,0.3),0 0 0 0 rgba(56,163,253,0.2),0 0 0 0 rgba(56,163,253,0.2)
             }
             40% {
                 box-shadow:0 8px 10px rgba(56,163,253,0.3),0 0 0 15px rgba(56,163,253,0.2),0 0 0 0 rgba(56,163,253,0.2)
             }
             80% {
                 box-shadow:0 8px 10px rgba(56,163,253,0.3),0 0 0 30px rgba(56,163,253,0),0 0 0 26.7px rgba(56,163,253,0.067)
             }
             100% {
                 box-shadow:0 8px 10px rgba(56,163,253,0.3),0 0 0 30px rgba(56,163,253,0),0 0 0 40px rgba(56,163,253,0.0)
             }
         }

        /* animations icon */

        @keyframes shake {
            0% {
                transform: rotateZ(0deg);
                -ms-transform: rotateZ(0deg);
                -webkit-transform: rotateZ(0deg);
            }
            10% {
                transform: rotateZ(-30deg);
                -ms-transform: rotateZ(-30deg);
                -webkit-transform: rotateZ(-30deg);
            }
            20% {
                transform: rotateZ(15deg);
                -ms-transform: rotateZ(15deg);
                -webkit-transform: rotateZ(15deg);
            }
            30% {
                transform: rotateZ(-10deg);
                -ms-transform: rotateZ(-10deg);
                -webkit-transform: rotateZ(-10deg);
            }
            40% {
                transform: rotateZ(7.5deg);
                -ms-transform: rotateZ(7.5deg);
                -webkit-transform: rotateZ(7.5deg);
            }
            50% {
                transform: rotateZ(-6deg);
                -ms-transform: rotateZ(-6deg);
                -webkit-transform: rotateZ(-6deg);
            }
            60% {
                transform: rotateZ(5deg);
                -ms-transform: rotateZ(5deg);
                -webkit-transform: rotateZ(5deg);
            }
            70% {
                transform: rotateZ(-4.28571deg);
                -ms-transform: rotateZ(-4.28571deg);
                -webkit-transform: rotateZ(-4.28571deg);
            }
            80% {
                transform: rotateZ(3.75deg);
                -ms-transform: rotateZ(3.75deg);
                -webkit-transform: rotateZ(3.75deg);
            }
            90% {
                transform: rotateZ(-3.33333deg);
                -ms-transform: rotateZ(-3.33333deg);
                -webkit-transform: rotateZ(-3.33333deg);
            }
            100% {
                transform: rotateZ(0deg);
                -ms-transform: rotateZ(0deg);
                -webkit-transform: rotateZ(0deg);
            }
        }

        @-webkit-keyframes shake {
            0% {
                transform: rotateZ(0deg);
                -ms-transform: rotateZ(0deg);
                -webkit-transform: rotateZ(0deg);
            }
            10% {
                transform: rotateZ(-30deg);
                -ms-transform: rotateZ(-30deg);
                -webkit-transform: rotateZ(-30deg);
            }
            20% {
                transform: rotateZ(15deg);
                -ms-transform: rotateZ(15deg);
                -webkit-transform: rotateZ(15deg);
            }
            30% {
                transform: rotateZ(-10deg);
                -ms-transform: rotateZ(-10deg);
                -webkit-transform: rotateZ(-10deg);
            }
            40% {
                transform: rotateZ(7.5deg);
                -ms-transform: rotateZ(7.5deg);
                -webkit-transform: rotateZ(7.5deg);
            }
            50% {
                transform: rotateZ(-6deg);
                -ms-transform: rotateZ(-6deg);
                -webkit-transform: rotateZ(-6deg);
            }
            60% {
                transform: rotateZ(5deg);
                -ms-transform: rotateZ(5deg);
                -webkit-transform: rotateZ(5deg);
            }
            70% {
                transform: rotateZ(-4.28571deg);
                -ms-transform: rotateZ(-4.28571deg);
                -webkit-transform: rotateZ(-4.28571deg);
            }
            80% {
                transform: rotateZ(3.75deg);
                -ms-transform: rotateZ(3.75deg);
                -webkit-transform: rotateZ(3.75deg);
            }
            90% {
                transform: rotateZ(-3.33333deg);
                -ms-transform: rotateZ(-3.33333deg);
                -webkit-transform: rotateZ(-3.33333deg);
            }
            100% {
                transform: rotateZ(0deg);
                -ms-transform: rotateZ(0deg);
                -webkit-transform: rotateZ(0deg);
            }
        }
        /* конец кнопки звонка */
    </style>
@yield('css')
<!-- slick -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/slick/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/front/slick/slick-theme.css')}}">
    <!-- GG FONT -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/releases/v5.0.13/all.css')}}">
    {{--    @dd(cart())--}}
    <style>
        header .icon-menu::before {
            content:attr(value);

        }

        header .menu-main li {
            padding-right: 0px !important;
        }

        .labels {
            position: unset !important;
        }
    </style>
</head>
<body>
<header class="container" id="header-v3">

    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 logo"><a href="{{route('index')}}"><img
                    src="{{asset('logonew.png')}}" alt="holiwood"></a></div>
        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 menu-mobile">
            <div class=" collapse navbar-collapse" id="myNavbar">

                <form class="hidden-lg hidden-md form-group form-search-mobile">
                    <input type="text" name="search" placeholder="Search here..." class="form-control">
                    <button type="submit"><img src="{{asset('assets/front/img/Search.png')}}" alt="search"
                                               class="img-responsive"></button>
                </form>
                <ul class="nav navbar-nav menu-main">
                    <li class="contact-menu"><a href="{{route('index')}}">{{getlang('home')}}</a>
                        <figure id="contact-1" class=" hidden-sm hidden-md hidden-xs"></figure>
                    </li>

                    <li class="shop-menu dropdown"><a href="#" class="dropdown-toggle"
                                                      data-toggle="dropdown">{{getlang('shop')}}</a>
                        <figure id="shop-1" class=" hidden-sm hidden-md hidden-xs"></figure>
                        <div class="dropdown-menu ">
                            <div class="container container-menu">
                                <ul class="row">
                                    <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <ul>
                                            @foreach(categories() as $category)
                                                <li class="col-lg-3 col-md-3 col-sm-12 col-xs-12 menu-home-lv2">
                                                    <ul>
                                                        @foreach($category->subs as $subcategory)
                                                            <li class="li-home li-one"><i
                                                                    class="fas fa-long-arrow-alt-right hidden-sm hidden-md hidden-xs"></i><a
                                                                    href="{{route('subcat.items',['category'=>$category->slug,'subcategory'=>$subcategory->slug])}}">{{$subcategory->title}}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </li>


                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="blog-menu dropdown">
                        <a href="{{route('about')}}" class="dropdown-toggle"
                           data-toggle="dropdown">{{getlang('info')}}</a>
                        <figure id="blog-1" class=" hidden-sm hidden-md hidden-xs"></figure>
                        <ul class="menu-home-lv2 dropdown-menu">
                            <li class="li-home"><i
                                    class="fas fa-long-arrow-alt-right hidden-sm hidden-md hidden-xs"></i><a
                                    href="{{route('about')}}">{{getlang('about')}}</a></li>
                            <li class="li-home"><i
                                    class="fas fa-long-arrow-alt-right hidden-sm hidden-md hidden-xs"></i><a
                                    href="{{route('return')}}">{{getlang('return')}}</a></li>
                            <li class="li-home"><i
                                    class="fas fa-long-arrow-alt-right hidden-sm hidden-md hidden-xs"></i><a
                                    href="{{route('delivery')}}">{{getlang('delivery')}}</a></li>
                        </ul>
                    </li>
                    <li class="contact-menu"><a href="{{route('contacts')}}">{{getlang('contacts')}}</a>
                        <figure id="contact-1" class=" hidden-sm hidden-md hidden-xs"></figure>
                    </li>
                    <li class="contact-menu"><a href="{{route('blog')}}">{{getlang('blog')}}</a>
                        <figure id="contact-1" class=" hidden-sm hidden-md hidden-xs"></figure>
                    </li>
                    <li class="contact-menu" style="{{Lang()=='am'?'display:none':""}}">
                        <a href="{{route('set.lang','am')}}"><img style="max-width: 30px"
                                                                  src="{{asset('flags/am.png')}}" alt=""></a>
                        <figure id="contact-1" class=" hidden-sm hidden-md hidden-xs"></figure>
                    </li>
                    <li class="contact-menu" style="{{Lang()=='ru'?'display:none':""}}">
                        <a href="{{route('set.lang','ru')}}"><img style="max-width: 30px"
                                                                  src="{{asset('flags/ru.png')}}" alt=""></a>
                        <figure id="contact-1" class=" hidden-sm hidden-md hidden-xs"></figure>
                    </li>
                    <li class="contact-menu" style="{{Lang()=='en'?'display:none':""}}">
                        <a href="{{route('set.lang','en')}}"><img style="max-width: 30px"
                                                                  src="{{asset('flags/en.png')}}" alt=""></a>
                        <figure id="contact-1" class=" hidden-sm hidden-md hidden-xs"></figure>
                    </li>
                    <li class="hidden-lg hidden-md"><a href="#"><i class="far fa-user"></i> My Account</a></li>
                    <li>
                        <figure id="btn-close-menu" class="hidden-lg hidden-md"><i class="far fa-times-circle"></i>
                        </figure>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-10 col-xs-9">
            <ul class="nav navbar-nav navbar-right icon-menu carttotal" value="{{!empty(cart()) ? cart()->count() : '0'}}">


                <li id="input-search" class="hidden-sm hidden-xs">
                    <a href="#"><img id="search-img" src="{{asset('assets/front/img/Search.png')}}" alt="search"></a>

                </li>
                <li class="icon-user hidden-sm hidden-xs"><a href="{{route('user.login')}}"><i class="far fa-user"></i></a>
                </li>
                <li class="dropdown cart-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset('assets/front/img/cart.png')}}" id="img-cart" alt="cart">
                    </a>
                    <div class="dropdown-menu ">
                        @php($total = 0)
                        <div class="headerCart">
                        @foreach(cart() as $cart)

                            <div class="cart-1">
                                <div class="img-cart"><img
                                        src="{{asset('productimages/'.$cart->product->image->image)}}"
                                        class="img-responsive" alt="holiwood"></div>
                                <div class="info-cart">
                                    <h1>{{$cart->product->title}}</h1>
                                    <span class="number">x{{$cart->qty}}</span>
                                    <span class="prince-cart">{{$cart->qty * $cart->product->price}}</span>
                                    @php($total +=$cart->qty * $cart->product->price)
                                </div>
                            </div>
                        @endforeach
                        </div>

                        <div class="total">
                            <span>{{getlang('Total')}}:</span>
                            <span>{{$total}} &#1423;</span>
                        </div>
                        <div id="div-cart-menu">
                            <a href="{{route('cart')}}" class="check">{{getlang('View cart')}}</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="navbar-header mobile-menu">
            <button type="button" class="navbar-toggle btn-menu-mobile" data-toggle="collapse" data-target="#myNavbar">
                <i class="fas fa-bars"></i>
            </button>
        </div>
    </div>

</header>
<div class="content-search">

    <div class="container container-100">
        <i class="far fa-times-circle" id="close-search"></i>
        <h3 class="text-center">{{getlang('WHAT ARE YOUR LOOKING FOR')}}</h3>
        <form method="get" action="/products" role="search" style="position: relative;">
            <input type="text" class="form-control control-search" value="" autocomplete="off" placeholder="{{getlang('Enter Search')}}" aria-label="SEARCH" name="search">

            <button class="button_search" type="submit">search</button>
        </form>
    </div>

</div>
@yield('content')


<div class="info-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <h3>{{getlang('Useful links')}}</h3>
                    <ul>
                        <li><i class="fas fa-long-arrow-alt-right"></i><a
                                href="{{route('contacts')}}">{{getlang('Contacts')}}</a></li>
                        {{--                        <li><i class="fas fa-long-arrow-alt-right"></i><a href="#">Trade Services</a></li>--}}
                        <li><i class="fas fa-long-arrow-alt-right"></i><a href="{{route('login')}}">{{getlang('Login/Register')}}</a>
                        </li>
                        <li><i class="fas fa-long-arrow-alt-right"></i><a href="{{route('delivery')}}">{{getlang('Delivery & Returns')}}</a></li>
                        <li><i class="fas fa-long-arrow-alt-right"></i><a href="{{route('faq')}}">{{getlang('FAQs')}}</a></li>
                        <li><i class="fas fa-long-arrow-alt-right"></i><a href="{{route('payment.terms')}}">{{getlang('Payment terms')}}</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <h3>{{getlang('Categories')}}</h3>

                    <ul>
                        @foreach(categories() as $category)
                            @if($category->footer =='yes')
                        <li><i class="fas fa-long-arrow-alt-right"></i><a href="{{route('category',$category->slug)}}">{{$category->name}}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 social">
                    <h3>{{@getlang('Order a call')}}</h3>
                    <h2>Նշեք Ձեր առիթը և հեռախոսահամարը, պատվիրեք անվճար զանգ և մենք կմշակենք Ձեր առիթին համապատասխան լավագույն առաջարկները և կկապնվենք Ձեզ հետ :</h2>
                    <button class="btn btn-success" data-toggle="modal" data-target="#exampleModal">{{@getlang('Order a call')}}</button>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 social">
                <p>{{getlang('CONNECT WITH US')}}</p>
                <a href="#"><i class="fa-brands fa-facebook-f" style="font-size: 30px"></i></a>
                <a href="#"><i class="fa-brands fa-instagram" style="font-size: 30px"></i></a>
                <a href="#"><i class="fa-brands fa-telegram" style="font-size: 30px"></i></a>

                <h3>{{getlang('Newsletter')}}</h3>
                <h2>{{getlang('Sign up for our mailing list to get latest updates and offers')}}</h2>
                <form class="form-group" id="newslatter" method="post">
                    <input type="text" name="email" placeholder="{{getlang('Your mail here')}}" class="input-lg">
                    <button type="submit"><img src="{{asset('assets/front/img/Send.png')}}" alt="holiwood"></button>
                </form>
            </div>
        </div>
    </div>
</div>
<footer>
{{--    <div title="Контакты" class="supportcss" data-toggle="modal" data-target="#exampleModal">{{getlang('Order a call')}}</div>--}}
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 logo-footer">
                <a href="#" >
                    <img src="{{asset('logonew.png')}}" alt="">
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 copy">
                <span>Copyright</span><i class="far fa-copyright"></i><span class="engo">2022</span>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 gmail-footer">
                <span id="gmail-footer"><a href="#">E: support@amokflowers.com</a></span>
            </div>
        </div>
    </div>
    <div class="hidden-lg hidden-md back-to-top fade"><i class="fas fa-caret-up"></i></div>
    <div class="BG-menu"></div>
</footer>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 99999">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <form style="margin: 0 auto" id="call">
                <h2>{{getlang('Order a call')}}</h2>
                <div class="form-group">
                    <label for="exampleInputEmail1">{{getlang('Tema')}}</label>
                    <select name="tema" class="form-control" id="">
                        <option value="wedding">
                            {{getlang('wedding')}}
                        </option>
                        <option value="flowers">
                            {{getlang('flowers')}}
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">{{getlang('phone')}}</label>
                    <input name="phone" type="text" class="form-control" id="exampleInputPassword1" placeholder="Phone">
                </div>

                <button type="submit" class="btn btn-primary">{{getlang('send')}}</button>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{getlang('Close')}}</button>
            </div>

        </div>
    </div>
</div>
<!-- boostrap & jquery -->
<script src="{{asset('assets/front/js/jquery.min_af.js')}}"></script>
<script src="{{asset('assets/front/js/bootstrap.min_0028.js')}}"></script>
<script src="{{asset('assets/front/slick/slick.js')}}" type="text/javascript" charset="utf-8"></script>
<!-- js file -->
<script src="{{asset('assets/front/js/function-slick.js')}}"></script>

<script src="{{asset('assets/front/js/function-time-counter.js')}}"></script>
<script src="{{asset('assets/front/js/function-input-number.js')}}"></script>
<script src="{{asset('assets/front/js/function-flower.js')}}"></script>
<script src="{{asset('assets/front/js/function-select-custom.js')}}"></script>
<script src="{{asset('assets/front/js/function-back-top.js')}}"></script>
<script src="{{asset('assets/front/js/function-sidebar.js')}}"></script>
<script src="{{asset('assets/front/js/funtion-header-v3.js')}}"></script>
<script src="{{asset('assets/front/js/function-search-v2.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"
        integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(() => {
        $('#call').submit(function (e){
            e.preventDefault();
            var tema = $(this).find('select[name=tema]:selected').val();
            var phone = $(this).find('input[name=phone]').val();
            $.ajax({
                url:"{{route('call')}}",
                type:"POST",
                dataType:"JSON",
                data:{
                    tema:tema,
                    phone:phone,
                },success:function(data){
                    console.log(data)
                    toastr.success('Ձեր զանգը հաջողությամբ պատվիրվել է')
                }
            })
        })
        $(() => {
            // -------time counter-------------------
            {{--console.log("{{round(microtime(true) * 1000 + 8,64e+7)}}")--}}
            // var target_date = new Date().getTime() + (86400*2*2); // set the countdown date
            // var days, hours, minutes, seconds; // variables for time units
            //
            // var countdown = document.getElementById("tiles"); // get tag element
            //
            // getCountdown();
            //
            // setInterval(function () { getCountdown(); }, 1000);
            //
            // function getCountdown(){
            //
            //     // find the amount of "seconds" between now and target
            //     var current_date = new Date().getTime();
            //     var seconds_left = (target_date - current_date) / 1000;
            //
            //     days = pad( parseInt(seconds_left / 86400) );
            //     seconds_left = seconds_left % 86400;
            //
            //     hours = pad( parseInt(seconds_left / 3600) );
            //     seconds_left = seconds_left % 3600;
            //
            //     minutes = pad( parseInt(seconds_left / 60) );
            //     seconds = pad( parseInt( seconds_left % 60 ) );
            //
            //     // format countdown string + set tag value
            //     countdown.innerHTML = "<span>" + days + "</span><span>" + hours + "</span><span>" + minutes + "</span><span>" + seconds + "</span>";
            // }
            //
            // function pad(n) {
            //     return (n < 10 ? '0' : '') + n;
            // }
            @if(isset($stock))
            @if($stock->to>time())

            var upgradeTime = {{$stock->to-time()}};
            var seconds = upgradeTime;
            var countdown = document.getElementById("tiles"); // get tag element
            function timer() {
                var days = Math.floor(seconds / 24 / 60 / 60);
                var hoursLeft = Math.floor((seconds) - (days * 86400));
                var hours = Math.floor(hoursLeft / 3600);
                var minutesLeft = Math.floor((hoursLeft) - (hours * 3600));
                var minutes = Math.floor(minutesLeft / 60);
                var remainingSeconds = seconds % 60;

                function pad(n) {
                    return (n < 10 ? "0" + n : n);
                }

                countdown.innerHTML = "<span>" + pad(days) + "</span><span>" + pad(hours) + "</span><span>" + pad(minutes) + "</span><span>" + pad(remainingSeconds) + "</span>";
                // countdown.innerHTML = pad(days) + ":" + pad(hours) + ":" + pad(minutes) + ":" + pad(remainingSeconds);
                if (seconds == 0) {
                    clearInterval(countdownTimer);
                    document.getElementById('countdown').innerHTML = "Completed";
                } else {
                    seconds--;
                }
            }

            var countdownTimer = setInterval(timer, 1000);
            @endif
            @endif
        })

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click', '.addToCart', function (e) {
            e.preventDefault();
            var product_id = $(this).data('id');
            $.ajax({
                url: "{{route('add.to.cart')}}",
                type: "POST",
                dataType: "JSON",
                data: {
                    product_id: product_id,
                    qty: 1,
                },
                success: function (data) {
                    toastr.success("{{getlang('addcart')}}");
                    UpdateHeaderCart();
                }
            })
        })

        function UpdateHeaderCart() {
            $.ajax({
                url: "{{route('get.updateCart')}}",
                type: "POST",
                dataType: "JSON",
                data: {}, success: function (data) {
                    $('.carttotal').attr('value',data.cart.length)
                    if (data.cart.length > 0) {
                        $('.headerCart').html('');
                        var total = 0;
                        $.map(data.cart, function (elem) {
                            var append = `<div class="cart-1">
                            <div class="img-cart"><img src="/productimages/${elem.product.image.image}" class="img-responsive" alt="holiwood"></div>
                            <div class="info-cart">
                                <h1>${elem.product.title}</h1>
                                <span class="number">x${elem.qty}</span>
                                <span class="prince-cart">${elem.qty * elem.product.price} &#1423;</span>
                                                            </div>
                        </div>`;
                            total += elem.qty * elem.product.price;
                            $('.headerCart').append(append);


                        });
                        $('.total').find('span').eq(1).html(total + " &#1423;")
                    }
                }

            })
        }
        function emailValidation(email){
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        $('#newslatter').submit(function (e){
            e.preventDefault();
            e.stopPropagation();
            var email = $(this).find('input[type=text]').val();
            if(!emailValidation(email)){
                toastr.error("{{getlang('emailerror')}}")
            }else{
                $.ajax({
                    url:"{{route('newsletter')}}",
                    type:"POST",
                    dataType:"JSON",
                    data:{
                        email:email,
                    },success:function(data){
                        toastr.success("{{getlang('Thanks for subscribing')}}");
                        $('#newslatter').find('input[type=text]').val('');
                    }
                })
            }
        })
    })
</script>
@yield('script')
</body>
</html>
