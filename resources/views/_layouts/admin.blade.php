<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Reality | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
          href="{{asset('assets/admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/admin/dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/summernote/summernote-bs4.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    @yield('style')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->


        <!-- Right navbar links -->
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{route('index','en')}}" class="brand-link">
            <img src="{{asset('assets/admin/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
                 class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">Dashboard</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{asset('assets/admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2"
                         alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">
                        {{--                        {{Auth::user()->name}}--}}
                    </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">

{{--                    <li class="nav-item has-treeview">--}}
{{--                        <a href="#" class="nav-link">--}}
{{--                            <i class="nav-icon fas fa-tree"></i>--}}
{{--                            <p>--}}
{{--                                Layouts--}}
{{--                                <i class="fas fa-angle-left right"></i>--}}
{{--                            </p>--}}
{{--                        </a>--}}
{{--                        <ul class="nav nav-treeview">--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="{{route('admin.add.floor')}}" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Add Floor</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="{{route('admin.add.layout.details')}}" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Add details</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="{{route('admin.layouts.list')}}" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>List</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="pages/UI/sliders.html" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Sliders</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="pages/UI/modals.html" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Modals & Alerts</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="pages/UI/navbar.html" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Navbar & Tabs</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="pages/UI/timeline.html" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Timeline</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="pages/UI/ribbons.html" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Ribbons</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
                    <li class="nav-item">
                        <a href="{{route('admin.layouts.list')}}" class="nav-link">
                            <i class="nav-icon fa-solid fa-screwdriver-wrench"></i>
                            <p>
                                Layouts
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.settings')}}" class="nav-link">
                            <i class="nav-icon fa-solid fa-screwdriver-wrench"></i>
                            <p>
                                Settings
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.translation')}}" class="nav-link">
                            <i class="nav-icon fa-solid fa-screwdriver-wrench"></i>
                            <p>
                                Translations
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.video')}}" class="nav-link">
                            <i class="nav-icon fa-solid fa-screwdriver-wrench"></i>
                            <p>
                                Video
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                         <a href="{{route('admin.photo-gallery-index')}}" class="nav-link">
                             <svg xmlns="http://www.w3.org/2000/svg" width="25" class="h-5 w-5 nav-icon" viewBox="0 0 20 20" fill="currentColor">
                                 <path d="M3 4a1 1 0 011-1h12a1 1 0 011 1v2a1 1 0 01-1 1H4a1 1 0 01-1-1V4zM3 10a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H4a1 1 0 01-1-1v-6zM14 9a1 1 0 00-1 1v6a1 1 0 001 1h2a1 1 0 001-1v-6a1 1 0 00-1-1h-2z" />
                             </svg>
                            <p>
                                Gallery
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                         <a href="{{route('admin.partners-index')}}" class="nav-link">
                             <svg xmlns="http://www.w3.org/2000/svg" width="25" class="h-5 w-5 nav-icon" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                 <path stroke-linecap="round" stroke-linejoin="round" d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                             </svg>
                            <p>
                                Partners
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                         <a href="{{route('admin.about.us')}}" class="nav-link">
                             <svg xmlns="http://www.w3.org/2000/svg" width="25" class="h-5 w-5 nav-icon" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                 <path stroke-linecap="round" stroke-linejoin="round" d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                             </svg>
                            <p>
                                About us
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                         <a href="{{route('admin.about.us.icons')}}" class="nav-link">
                             <svg xmlns="http://www.w3.org/2000/svg" width="25" class="h-5 w-5 nav-icon" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                 <path stroke-linecap="round" stroke-linejoin="round" d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                             </svg>
                            <p>
                                About us icons
                            </p>
                        </a>
                    </li>
                    {{--                    <li class="nav-item has-treeview">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="nav-icon fas fa-edit"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Forms--}}
                    {{--                                <i class="fas fa-angle-left right"></i>--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                        <ul class="nav nav-treeview">--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/forms/general.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>General Elements</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/forms/advanced.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Advanced Elements</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/forms/editors.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Editors</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/forms/validation.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Validation</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item has-treeview">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="nav-icon fas fa-table"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Tables--}}
                    {{--                                <i class="fas fa-angle-left right"></i>--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                        <ul class="nav nav-treeview">--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/tables/simple.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Simple Tables</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/tables/data.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>DataTables</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/tables/jsgrid.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>jsGrid</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-header">EXAMPLES</li>--}}
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="pages/calendar.html" class="nav-link">--}}
                    {{--                            <i class="nav-icon far fa-calendar-alt"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Calendar--}}
                    {{--                                <span class="badge badge-info right">2</span>--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="pages/gallery.html" class="nav-link">--}}
                    {{--                            <i class="nav-icon far fa-image"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Gallery--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item has-treeview">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="nav-icon far fa-envelope"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Mailbox--}}
                    {{--                                <i class="fas fa-angle-left right"></i>--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                        <ul class="nav nav-treeview">--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/mailbox/mailbox.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Inbox</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/mailbox/compose.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Compose</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/mailbox/read-mail.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Read</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item has-treeview">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="nav-icon fas fa-book"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Pages--}}
                    {{--                                <i class="fas fa-angle-left right"></i>--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                        <ul class="nav nav-treeview">--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/invoice.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Invoice</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/profile.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Profile</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/e-commerce.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>E-commerce</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/projects.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Projects</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/project-add.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Project Add</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/project-edit.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Project Edit</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/project-detail.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Project Detail</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/contacts.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Contacts</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item has-treeview">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="nav-icon far fa-plus-square"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Extras--}}
                    {{--                                <i class="fas fa-angle-left right"></i>--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                        <ul class="nav nav-treeview">--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/login.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Login</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/register.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Register</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/forgot-password.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Forgot Password</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/recover-password.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Recover Password</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/lockscreen.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Lockscreen</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/legacy-user-menu.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Legacy User Menu</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/language-menu.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Language Menu</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/404.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Error 404</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/500.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Error 500</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/pace.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Pace</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="pages/examples/blank.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Blank Page</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="starter.html" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Starter Page</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-header">MISCELLANEOUS</li>--}}
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="https://adminlte.io/docs/3.0" class="nav-link">--}}
                    {{--                            <i class="nav-icon fas fa-file"></i>--}}
                    {{--                            <p>Documentation</p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-header">MULTI LEVEL EXAMPLE</li>--}}
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="fas fa-circle nav-icon"></i>--}}
                    {{--                            <p>Level 1</p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item has-treeview">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="nav-icon fas fa-circle"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Level 1--}}
                    {{--                                <i class="right fas fa-angle-left"></i>--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                        <ul class="nav nav-treeview">--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="#" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Level 2</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item has-treeview">--}}
                    {{--                                <a href="#" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>--}}
                    {{--                                        Level 2--}}
                    {{--                                        <i class="right fas fa-angle-left"></i>--}}
                    {{--                                    </p>--}}
                    {{--                                </a>--}}
                    {{--                                <ul class="nav nav-treeview">--}}
                    {{--                                    <li class="nav-item">--}}
                    {{--                                        <a href="#" class="nav-link">--}}
                    {{--                                            <i class="far fa-dot-circle nav-icon"></i>--}}
                    {{--                                            <p>Level 3</p>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="nav-item">--}}
                    {{--                                        <a href="#" class="nav-link">--}}
                    {{--                                            <i class="far fa-dot-circle nav-icon"></i>--}}
                    {{--                                            <p>Level 3</p>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="nav-item">--}}
                    {{--                                        <a href="#" class="nav-link">--}}
                    {{--                                            <i class="far fa-dot-circle nav-icon"></i>--}}
                    {{--                                            <p>Level 3</p>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                </ul>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="#" class="nav-link">--}}
                    {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Level 2</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="fas fa-circle nav-icon"></i>--}}
                    {{--                            <p>Level 1</p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-header">LABELS</li>--}}
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="nav-icon far fa-circle text-danger"></i>--}}
                    {{--                            <p class="text">Important</p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="nav-icon far fa-circle text-warning"></i>--}}
                    {{--                            <p>Warning</p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="nav-icon far fa-circle text-info"></i>--}}
                    {{--                            <p>Informational</p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">{{isset($page_title) ? $page_title :""}}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Home</a></li>
                            <li class="breadcrumb-item active">{{isset($page_title) ? $page_title :""}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        @yield('content')
    </div>

    <footer class="main-footer">
        <strong>Copyright &copy; 2020 </strong>
        All rights reserved.

    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('assets/admin/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('assets/admin/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('assets/admin/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
{{--<script src="{{asset('assets/admin/plugins/jqvmap/jquery.vmap.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/admin/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>--}}
<!-- jQuery Knob Chart -->
<script src="{{asset('assets/admin/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('assets/admin/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('assets/admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('assets/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('assets/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/admin/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('assets/admin/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('assets/admin/dist/js/demo.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $(() => {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    })
    $(document).ready(function () {
        var dateToday = new Date();

        $('.selectcustom2').select2();
        $('#datepicker').datepicker({
            autoclose: true,
            dateFormat: 'yy-mm-dd',
            minDate: dateToday,

        })
    });
</script>
<script src="https://kit.fontawesome.com/8bcefe98db.js" crossorigin="anonymous"></script>

@yield('script')
</body>
</html>
