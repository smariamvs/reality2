@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Orders List</h3>
                        <div class="d-flex justify-content-end">
                            <span><a href="?payed=1">Payed</a></span>  &nbsp;
                            <span> <a href="{{route('admin.order')}}">All</a></span>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Amount</th>
                                <th>Product id</th>
                                <th>First name</th>
                                <th>Last Name</th>
                                <th>Street addres</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Comment</th>
                                <th>Payment method</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                        @php($ordprod = json_decode($order->products_id,true))
{{--                                    @dd($ordprod)--}}
                                    <td>{{$order->amount}}</td>
                                    <td>
                                        @if($ordprod!=null)
                                        @foreach($ordprod as $keys => $prod)
                                            <p>{{\App\Models\Products::where('lang','en')->where('unique_id',$keys)->first()->title}} x {{$prod}}</p>
                                            @endforeach
                                            @endif
                                    </td>
                                    <td>{{$order->last_name}}</td>
                                    <td>{!! $order->first_name !!}</td>
                                    <td>{{$order->street_addres}}</td>
                                    <td>{{$order->phone}}</td>
                                    <td>{{$order->email}}</td>
                                    <td>{{$order->comment}}</td>
                                    <td>{{$order->payment}}</td>
                                    <td>{{$order->status==0?"No payed":"Payed"}}</td>
                                    <td>{{$order->created_at}}</td>

                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        </div>

    </section>
    <!-- /.modal -->
@endsection
