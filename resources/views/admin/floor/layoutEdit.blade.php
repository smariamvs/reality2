@extends('_layouts.admin')
@section('content')
    <style>
    .form-check{
        padding-right: 10px;
    }
    </style>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Edit floor details</h3>
                        <div class="d-flex justify-content-end">

                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">


                        <div class="row">
                            <div class="col-md-12">
                                <section>
                                    <form action="{{route('admin.store.floor')}}" method="POST"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="floor-list" style="width: 800px">

                                        <img id="vegetables" style="width: 800px; max-width: 800px" src="{{asset('uploads/'.$layout->image)}}"
                                                 usemap="#veg"/>

                                            <map id="veg_map" name="veg">
                                                @if($layout->details->count()>0)
                                                    @foreach($layout->details as $detail)
                                                        @php
                                                            $d = $detail->detail;

                                                            $rand = str_random(9);
                                                            $d = str_replace('name=""',"name='$rand'",$d);
                                                           $d = str_replace('data-bussy=""',"data-bussy='$detail->busy' data-ids='$detail->id' data-image='".asset('uploads/'.$detail->image)."'",$d);
                                                        @endphp
                                                        {!! $d !!}
                                                    @endforeach
                                                @endif
                                            </map>
                                        </div>
                                        <button type="submit" class="btn btn-success">Add</button>
                                    </form>
                                </section>
                                <!-- Section: Live preview -->

                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <div class="modal fade bd-example-modal-lg showModalDetails" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form action="{{route('admin.update.layout.details')}}" method="post"  enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit flool detail</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="row mb-0">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="busy"  value="yes" id="busyYes">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                           Yes
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="busy" value="no" id="busyNo" checked>
                                        <label class="form-check-label" for="flexRadioDefault2">
                                            No
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <img src="" id="layoutdetailimage" class="img-fluid">

                            <div class="form-group" style="margin: 10px">
                                <input type="file" name="image" id="image">
                                <div>
                                    <img src="" id="img" style="display: none; width: 100%" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- /.modal -->
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/imagemapster@1.5.4/dist/jquery.imagemapster.js"></script>
    <script>


        let image = $('#vegetables');
        var areas = [];
        $('map').find('area').each(function () {
            var color = "33ff00b8";
            if ($(this).data('bussy') == 'yes') {
                color = 'ff0000c7'
            }
            areas.push({
                key: $(this).attr('name'),
                fillColor: color
            })
        })
        image.mapster(
            {

                fillOpacity: 0.8,
                fillColor: "ef280b",
                stroke: true,
                strokeColor: "3320FF",
                strokeOpacity: 0.8,
                strokeWidth: 0.2,
                singleSelect: true,
                mapKey: 'name',
                listKey: 'name',
                toolTipClose: ["tooltip-click", "area-click"],
                areas: areas
            });

        $('map area').click(function (e){
            e.preventDefault()
        $('.showModalDetails').modal('show')
            $('#layoutdetailimage').attr('src',$(this).data('image'));
            $('input[name=id]').val($(this).data('ids'));
            var busy = $(this).data('bussy');
            if(busy=='yes'){
                $('#busyYes').prop('checked',true)
                $('#busyNo').prop('checked',false)

            }else if(busy=='no'){
                $('#busyYes').prop('checked',false)
                $('#busyNo').prop('checked',true)


            }

        })


    </script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img').attr('src', e.target.result);
                    $('#img').show();
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#image").change(function () {
            readURL(this);

        });
    </script>
@endsection

