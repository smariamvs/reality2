@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Add floor
                        </h3>
                        <div class="d-flex justify-content-end">

                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">


                        <div class="row">
                            <div class="col-xl-12 mb-4 mb-xl-0">
                                <section>
                                    <form action="{{route('admin.save.layout.details')}}" method="POST"
                                          enctype="multipart/form-data">
                                        @csrf

                                        <div class="tab-content" id="myTabContent">

                                            <div class="tab-pane fade active show" id="home" role="tabpanel"
                                                 aria-labelledby="home-tab">
                                                <div class="card card-primary">


                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="parent_id">Parent</label>
                                                                <select name="parent_id" class="form-control"
                                                                        id="parent_id">
                                                                    @foreach($floors as $floor)
                                                                        <option
                                                                            value="{{$floor->id}}" {{$floor->id==4?'selected':""}}>{{$floor->floor}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="layoutDetails">
                                                            <button class="btn btn-success adddetail" type="button"><i
                                                                    class="fa fa-plus"></i></button>
                                                            <div class="form-group">
                                                                <div class="mb-3">
                                                                    <label for="detail">Detail</label>
                                                                    <textarea name="details[]"
                                                                              class="form-control"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>


                                                </div>
                                            </div>


                                        </div>
                                        <button type="submit" class="btn btn-success">Add</button>
                                    </form>
                                </section>
                                <!-- Section: Live preview -->

                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.modal -->
@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img').attr('src', e.target.result);
                    $('#img').show();
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#image").change(function () {
            readURL(this);
        });

        $('.adddetail').click(function (e) {
            var append = `<div class="form-group appended">
                                                            <div class="mb-3">
                                                                <label for="detail">Detail <button type="button" class="btn btn-danger removedetails"><i class="fa fa-minus"></i></button></label>
                                                                <textarea name="details[]" class="form-control"></textarea>
                                                            </div>
                                                        </div>`;
            $('.layoutDetails').append(append)
        });

        $(document).on('click','.removedetails',function (){
            $(this).closest('.appended').remove();
        })
    </script>
@endsection

