@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Layouts list</h3>
                        <div class="d-flex justify-content-end">

                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Floor</th>
                                <th>Layout</th>


                                <th>*</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($layouts as $key => $layout)
                                @if(!empty($layout->details))
                                    <tr>

                                        <td>{{$layout->floor}}</td>
                                        <td>
                                            <img width="300px" src="{{asset('uploads/'.$layout->image)}}" alt="">
                                        </td>

                                        <td>

                                            <a href="{{route('admin.layouts.edit',$layout->id)}}" class="btn btn-warning">
                                                <i class="fas fa-edit"></i> Edit
                                            </a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        </div>

    </section>
    <!-- /.modal -->
@endsection
