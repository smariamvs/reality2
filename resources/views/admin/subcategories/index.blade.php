@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Все подкатегории</h3>
                        <div class="d-flex justify-content-end">
                            <a href="{{route('admin.subcategories.create')}}" class="btn btn-default">
                                Добавить подкатегория
                            </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Картинка</th>
                                <th>Название</th>
                                <th>Категория</th>
                                <th>Подвал</th>

                                <th>*</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $key => $category)
                                <tr>

                                    <td><img src="{{asset('uploads/'.$category->image)}}" width="150px" alt=""></td>
                                    <td>{{$category->title}}</td>
                                    <td>{{$category->category->name}}</td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" {{$category->footer=='yes'?'checked':""}} name="footer" type="checkbox" value="" data-id="{{$category->slug}}">
                                        </div>
                                    </td>

                                    <td>
                                        <a href="{{route('admin.subcategories.edit',$category->slug)}}" class="btn btn-info">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                        <a href="{{route('admin.sliders.delete',$category->slug)}}" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                            <i class="fas fa-trash-alt"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        </div>

    </section>
    <!-- /.modal -->
@endsection
@section('script')
    <script>
        $(()=>{
            $('input[name=footer]').on('change',function (){
                var checked = $(this).prop('checked');
                var slug = $(this).data('id');
                $.ajax({
                    url:"{{route('change.subcategory.footer')}}",
                    type:"POST",
                    dataType:"JSON",
                    data:{
                        slug:slug,
                        footer:checked
                    },success:function(data){

                    }
                })
            })
        })
    </script>
@endsection
