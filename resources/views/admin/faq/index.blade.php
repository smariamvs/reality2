@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Faqs</h3>
                        <div class="d-flex justify-content-end">
                            <a href="{{route('admin.faq.create')}}" class="btn btn-default">
                                Добавить
                            </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Описание</th>
                                <th>*</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($faqs as $faq)
                                <tr>

                                    <td>{{$faq->title}}</td>
                                    <td>{{ mb_strimwidth(strip_tags ($faq->description), 0, 50, "...")}}</td>
                                    <td>
                                        <a href="{{route('admin.faq.edit',$faq->unique_id)}}" class="btn btn-info">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                        <a href="{{route('admin.product.delete',1)}}" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                            <i class="fas fa-trash-alt"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        </div>

    </section>
    <!-- /.modal -->
@endsection
