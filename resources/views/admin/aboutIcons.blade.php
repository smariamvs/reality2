@extends('_layouts.admin')
@section('content')

    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">About icons</h3>
                        <div class="d-flex justify-content-end">
                            <a class="btn btn-success" href="{{route('admin.about.us.icons.add')}}">Add Icon</a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Icon</th>
                                <th>Text</th>
                                <th>*</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($icons as $key => $icon)
                                    <tr>

                                        <td>
                                            <i style="font-size: 37px;color:green" class="{{$icon->icon}}"></i>
                                        </td>
                                        <td>
                                            {{$icon->text}}
                                        </td>

                                        <td>

                                            <a href="{{route('admin.edit.about.icon',$icon->unique_id)}}" class="btn btn-warning">
                                                <i class="fas fa-edit"></i> Edit
                                            </a>

                                            <a href="{{route('admin.delete.about.us.icon',$icon->unique_id)}}" class="btn btn-danger">
                                                <i class="fas fa-trash"></i> Delete
                                            </a>
                                        </td>
                                    </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        </div>

    </section>
    <!-- /.modal -->
@endsection
