@extends('_layouts.admin')
@section('style')
    <style>
        .object-cover{
            object-fit: cover;
        }
        .right-0{
            right: 0;
        }

        .reorder_link {
            color: #3675B4;
            border: solid 2px #3675B4;
            border-radius: 3px;
            text-transform: uppercase;
            background: #fff;
            font-size: 18px;
            padding: 10px 20px;
            margin: 15px 15px 15px 0px;
            font-weight: bold;
            text-decoration: none;
            transition: all 0.35s;
            -moz-transition: all 0.35s;
            -webkit-transition: all 0.35s;
            -o-transition: all 0.35s;
            white-space: nowrap;
        }
        .reorder_link:hover {
            color: #fff;
            border: solid 2px #3675B4;
            background: #3675B4;
            box-shadow: none;
        }
        #reorder-helper{
            margin: 18px 10px;
            padding: 10px;
        }
        .light_box {
            background: #efefef;
            padding: 20px;
            margin: 15px 0;
            text-align: center;
            font-size: 1.2em;
        }

        /* image gallery */
        .gallery{
            width:100%;
            float:left;
            margin-top:15px;
        }
        .gallery ul{
            margin:0;
            padding:0;
            list-style-type:none;
        }
        .gallery ul li{
            padding:7px;
            border:2px solid #ccc;
            float:left;
            margin:10px 7px;
            background:none;
            width:auto;
            height:auto;
        }
        .gallery img{
            width:250px;
        }

        /* notice box */
        .notice, .notice a{
            color: #fff !important;
        }
        .notice {
            z-index: 8888;
            padding: 10px;
            margin-top: 20px;
        }
        .notice a {
            font-weight: bold;
        }
        .notice_error {
            background: #E46360;
        }
        .notice_success {
            background: #657E3F;
        }
    </style>
@endsection
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Partners</h3>
                        <div class="d-flex justify-content-end">
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                                Add Partner
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div>
                                        <a href="javascript:void(0);" class="btn outlined mleft_no reorder_link" id="save_reorder">reorder photos</a>
                                        <div id="reorder-helper" class="light_box" style="display:none;">1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished.</div>
                                        <div class="gallery">
                                            <ul class="reorder_ul reorder-photos-list">
                                                @foreach($partners as $item)
                                                    <li id="{{'image_li_' . $item->id}}" class="ui-sortable-handle position-relative">
                                                        <a href="{{route('admin.partners-delete',['id'=>$item->id])}}" class="btn btn-danger position-absolute top-0 right-0 text-white" onclick="return confirm('Are you sure?')">x</a> <a href="javascript:void(0);" style="float:none;" class="image_link"><img src="{{$item->image}}" width="150" height="200" alt="" class="object-cover"></a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>

            </div>

        </div>

    </section>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="POST" class="was-validated" action="{{route('admin.partners-save')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="card card-primary">

                            <!-- /.card-header -->
                            <!-- form start -->

                            <div class="card-body">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="images[]" id="validatedCustomFile" accept="images/*" required multiple>
                                    <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                    <div class="invalid-feedback">Please choose at least 1 image</div>
                                </div>


                            </div>


                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endsection
@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.reorder_link').on('click',function(){
                $("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
                $('.reorder_link').html('save reordering');
                $('.reorder_link').attr("id","saveReorder");
                $('#reorderHelper').slideDown('slow');
                $('.image_link').attr("href","javascript:void(0);");
                $('.image_link').css("cursor","move");

                $("#saveReorder").click(function( e ){
                    if( !$("#saveReorder i").length ){
                        $(this).html('').prepend('<img src="images/refresh-animated.gif"/>');
                        $("ul.reorder-photos-list").sortable('destroy');
                        $("#reorderHelper").html("Reordering Photos - This could take a moment. Please don't navigate away from this page.").removeClass('light_box').addClass('notice notice_error');

                        var h = [];
                        $("ul.reorder-photos-list li").each(function() {
                            h.push($(this).attr('id').substr(9));
                        });

                        $.ajax({
                            type: "POST",
                            url: "{{route('admin.gallery-change-order')}}",
                            data: {ids: " " + h + ""},
                            success: function(res){
                                window.location.reload();
                            }
                        });
                        return false;
                    }
                    e.preventDefault();
                });
            });
        });
    </script>
    <script>
        $('.edites').click(function (e){
            // let id = $(this).data('id');
            // let key = $(this).data('key');
            // let am = $(this).data('am');
            // let en = $(this).data('en');
            // let ru = $(this).data('ru');
            //
            // $('#editkey').val(key);
            // $('#editam').val(am);
            // $('#editru').val(ru);
            // $('#editen').val(en);
            // $('#editid').val(id);

        })
    </script>
@endsection
