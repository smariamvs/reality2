@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                            @if (\Session::has('success'))
                                <div class="alert alert-success">
                                    <ul>
                                        <li>{!! \Session::get('success') !!}</li>
                                    </ul>
                                </div>
                            @endif
                            @if ($video)
                                <div class="card text-center">
                                    <div class="card-header">
                                        Main page video already exists. If You want to change it, upload new one.str_random
                                    </div>
                                    <div class="card-body">
                                        <video style="width: 100%" src="{{$video->value}}" controls></video>
                                    </div>
                                </div>
                            @endif
                            <div class="card text-center">
                                <div class="card-header">
                                    Main Page Video
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title">Add or Edit main page video</h5>
                                    <p class="card-text"></p>
                                    <form action="{{route('admin.save-video')}}" enctype="multipart/form-data" method="POST">
                                        @csrf

                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="video" id="validatedCustomFile"  accept="video/*" required>
                                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                                        </div>
                                        <div class="mt-5">
                                            <button class="btn btn-success">Save Video</button>
                                        </div>
                                    </form>

                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
