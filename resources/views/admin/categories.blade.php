@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                                    @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                        {{ $error }}
                                </div>
                                    @endforeach

                            </div>
                        @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                               {!! \Session::get('success') !!}

                        </div>
                    @endif
                        <div class="card-header">
                            <h3 class="card-title">Product Categories </h3>
                            <div class="d-flex justify-content-end">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                                    Add Category
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Footer</th>

                                    <th>*</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $cat)
                                <tr>

                                    <td>{{$cat->id}}</td>
                                    <td>{{$cat->name}}</td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input" {{$cat->footer=='yes'?'checked':""}} name="footer" type="checkbox" value="" data-id="{{$cat->slug}}">
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-info">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                        <a href="{{route('admin.category.delete',$cat->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                            <i class="fas fa-trash-alt"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>

    </section>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="POST" enctype="multipart/form-data" action="{{route('admin.categories.add')}}">
                    @csrf
                <div class="modal-body">
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name_am">Name Am</label>
                                    <input type="text" name="name_am" class="form-control" id="name_am" placeholder="Name Armenian">
                                </div>
                                <div class="form-group">
                                    <label for="name_am">Description Am</label>
                                    <input type="text" name="desc_am" class="textarea" id="desc_am">
                                </div>
                                <div class="form-group">
                                    <label for="name_ru">Name Ru</label>
                                    <input type="text" name="name_ru" class="form-control" id="name_ru" placeholder="Name Russian">
                                </div>
                                <div class="form-group">
                                    <label for="name_am">Description Ru</label>
                                    <input type="text" name="desc_ru" class="textarea" id="desc_ru">
                                </div>
                                <div class="form-group">
                                    <label for="name_en">Name En</label>
                                    <input type="text" name="name_en" class="form-control" id="name_en" placeholder="Name English">
                                </div>
                                <div class="form-group">
                                    <label for="name_am">Description En</label>
                                    <input type="text" name="desc_en" class="textarea" id="desc_en">
                                </div>

                                <div class="form-group">
                                    <label for="name_en"></label>
                                    <input type="file" name="image" class="form-control" id="image" placeholder="Name English">
                                </div>
                             </div>


                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
@section('script')
    <script>
        $(()=>{
            $('input[name=footer]').on('change',function (){
               var checked = $(this).prop('checked');
               var slug = $(this).data('id');
               $.ajax({
                   url:"{{route('change.category.footer')}}",
                   type:"POST",
                   dataType:"JSON",
                   data:{
                       slug:slug,
                       footer:checked
                   },success:function(data){

                   }
               })
            })
        })
    </script>
@endsection
