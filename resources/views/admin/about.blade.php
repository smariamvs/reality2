@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Edit About</h3>
                        <div class="d-flex justify-content-end">

                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">


                        <div class="row">
                            <div class="col-xl-12 mb-4 mb-xl-0">
                                <section>
                                    <form action="{{route('admin.about.store')}}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item waves-effect waves-light">
                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Am</a>
                                        </li>
                                        <li class="nav-item waves-effect waves-light">
                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Ru</a>
                                        </li>
                                        <li class="nav-item waves-effect waves-light">
                                            <a class="nav-link"  id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">En</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">

                                        <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                                            <div class="card card-primary">

                                                <div class="ads_images">
                                                    <input index2="0" type="file" name="ads_images" class="indexes is_free" style="display: none;">
                                                </div>
                                                    <div class="card-body">

                                                         <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_am">Text</label>
                                                                <textarea class="textarea" id="about_am" name="about_am" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px; display: none;">
                                                                    {{$about_am->text}}
                                                                </textarea>

                                                            </div>
                                                        </div>
                                                    </div>


                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                            <div class="card card-primary">


                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_ru">Text</label>
                                                                <textarea class="textarea" id="about_ru" name="about_ru" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px; display: none;">
                                                                    {{$about_ru->text}}
                                                                </textarea>

                                                            </div>
                                                        </div>

                                                    </div>

                                            </div>
                                        </div>
                                        <div class="tab-pane fade " id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                            <div class="card card-primary">


                                                <div class="card-body">

                                                    <div class="form-group">
                                                        <div class="mb-3">
                                                            <label for="desc_en">Text</label>
                                                            <textarea class="textarea" id="about_en" name="about_en" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px; display: none;">
                                                                {{$about_en->text}}
                                                            </textarea>

                                                        </div>
                                                    </div>

                                                </div>




                                            </div>
                                        </div>
                                    </div>
                                        <button type="submit" class="btn btn-success">Edit</button>
                                    </form>
                                </section>
                                <!-- Section: Live preview -->

                            </div>
                            <!-- Grid column -->

                        </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>

        </div>
    </section>
    <!-- /.modal -->
@endsection
@section('script')
    <script>
        var indexBefore = -1;

        function getIndex(itm, list) {
            var i;
            for (i = 0; i < list.length; i++) {
                if (itm[0] === list[i]) break;
            }
            return i >= list.length ? -1 : i;
        }

        $('.photo_img').sortable({
            items: "> li:not(:last)",
            start: function(event, ui) {
                indexBefore = getIndex(ui.item, $('.photo_img li'));

            },
            change: function(event, ui) {

                var indexAfter = getIndex(ui.item,$(".photo_img li"));
            },
            stop: function(event, ui) {
                $(".photo_img").children().css('border',0);
                $(".photo_img li:first-child").css(
                    "border", "2px solid red");
                var indexAfter = getIndex(ui.item,$(".photo_img li"));
                if (indexBefore==indexAfter) return;
                if (indexBefore<indexAfter) {
                    $(indexAfter).css('border','2px solid red;')

                    $($(".ads_images .indexes")[indexBefore]).insertAfter(

                        $($(".ads_images .indexes")[indexAfter]));
                }
                else {
                    $($(".ads_images .indexes")[indexBefore]).insertBefore(
                        $($(".ads_images .indexes")[indexAfter]));
                }
            }
        });

        function closee(index){

            // console.log(index);
            var empty_img = $('.imgs_list').find('.empty_img');
            if (!$(empty_img).length) {


                $('[index='+index+']').remove();
                $('[indexx2='+index+']').remove();
            }else{
                $('[index='+index+']').attr('style','');
                $('[index='+index+']').addClass('empty_img');
                $('[indexx2='+index+']').remove();
            }
        }
        var indexx=0;
        function readURL(input,indexxx) {

            if (input.files && input.files[0]) {

                var reader = new FileReader();
                reader.onload = function(e) {

                    var empty_img = $('.imgs_list').find('.empty_img');
                    if ($(empty_img).length) {

                        $(empty_img).attr('index',indexxx);
                        $(empty_img).css('background-image', 'url('+e.target.result+')');
                        $(empty_img).find('i').addClass('close');
                        $(empty_img).find('i').attr('onclick','closee('+indexxx+');');
                        $(empty_img).removeClass('empty_img').addClass('busy_img');

                    }
                    else {
                        var len=indexxx;

                        var img_html = '<li  index="'+len+'" class="busy_img" style="background-image: url('+e.target.result+');"><i onclick="closee('+len+')" class="close"></i></li>';
                        $(img_html).insertBefore($('.select_img'));
                        console.log('hello');

                    }

                }

                reader.readAsDataURL(input.files[0]);

            }

        }
        let i = 0;
        let indexes = [];
        $(document).on('change', '.is_free', function() {
            // indexxx=Math.round(Math.random(1,1000)*100);
            indexxx=i;
            indexes.push(i)

            readURL(this,indexxx);
            var len=$(".cat_img").eq(0).find('li').length-2;
            $(this).removeClass('is_free');
            $(this).attr('indexx2',indexxx);
            $('div.ads_images').append('<input type="file" name="ads_images_'+i+'" class="indexes is_free" style="display: none;">');
            i++;

        });

        $(document).off('click', '.select_img').on('click', '.select_img', function() {

            var is_free = $('.ads_images').find('input.is_free').first();
            if (!is_free) {
                return false;
            }

            $(is_free).click();
            return false;

        });

    </script>
@endsection
@section('style')
    <style>
        ul.cat_img {
            list-style: none;
            margin: 0;
            padding: 0;
            -webkit-column-count: 2;
            -moz-column-count: 2;
            column-count: 2;
            display: flex;
            margin-top: 15px;
        }
        ul.cat_img li {
            display: block;
            width: 94px;
            height: 94px;
            border: 1px solid #ddd;
            border-radius: 3px;
            text-align: center;
            margin-right: 10px;
            margin-bottom: 10px;
            position: relative;
            cursor: pointer;
        }
        .cat_img li div.cat_img {
            width: 100%;
            height: 55px;
            background-repeat: no-repeat !important;
            background-position: 50% 15px !important;
            filter: gray;
            -webkit-filter: grayscale(1);
            filter: grayscale(1);
        }
        .cat_img li.active {
            border: 1px solid #FF7313;
        }
        .cat_img li.active p {
            color: #FF7313;
        }
        .cat_img li.active div.cat_img {
            filter: none;
            -webkit-filter: none;
            filter: none;
        }
        .cat_img li div.cat_img img {
            filter: gray;
            -webkit-filter: grayscale(1);
            filter: grayscale(1);
        }
        .cat_img li p {
            position: absolute;
            display: block;
            text-align: center;
            bottom: -8px;
            width: 100%;
            font-size: 12px;
        }
        .empty_img {
            background: #ddd;
            background-size: cover;
        }
        .busy_img {
            background-size: cover;
        }
        .select_img {
            line-height: 94px;
            font-size: 40px;
            color: #FF7313;
        }
        .busy_img{
            cursor:grab!important;
        }
        position: absolute;
        right: 0;
        top: 0;
        width: 32px;
        height: 32px;
        opacity: 24.3;
        }
        .close:hover {
            opacity:1;
        }
        .close:before, .close:after {
            position: absolute;
            left: 15px;
            content: ' ';
            height: 33px;
            width: 2px;
            background-color: red;
        }
        .close:before {
            transform: rotate(45deg);
        }
        .close:after {
            transform: rotate(-45deg);
        }
        .closes {
            position: absolute;
            right: 0;
            top: 0;
            width: 32px;
            height: 32px;
            opacity: 24.3;
        }
        .closes:hover {
            opacity:1;
        }
        .closes:before, .closes:after {
            position: absolute;
            left: 15px;
            content: ' ';
            height: 33px;
            width: 2px;
            background-color: red;
        }
        .closes:before {
            transform: rotate(45deg);
        }
        .closes:after {
            transform: rotate(-45deg);
        }

        .close{
            cursor: pointer!important;
        }
        .busy_img:first-child {
            border: 2px solid red;
        }
        .topimg {
            height: 89px;
            width: 100%;
        }

    </style>
@endsection
