@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">String Translation</h3>
                        <div class="d-flex justify-content-end">
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                                Add Translation
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Key</th>
                                <th>Am</th>
                                <th>Ru</th>
                                <th>En</th>
                                <th>*</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($translation as $trans)
                                <tr>

                                    <td>{{$trans->id}}</td>
                                    <td>{{$trans->key}}</td>
                                    <td>{{$trans->am}}</td>
                                    <td>{{$trans->ru}}</td>
                                    <td>{{$trans->en}}</td>
                                    <td>
                                        <button data-id="{{$trans->id}}" data-key="{{$trans->key}}" data-am="{{$trans->am}}" data-ru="{{$trans->ru}}" data-en="{{$trans->en}}" type="button" class="btn btn-info edites" data-toggle="modal" data-target="#modal-edit">
                                            <i class="fas fa-edit"></i> Խմբագրել
                                        </button>

                                        <a href="{{route('admin.translation.delete',$trans->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                            <i class="fas fa-trash-alt"></i> Ջնջել
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        </div>

    </section>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Translation</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="POST" action="{{route('admin.translation.add')}}">
                    @csrf
                    <div class="modal-body">
                        <div class="card card-primary">

                            <!-- /.card-header -->
                            <!-- form start -->

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Key</label>
                                    <input type="text" name="key" class="form-control" id="exampleInputEmail1" placeholder="Enter Key">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Am</label>
                                    <input type="text" name="am" class="form-control" id="exampleInputPassword1" placeholder="Enter translation Armenian">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Ru</label>
                                    <input type="text" name="ru" class="form-control" id="exampleInputPassword1" placeholder="Enter translation Russian">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">En</label>
                                    <input type="text" name="en" class="form-control" id="exampleInputPassword1" placeholder="Enter translation English">
                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Translation</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="POST" action="{{route('admin.translation.edit')}}">
                    @csrf
                    <input type="hidden" name="editid" id="editid">
                    <div class="modal-body">
                        <div class="card card-primary">

                            <!-- /.card-header -->
                            <!-- form start -->

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Key</label>
                                    <input type="text" name="key" class="form-control" id="editkey" placeholder="Enter Key">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Am</label>
                                    <input type="text" name="am" class="form-control" id="editam" placeholder="Enter translation Armenian">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Ru</label>
                                    <input type="text" name="ru" class="form-control" id="editru" placeholder="Enter translation Russian">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">En</label>
                                    <input type="text" name="en" class="form-control" id="editen" placeholder="Enter translation English">
                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('script')
    <script>
        $('.edites').click(function (e){
            let id = $(this).data('id');
            let key = $(this).data('key');
            let am = $(this).data('am');
            let en = $(this).data('en');
            let ru = $(this).data('ru');

            $('#editkey').val(key);
            $('#editam').val(am);
            $('#editru').val(ru);
            $('#editen').val(en);
            $('#editid').val(id);

        })
    </script>
@endsection
