@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                                    @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                        {{ $error }}
                                </div>
                                    @endforeach

                            </div>
                        @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                               {!! \Session::get('success') !!}

                        </div>
                    @endif
                        <div class="card-header">
                            <h3 class="card-title">Product</h3>
                            <div class="d-flex justify-content-end">
                                <a href="{{route('admin.products.add')}}" class="btn btn-default">
                                    Add Product
                                </a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Category</th>
                                    <th>*</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $prod)
                                <tr>

                                    <td><img src="{{$prod->himage!=null ? asset('productimages/'.$prod->himage->image.''): ""}}" width="150px" alt=""></td>
                                    <td>{{$prod->id}}</td>
                                    <td>{{$prod->title}}</td>
                                    <td>{!! $prod->desc !!}</td>
                                    <td>{{$prod->price}}</td>
                                    @if($prod->categorys!=null)
                                    <td>{!! $prod->categorys->name !!}</td>
                                    @else
                                        <td></td>
                                        @endif

                                    <td>
                                        <a href="{{route('admin.product.edit',$prod->id)}}" class="btn btn-info">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                        <a href="{{route('admin.product.delete',$prod->unique_id)}}" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                            <i class="fas fa-trash-alt"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.modal -->
@endsection
