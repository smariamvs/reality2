@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Все продукты акции</h3>
                        <div class="d-flex justify-content-end">
                            <a href="{{route('admin.stock.add.product')}}" class="btn btn-default">
                                Добавить продукт к акции
                            </a>

                            <a href="{{route('admin.stock.setting')}}" class="btn btn-info">
                               Настройка акции
                            </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Картинка</th>
                                <th>Название</th>
                                <th>Категория</th>
                                <th>Старая цена</th>
                                <th>Новая цена</th>

                                <th>*</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stockProducts as $key => $product)
                                @if(!empty($product->product))
                                <tr>

                                    <td><img src="{{asset('productimages/'.$product->product->image->image)}}" width="150px" alt=""></td>
                                    <td>{{$product->product->title}}</td>
                                    <td>{{$product->product->categories->name}}</td>
                                    <td>{{$product->old_price}}</td>
                                    <td>{{$product->new_price}}</td>
                                    <td>

                                        <a href="" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                            <i class="fas fa-trash-alt"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        </div>

    </section>
    <!-- /.modal -->
@endsection
