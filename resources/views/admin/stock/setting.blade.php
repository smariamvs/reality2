@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Настройка акции </h3>
                        <div class="d-flex justify-content-end">
                            <a href="{{route('admin.stock.index')}}" class="btn btn-default">
                                Акции
                            </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">


                        <div class="row">
                            <div class="col-xl-12 mb-4 mb-xl-0">
                                <section>
                                    <form action="{{route('admin.stock.setting.update')}}" method="POST"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                                   role="tab" aria-controls="home" aria-selected="true">Am</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="myTabContent">

                                            <div class="tab-pane fade active show" id="home" role="tabpanel"
                                                 aria-labelledby="home-tab">
                                                <div class="card card-primary">


                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_am">Статус</label>
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="radio"
                                                                           name="status" value="on"
                                                                           id="flexRadioDefault1"
                                                                                {{$setting->status=='on'?'checked':""}}>
                                                                    <label class="form-check-label"
                                                                           for="flexRadioDefault1">
                                                                        Включен
                                                                    </label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="radio"
                                                                           name="status"
                                                                           id="flexRadioDefault2" value="off"
                                                                        {{$setting->status=='off'?'checked':""}}>
                                                                    <label class="form-check-label"
                                                                           for="flexRadioDefault2">
                                                                        Отключен
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Date:</label>
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <input name="to" autocomplete="off" type="text"
                                                                       class="form-control pull-right" id="datepicker" value="{{date('Y-m-d',$setting->to)}}">
                                                            </div>

                                                        </div>


                                                    </div>


                                                </div>
                                            </div>


                                        </div>
                                        <button type="submit" class="btn btn-success">Сохранит</button>
                                    </form>
                                </section>
                                <!-- Section: Live preview -->

                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.modal -->
@endsection

@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img').attr('src', e.target.result);
                    $('#img').show();
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#image").change(function () {
            readURL(this);
        });
    </script>
@endsection

