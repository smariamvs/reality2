@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Добавить подкатегория
                        </h3>
                        <div class="d-flex justify-content-end">
                            <a href="{{route('admin.subcategories.index')}}" class="btn btn-default">
                                Все подкатегории
                            </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">


                        <div class="row">
                            <div class="col-xl-12 mb-4 mb-xl-0">
                                <section>
                                    <form action="{{route('admin.stock.add.product.stock')}}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Am</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="myTabContent">

                                            <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                <div class="card card-primary">


                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_am">Новая цена</label>
                                                                <input type="number" name="new_price" required class="form-control">
                                                            </div>
                                                        </div>


                                                        <div class="form-group form_flex">
                                                            <p>Продукт</p>
                                                            <select name="product_id" id="" required class="form-control selectcustom2">
                                                                @foreach($products as $category)
                                                                    <option value="{{$category->unique_id}}">{{$category->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Date:</label>
                                                            <div class="input-group date">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                                <input name="to" autocomplete="off" type="text" class="form-control pull-right" id="datepicker">
                                                            </div>

                                                        </div>


                                                    </div>


                                                </div>
                                            </div>



                                        </div>
                                        <button type="submit" class="btn btn-success">Add</button>
                                    </form>
                                </section>
                                <!-- Section: Live preview -->

                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.modal -->
@endsection

@section('script')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img').attr('src', e.target.result);
                $('#img').show();
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }
    $("#image").change(function() {
        readURL(this);
    });
</script>
@endsection

