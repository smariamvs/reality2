@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Edit Icon
                        </h3>
                        <div class="d-flex justify-content-end">

                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">


                        <div class="row">
                            <div class="col-xl-12 mb-4 mb-xl-0">
                                <section>
                                    <form action="{{route('admin.update.about.icon',$icon_am->unique_id)}}" method="POST" enctype="multipart/form-data">
                                        @csrf

                                        <div class="tab-content" id="myTabContent">

                                            <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                <div class="card card-primary">


                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_am">Icon</label>
                                                                <input type="text" name="icon" value="{{$icon_am->icon}}" class="form-control">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_am">Text Armenian</label>
                                                                <input type="text" value="{{$icon_am->text}}" name="text_am" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_am">Text English</label>
                                                                <input type="text" value="{{$icon_en->text}}"  name="text_en" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_am">Text Russian</label>
                                                                <input type="text" value="{{$icon_ru->text}}"  name="text_ru" class="form-control">
                                                            </div>
                                                        </div>

                                                    </div>


                                                </div>
                                            </div>

                                        </div>
                                        <button type="submit" class="btn btn-success">Add</button>
                                    </form>
                                </section>
                                <!-- Section: Live preview -->

                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.modal -->
@endsection

@section('script')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img').attr('src', e.target.result);
                $('#img').show();
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }
    $("#image").change(function() {
        readURL(this);
    });
</script>
@endsection

