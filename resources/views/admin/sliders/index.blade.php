@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Sliders</h3>
                        <div class="d-flex justify-content-end">

                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Text </th>
                                <th>Image </th>

                                <th>*</th>

                            </tr>
                            </thead>
                            <tbody>

                        @foreach($sliders as $slider)
                                <tr>
                        <td>
                            {{$slider->text}}
                        </td>
                        <td>
                            <img src="{{asset('slider/'.$slider->img)}}" style="width: 150px" alt="">
                        </td>
                        <td>
                           <a href="{{route('admin.sliders.edit',$slider->slug)}}" class="btn btn-success">Edit</a>
                        </td>


                                </tr>
                        @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        </div>

    </section>
    <!-- /.modal -->
@endsection
