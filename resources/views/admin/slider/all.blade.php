@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">All Sliders</h3>
                        <div class="d-flex justify-content-end">
                            <a href="{{route('admin.sliders.add')}}" class="btn btn-default">
                                Add Slider
                            </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>#</th>
                                <th>Price</th>
                                <th>Description</th>

                                <th>*</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sliders as $key => $slider)
                                <tr>

                                    <td><img src="{{asset('slider/'.$slider->image)}}" width="150px" alt=""></td>
                                    <td>{{$key}}</td>
                                    <td>{{$slider->price}}</td>
                                    <td>{{ mb_strimwidth(strip_tags ($slider->text), 0, 50, "...")}}</td>



                                    <td>
                                        <a href="{{route('admin.slider.edit',$slider->slug)}}" class="btn btn-info">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                        <a href="{{route('admin.sliders.delete',$slider->slug)}}" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                            <i class="fas fa-trash-alt"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
        </div>

    </section>
    <!-- /.modal -->
@endsection
