@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Add Slider</h3>
                        <div class="d-flex justify-content-end">
                            <a href="{{route('admin.sliders.two')}}" class="btn btn-default">
                                All Sliders
                            </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">


                        <div class="row">
                            <div class="col-xl-12 mb-4 mb-xl-0">
                                <section>
                                    <form action="{{route('admin.sliders.save')}}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Am</a>
                                            </li>
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Ru</a>
                                            </li>
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link"  id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">En</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="myTabContent">

                                            <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                <div class="card card-primary">


                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_am">Description</label>
                                                                <textarea class="textarea" id="desc_am" name="desc_am" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px; display: none;"></textarea>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_am">Price</label>
                                                                <input type="number" name="price" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form_flex">
                                                            <p>Image</p>
                                                            <input type='file' id="image" name="image"/>
                                                            <img id="img" style="width: 200px; display: none" src="" alt="your image"/>
                                                        </div>

                                                    </div>


                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                <div class="card card-primary">


                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_ru">Description</label>
                                                                <textarea class="textarea" id="desc_ru" name="desc_ru" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px; display: none;"></textarea>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <div class="tab-pane fade " id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                                <div class="card card-primary">


                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_en">Description</label>
                                                                <textarea class="textarea" id="desc_en" name="desc_en" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px; display: none;"></textarea>

                                                            </div>
                                                        </div>

                                                    </div>




                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-success">Add</button>
                                    </form>
                                </section>
                                <!-- Section: Live preview -->

                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.modal -->
@endsection

@section('script')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img').attr('src', e.target.result);
                $('#img').show();
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }
    $("#image").change(function() {
        readURL(this);
    });
</script>
@endsection

