@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Edit Product</h3>
                        <div class="d-flex justify-content-end">
                            <a href="{{route('admin.products')}}" class="btn btn-default">
                                All Products
                            </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">


                        <div class="row">
                            <div class="col-xl-12 mb-4 mb-xl-0">
                                <section>
                                    <form action="{{route('admin.slider.edit.save',$slider_am->slug)}}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Am</a>
                                            </li>
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Ru</a>
                                            </li>
                                            <li class="nav-item waves-effect waves-light">
                                                <a class="nav-link"  id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">En</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="myTabContent">

                                            <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                <div class="card card-primary">

                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_am">Description</label>
                                                                <textarea class="textarea" id="desc_am" name="desc_am" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px; display: none;">{{$slider_am->text}}</textarea>
                                                                <div class="form-group">
                                                                    <div class="mb-3">
                                                                        <label for="desc_am">Price</label>
                                                                        <input type="number" name="price" value="{{$slider_am->price}}" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <input type='file' id="imgInp" name="image"/>
                                                                <img id="blah" style="width: 200px; height: 350px" src="{{asset('slider/'.$slider_am->image)}}" alt="your image" />
                                                            </div>
                                                        </div>


                                                        <div class="form-group form_flex">


                                                        </div>

                                                    </div>


                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                <div class="card card-primary">


                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_ru">Description</label>
                                                                <textarea class="textarea" id="desc_ru" name="desc_ru" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px; display: none;">{{$slider_ru->text}}</textarea>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <div class="tab-pane fade " id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                                <div class="card card-primary">


                                                    <div class="card-body">

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="desc_en">Description</label>
                                                                <textarea class="textarea" id="desc_en"  name="desc_en" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px; display: none;">{{$slider_en->text}}</textarea>

                                                            </div>
                                                        </div>

                                                    </div>




                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-success">Save</button>
                                    </form>
                                </section>
                                <!-- Section: Live preview -->

                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.modal -->
@endsection
@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });



    </script>
@endsection
@section('style')
    <style>
        ul.cat_img {
            list-style: none;
            margin: 0;
            padding: 0;
            -webkit-column-count: 2;
            -moz-column-count: 2;
            column-count: 2;
            display: flex;
            margin-top: 15px;
        }
        ul.cat_img li {
            display: block;
            width: 94px;
            height: 94px;
            border: 1px solid #ddd;
            border-radius: 3px;
            text-align: center;
            margin-right: 10px;
            margin-bottom: 10px;
            position: relative;
            cursor: pointer;
        }
        .cat_img li div.cat_img {
            width: 100%;
            height: 55px;
            background-repeat: no-repeat !important;
            background-position: 50% 15px !important;
            filter: gray;
            -webkit-filter: grayscale(1);
            filter: grayscale(1);
        }
        .cat_img li.active {
            border: 1px solid #FF7313;
        }
        .cat_img li.active p {
            color: #FF7313;
        }
        .cat_img li.active div.cat_img {
            filter: none;
            -webkit-filter: none;
            filter: none;
        }
        .cat_img li div.cat_img img {
            filter: gray;
            -webkit-filter: grayscale(1);
            filter: grayscale(1);
        }
        .cat_img li p {
            position: absolute;
            display: block;
            text-align: center;
            bottom: -8px;
            width: 100%;
            font-size: 12px;
        }
        .empty_img {
            background: #ddd;
            background-size: cover;
        }
        .busy_img {
            background-size: cover;
        }
        .select_img {
            line-height: 94px;
            font-size: 40px;
            color: #FF7313;
        }
        .busy_img{
            cursor:grab!important;
        }
        position: absolute;
        right: 0;
        top: 0;
        width: 32px;
        height: 32px;
        opacity: 24.3;
        }
        .close:hover {
            opacity:1;
        }
        .close:before, .close:after {
            position: absolute;
            left: 15px;
            content: ' ';
            height: 33px;
            width: 2px;
            background-color: red;
        }
        .close:before {
            transform: rotate(45deg);
        }
        .close:after {
            transform: rotate(-45deg);
        }
        .closes {
            position: absolute;
            right: 0;
            top: 0;
            width: 32px;
            height: 32px;
            opacity: 24.3;
        }
        .closes:hover {
            opacity:1;
        }
        .closes:before, .closes:after {
            position: absolute;
            left: 15px;
            content: ' ';
            height: 33px;
            width: 2px;
            background-color: red;
        }
        .closes:before {
            transform: rotate(45deg);
        }
        .closes:after {
            transform: rotate(-45deg);
        }

        .close{
            cursor: pointer!important;
        }
        .busy_img:first-child {
            border: 2px solid red;
        }
        .topimg {
            height: 89px;
            width: 100%;
        }

    </style>
@endsection
