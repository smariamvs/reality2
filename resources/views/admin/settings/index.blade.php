@extends('_layouts.admin')
@section('content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (count($errors) > 0)


                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach

                    </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">

                            {!! \Session::get('success') !!}

                        </div>
                    @endif
                    <div class="card-header">
                        <h3 class="card-title">Edit Site Settings</h3>
                        <div class="d-flex justify-content-end">

                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">


                        <div class="row">
                            <div class="col-xl-12 mb-4 mb-xl-0">
                                <section>
                                    <form action="{{route('admin.settings.save')}}" method="POST"
                                          enctype="multipart/form-data">
                                        @csrf

                                        <div class="tab-content" id="myTabContent">

                                            <div class="tab-pane fade active show" id="home" role="tabpanel"
                                                 aria-labelledby="home-tab">
                                                <div class="card card-primary">


                                                    <div class="card-body">
                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="fb_link">Facebook link</label>
                                                                <input class="form-control" type="text" name="fb"
                                                                       id="fb_link" value="{{$setting->fb}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="instalink">Instagram link</label>
                                                                <input class="form-control" type="text" name="insta"
                                                                       id="instalink" value="{{$setting->insta}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="email">Site email</label>
                                                                <input class="form-control" type="email" name="email"
                                                                       id="email" value="{{$setting->email}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="mb-3">
                                                                <label for="phone">Phone</label>
                                                                <input class="form-control" type="tel" name="phone"
                                                                       id="phone" value="{{$setting->phone}}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group form_flex">
                                                            <p>Header logo</p>
                                                            <input type='file' id="header" name="logo"/>
                                                            <img id="headerimg" style="width: 200px"
                                                                 src="{{asset('uploads/'.$setting->logo)}}"
                                                                 alt="your image"/>
                                                        </div>

                                                        <div class="form-group form_flex">
                                                            <p>Partners section background</p>
                                                            <input type='file' id="partners" name="partners"/>
                                                            <img id="partnersback" style="width: 200px"
                                                                 src="{{asset('uploads/'.$setting->partners)}}"
                                                                 alt="your image"/>
                                                        </div>


                                                        <div class="form-group form_flex">
                                                            <p>Green section background</p>
                                                            <input type='file' id="green" name="green"/>
                                                            <img id="greensection" style="width: 200px"
                                                                 src="{{asset('uploads/'.$setting->green)}}"
                                                                 alt="your image"/>
                                                        </div>


                                                    </div>


                                                </div>
                                            </div>

                                        </div>
                                        <button type="submit" class="btn btn-success">Save</button>
                                    </form>
                                </section>
                                <!-- Section: Live preview -->

                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.modal -->
@endsection
@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#headerimg').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        function readURLfoot(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#partnersback').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        function readURLslide(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#slide_background').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }
        function readURLslide(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#greensection').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#header").change(function () {
            readURL(this);
        });
        $("#partners").change(function () {
            readURLfoot(this);
        });

        $("#slide").change(function () {
            readURLslide(this);
        });
        $("#green").change(function () {
            readURLslide(this);
        });


    </script>
@endsection
